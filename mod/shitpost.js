"use strict";

const fs = require("fs");

const source = fs.readFileSync("DEMO_DAT.BIN");
const pp = fs.readFileSync("palette.bin");
const ii = fs.readFileSync("image.bin");

pp.copy(source, 0x0032100);
ii.copy(source, 0x0032800);

const pd = fs.readFileSync("palette_16.bin");
const id = fs.readFileSync("image_16.bin");

pd.copy(source, 0x0007100);
id.copy(source, 0x0007800);

fs.writeFileSync("DEMO_POO.BIN", source);

/*-----------------------------------------------------------------------------

	Dash Rom Hacker Copyright 2018 DashGL Project 

    Dash Rom Hacker is free software: you can redistribute it and/or modify 
	it under the terms of the GNU General Public License as published by the 
	Free Software Foundation, either version 3 of the License, or (at your 
	option) any later version.

    Dash Rom Hacker is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
	or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
	more details.

    You should have received a copy of the GNU General Public License along with
	Dash Rom Hacker. If not, see http://www.gnu.org/licenses/.

------------------------------------------------------------------------------*/

const fs = require("fs");

const SEARCH = [
	{
		memo : "Horroko",
		find : "00003acbb5ba73b20fae91a62ea2cc99c698eead51ae74a44b98ab9d93b68a25",
		repl : "0000ffff7abbbdb01dae91a6af20579ba980eead51ae5ccb4b9865fd93b68aa5"
	},
	{
		memo : "Red Megaman",
		find : "0000c6fd65fde4eca4d064b8e083ecf288f2e6e545d1ff816cb509adc8a4968c",
		repl : "0000108c9f8c6480efc139e3e08362987a88adb5a59cff816cb509adc8a4968c"
	},
	{
		memo : "Yellow Roll",
		find : "00003fcfbebed5a5df825f82dc811c9db694718c2c847befd6daefbdc698a381",
		repl : "00003fcfbebed5a5e7eb2acb05d37c8f5b97ba8eb9857befd6daefbde79c4aa5"
	}
]

if(process.argv.length < 3) {
	console.error("Usage: node replace.js <megaman rom file>");
	process.exit();
}

console.log("Megaman Legends ROM hacker utility");
console.log("Disclaimer: this utility comes with no warranty, use with caution");
console.log("Read: http://mmls.proboards.com/post/97613/thread for more information");
console.log("");

let source;
let name = process.argv[2].split(".");
name[name.length - 2] += "_out";
name = name.join(".");

try {
	source = fs.readFileSync(process.argv[2]);
} catch(err) {
	console.error("Could not open rom file for reading");
	process.exit();
}

if(fs.existsSync(name) && process.argv[3] !== "-y") {
	console.log("Error! Output file: %s already exists", name);
	console.log("Please include option to overwrite");
	console.error("Example: node replace.js <megaman rom file> -y");
	console.log("Exitting process");
	process.exit();
}

SEARCH.forEach(cond => {

	if(cond.memo) {
		console.log("Searching ROM file for: %s", cond.memo);
	}

	const find = Buffer.from(cond.find, "hex");
	const repl = Buffer.from(cond.repl, "hex");

	let index = 0;
	while( (index = source.indexOf(find, index)) !== -1) {
		console.log("Index found at: 0x%s, replacing", index.toString(16));
		repl.copy(source, index);
		index++;
	}

	if(cond.memo) {
		console.log("");
	}

});

console.log("Writing output to: $s", name);
fs.writeFileSync(name, source);

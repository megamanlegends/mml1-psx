"use strict";

const fs = require("fs");
const haystack = fs.readFileSync("mml1.bin");

const needle = Buffer.from("2E3F454F363F4413868903", "hex");
//const needle = Buffer.from("2E3F454F363F4413868903D00D89009A0D050D9F", "hex");

let index = 0;
while( (index = haystack.indexOf(needle, index)) !== -1) {
	
	let ofs = index.toString(16);
	index += needle.length;
	let bytes = [
		haystack.readUInt8(index + 0).toString(16),
		haystack.readUInt8(index + 1).toString(16),
		haystack.readUInt8(index + 2).toString(16),
		haystack.readUInt8(index + 3).toString(16)
	];

	for(let i = 0; i < bytes.length; i++) {
		if(bytes[i].length < 2) {
			bytes[i] = "0" + bytes[i];
		}
	}

	haystack.writeUInt8(0x10, index + 1);
	haystack.writeUInt8(0x10, index + 5);

	console.log("Chest found at offset: 0x%s with %s %s %s %s",
		ofs,
		bytes[0],
		bytes[1],
		bytes[2],
		bytes[3]
	);

	break;

}

fs.writeFileSync("mml_cheat.bin", haystack);

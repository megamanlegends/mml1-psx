"use strict";

const fs = require("fs");
const source = fs.readFileSync("mml1.bin");

const LENGTH = 0x800;
const STRIDE = 0x130;

let pal, palOfs, img, imgOfs, ofs;

/* Replace Servbot Texture */

palOfs = 0x0230818;
imgOfs = 0x0231048;

pal = fs.readFileSync("palette_16.bin");
img = fs.readFileSync("image_16.bin");

pal.copy(source, palOfs);

ofs = imgOfs;
for(let i = 0; i < img.length / LENGTH; i++) {
	img.copy(source, ofs, i*LENGTH, (i+1)*LENGTH);
	ofs += (LENGTH + STRIDE);
}

/* Title Logo Texture */

palOfs = 0x0261e38;
imgOfs = 0x0262668;

pal = fs.readFileSync("palette_256.bin");
img = fs.readFileSync("image_256.bin");

pal.copy(source, palOfs);

ofs = imgOfs;
for(let i = 0; i < img.length / LENGTH; i++) {
	img.copy(source, ofs, i*LENGTH, (i+1)*LENGTH);
	ofs += (LENGTH + STRIDE);
}

/* Write to new ROM */

fs.writeFileSync("america.bin", source);

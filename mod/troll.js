"use strict";

const fs = require("fs");

const source = fs.readFileSync("mml1_out.bin");
const whence = Buffer.from("00bcbc1a1a1aaf2819af19af", "hex");
const pp = fs.readFileSync("palette.bin");
const ii = fs.readFileSync("image.bin");

console.log(source.indexOf(whence, 0x0261e38).toString(16));

pp.copy(source, 0x0261e38);
ii.copy(source, 0x0262668);

fs.writeFileSync("mml_cheat.bin", source);

const CHAR = [
"0","1","2","3","4","5","6","7","8","9",
null,null,null,"!","?",null,null,"(", ")",":",null,
"A","B","C","D","E","F","G","H","I","J","K","L",
"M","N","O","P","Q","R","S","T","\0","U","V","W","X","Y","Z",
"a","b","c","d","e","f","g","h","i","j","k","l","m","n","\0",
"o","p","q","r","s","t","u","v","w","x","y","z",null,null,null,null,
" ",null,null,null,null,"○","△","X","□","L1","L2","R1","R2",
",", null, ".", "…", null, null, null, null, null, null, null, null,
null, null, null, null, null, null, null, null, null, null, null,
null, null, null, null, null, null, null, null, null, null,
null, null, null, null, null, null
]


const MsgEditor = (function() {

    this.MEM = {
        db : new Dexie('mml1-msg'),
    };

	this.DOM = {
		nav : document.getElementById('nav'),
		ofs : document.getElementById('ofs'),
		hex : document.getElementById('hex'),
		read : document.getElementById('read'),
		high : document.getElementById('high'),
		write : document.getElementById('write'),
		page : document.getElementById('page')
	}

	this.EVT = {
		handleNavClick : evt_handleNavClick.bind(this)
	}

	this.API = {
		renderFileList : api_renderFileList.bind(this),
		renderMessageHex : api_renderMessageHex.bind(this)
	}

	init.apply(this);
	return this;

	function init() {
		
        this.MEM.db.version(1).stores({
            files : '&name'
        });
        this.MEM.db.open();

		this.DOM.nav.addEventListener('click', this.EVT.handleNavClick);
		this.API.renderFileList();

	}

	async function evt_handleNavClick(evt) {

        let query = {
            name : evt.target.textContent
        };

        let file = await this.MEM.db.files.get(query);
        if(!file) {
            return;
        }

        if(this.MEM.activeNav) {
            this.MEM.activeNav.classList.remove("active");
        }

        this.MEM.activeNav = evt.target;
        this.MEM.activeNav.classList.add("active");
		
		this.API.renderMessageHex(file);

	}

	function api_renderFileList() {

        this.DOM.nav.innerHTML = "";

        this.MEM.db.files.each(file => {

            let li = document.createElement('li');
            li.textContent = file.name;
            nav.appendChild(li);

        });


	}

	function api_renderMessageHex(file) {

		let ofs_str = "";
		let hex_str = "";
		let hi_str = "";
		let r_str = "";
		let w_str = "";

		let data = new Uint8Array(file.data);

		for(let i = 0; i < data.byteLength; i++) {
			
			let c ,h;
			
			c = data[i].toString(16);
			if(c.length < 2) {
				c = "0" + c;
			}
			
			h = "  ";
			if(CHAR[data[i]]) {
				h = c;
			} 
			
			r_str += CHAR[data[i]] || ".";
			hex_str += c;
			hex_str += " ";
			
			hi_str += h;
			hi_str += " ";

		}
		
		let rows = Math.ceil(data.byteLength / 16);
		for(let i = 0; i < rows; i++) {
			
			let n = (i * 16).toString(16);
			while(n.length < 6) {
				n = "0" + n;
			}

			ofs_str += n;
			ofs_str += "\n";
		}
		
		//this.DOM.high.value = hi_str;
		this.DOM.high.value = "";
		this.DOM.high.setAttribute("rows", rows);

		this.DOM.read.value = r_str;
		this.DOM.read.setAttribute("rows", rows);
		this.DOM.write.value = w_str;
		this.DOM.write.setAttribute("rows", rows);

		this.DOM.ofs.value = ofs_str;
		this.DOM.ofs.setAttribute("rows", rows);
		this.DOM.hex.value = hex_str;
		this.DOM.hex.setAttribute("rows", rows);

		let height = this.DOM.hex.offsetHeight;
		this.DOM.page.style.height = (height + 100) + "px";

	}

}).apply({});

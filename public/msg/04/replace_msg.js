"use strict";

const fs = require("fs");

// Check the number of arguments

if(process.argv.length !== 5) {
	console.error("Usage: node replace.js <rom> <json> <out>");
	process.exit();
}

// Check the rom file

let rom;
let ext = process.argv[2].split(".").pop().toLowerCase();

switch(ext) {
case "img":
case "bin":
	rom = fs.readFileSync(process.argv[2]);
	break;
default:
	console.error("Expecting .bin or .img for rom");
	process.exit();
	break;
}

// Check the json file

ext = process.argv[3].split(".").pop().toLowerCase();
if(ext !== "json") {
	console.error("Expecting .json for patch file");
	process.exit();
}

// Read the json file

let buffer = fs.readFileSync(process.argv[3]);
let json = JSON.parse(buffer.toString());

// Look up file offsets

const FILES = {};

// Replace Messages

for(let key in json) {
	
	let msg = json[key];

	console.log(key);

	let src = Buffer.from(msg.src, "base64");
	let dst = Buffer.from(msg.dst, "base64");

	// Replace all instances
	
	let ofs = rom.indexOf(key);
	ofs = rom.indexOf(src, ofs);
	if(ofs === -1) {
		console.log("Could not copy");
		continue;
	}
	dst.copy(rom, ofs);
		
}

fs.writeFileSync(process.argv[4], rom);

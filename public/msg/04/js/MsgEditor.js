const CHAR = [
	"0","1","2","3","4","5","6","7","8","9",
	null,null,null,"!","?",null,null,"(", ")",":",null,
	"A","B","C","D","E","F","G","H","I","J","K","L",
	"M","N","O","P","Q","R","S","T","\0","U","V","W","X","Y","Z",
	"a","b","c","d","e","f","g","h","i","j","k","l","m","n","\0",
	"o","p","q","r","s","t","u","v","w","x","y","z",null,null,null,null,
	" ",null,null,null,null,"○","△","X","□","L1","L2","R1","R2",
	",", null, ".", "…", null, null, null, null, null, null, null, null,
	null, null, null, null, null, null, null, null, null, null, null,
	null, null, null, null, null, null, null, null, null, null,
	null, null, null, null, null, null
];

const MsgEditor = (function() {

	this.MEM = {
		db : new Dexie('mml1-msg'),
	};

	this.DOM = {
		files : document.getElementById('files'),
		nav : document.getElementById('nav'),
		aside : document.getElementById('aside'),
		label : document.getElementById('label'),
		textarea : document.getElementById('textarea'),
		patch : document.getElementById('patch'),
		restore : document.getElementById('restore')
	};

	this.EVT = {
		handleFileChange : evt_handleFileChange.bind(this),
		handleNavClick : evt_handleNavClick.bind(this),
		handleAsideClick : evt_handleAsideClick.bind(this),
		handleTextInput : evt_handleTextInput.bind(this),
		handlePatchClick : evt_handlePatchClick.bind(this),
		handleRestoreClick : evt_handleRestoreClick.bind(this)
	};

	this.API = {
		readFileAsync : api_readFileAsync.bind(this),
		renderFileList : api_renderFileList.bind(this),
		renderMessage : api_renderMessage.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

		this.MEM.db.version(1).stores({
			files : '&name,dirty',
		});
		this.MEM.db.open();

		this.API.renderFileList();
		this.DOM.files.addEventListener('change', this.EVT.handleFileChange);
		this.DOM.nav.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.aside.addEventListener('click', this.EVT.handleAsideClick);

		this.DOM.textarea.value = "";
		this.DOM.textarea.setAttribute("disabled", "disabled");
		this.DOM.textarea.addEventListener('input', this.EVT.handleTextInput);
		this.DOM.patch.addEventListener('click', this.EVT.handlePatchClick);
		this.DOM.restore.addEventListener('click', this.EVT.handleRestoreClick);

	}

	async function evt_handleFileChange(evt) {

		let files = evt.target.files;
		for(let i = 0; i < files.length; i++) {

			let buffer = await this.API.readFileAsync(files[i]);
			let view = new DataView(buffer);

			let ofs = 0;
			do {
				
				let short = view.getUint16(ofs + 0x40, true);
				if(short !== 0x2e2e) {
					continue;
				}

				let name = "";
				for(let k = 0; k < 0x20; k++) {
					let byte = view.getUint8(ofs + 0x40 + k);
					if(!byte) {
						break;
					}
					name += String.fromCharCode(byte);
				}

				name = name.toLowerCase();

				if(name.indexOf(".msg") === -1) {
					continue;
				}

				if(name.indexOf("dummy") !== -1) {
					continue;
				}

				let len = view.getUint32(ofs + 4, true);
				ofs += 0x800;
				let data = buffer.slice(ofs, ofs + len);
				await this.MEM.db.files.put({
					name : name,
					dirty : 0,
					data : data
				});

			} while( (ofs += 0x400) < buffer.byteLength);
			
		}
		
		this.API.renderFileList();
	}

	async function evt_handleNavClick(evt) {

		let query = {
			name : evt.target.textContent
		};

		let file = await this.MEM.db.files.get(query);
		if(!file) {
			return;
		}

		if(this.MEM.activeNav) {
			this.MEM.activeNav.classList.remove("active");
		}

		this.MEM.activeNav = evt.target;
		this.MEM.activeNav.classList.add("active");

		let view = new DataView(file.data);
		let first = view.getUint16(0, true);
		let count = first / 2;

		// Populate aside
		
		let keys = [];
		this.DOM.aside.innerHTML = "";
		for(let i = 0; i < count; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			let li = document.createElement("li");
			li.setAttribute("data-index", i);
			if(i === 0) {
				li.classList.add('active');
				this.MEM.activeAside = li;
			}
			li.textContent = "Message " + num;
			this.DOM.aside.appendChild(li);
			keys[i] = "Message " + num;

		}

		// Check for converted conversation

		this.MEM.file = file;
		
		if(file.msg) {
			this.API.renderMessage(keys[0], 0);
			return;
		}

		let table = [];
		let ofs = 0;

		for(let i = 0; i < count; i++) {
			table[i] = view.getUint16(i * 2, true);
		}
		table.push(file.data.byteLength);
		
		file.msg = {};
		for(let i = 0; i < count; i++) {

			let ofs = table[i];
			let end = table[i + 1];

			file.msg[keys[i]] = [];

			while(ofs < end) {
				let byte = view.getUint8(ofs++);
				file.msg[keys[i]].push(byte);
			}

		}

		this.API.renderMessage(keys[0], 0);

	}

	function evt_handleAsideClick(evt) {

		let index = evt.target.getAttribute("data-index");
		if(!index) {
			return;
		}

		index = parseInt(index);
		this.API.renderMessage(evt.target.textContent, index);

		if(this.MEM.activeAside) {
			this.MEM.activeAside.classList.remove("active");
		}

		this.MEM.activeAside = evt.target;
		this.MEM.activeAside.classList.add("active");

	}

	function evt_handleTextInput() {
		
		const val = this.DOM.textarea.value;
		const len = this.DOM.textarea.value.length;

		if(this.MEM.str.length === len) {
			console.log("its teh same");
			return;
		}
		
		if(this.MEM.str.length > len) {
			
			// Character has been removed,
			// splice out of msg array
			let diff = this.MEM.str.length - len;

			for(let i = 0; i < this.MEM.str.length; i++) {
				if(this.MEM.str[i] === val[i]) {
					continue;
				}
				
				this.MEM.msg.splice(i, diff);
				break;
			}

		} else if(this.MEM.str.length < len) {

			// character has been added
			// splice character into array

			let diff = len - this.MEM.str.length;

			for(let i = 0; i < len; i++) {
				if(this.MEM.str[i] === val[i]) {
					continue;
				}
				
				for(let k = 0; k < diff; k++) {
					let code = CHAR.indexOf(val[i+k]);
					if(val[i+k] === '\n') {
						code = 0x86;
					}
					this.MEM.msg.splice(i+k, 0, code);
				}

				break;
			}

		}

		this.MEM.str = val;
		this.MEM.file.dirty = 1;
		this.MEM.db.files.put(this.MEM.file);
		this.MEM.activeNav.classList.add("dirty");

	}

	async function evt_handlePatchClick() {

		let query = { dirty : 1 };
		const msgs = await this.MEM.db.files.where(query).toArray();
		const patch = {};

		msgs.forEach(msg => {
			
			// Namespace

			const mem = {};

			// Base64 encode source

			let array = new Uint8Array(msg.data);
			let code = String.fromCharCode.apply(null, array);
			mem.src = btoa(code);

			// Create dest array
			
			console.log(msg);

			let keys = Object.keys(msg.msg);
			let len = keys.length;
			let byteLen = len * 2;
			let ofs = byteLen;
			let head = new Uint16Array(len);
			
			let i = 0;
			for(let key in msg.msg) {
				head[i++] = ofs;
				ofs += (msg.msg[key].length || 0);
				byteLen += (msg.msg[key].length || 0);
			}

			let bytes = new Uint8Array(byteLen);
			let header = new Uint8Array(head.buffer);

			ofs = 0;
			for(i = 0; i < header.byteLength; i++) {
				bytes[ofs++] = header[i];
			}
			
			for(let i = 0; i < keys.length; i++) {
				let key = keys[i];
				for(k = 0; k < msg.msg[key].length; k++) {
					bytes[ofs++] = msg.msg[key][k];
				}
			}

			code = String.fromCharCode.apply(null, bytes);
			mem.dst = btoa(code);
			patch[msg.name] = mem;

		});

		let str = JSON.stringify(patch);
		var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "dash_message.json");

	}

	function evt_handleRestoreClick(evt) {

		if(!this.MEM.file) {
			return;
		}

		console.log("restore");
		console.log(this.MEM);
		
		let view = new DataView(this.MEM.file.data);
		let first = view.getUint16(0, true);
		let count = first / 2;

		let table = [];
		let ofs = 0;

		for(let i = 0; i < count; i++) {
			table[i] = view.getUint16(i * 2, true);
		}
		table.push(this.MEM.file.data.byteLength);
		
		let i = this.MEM.index;
		let num = i.toString();
		while(num.length < 3) {
			num = "0" + num;
		}
		let key = "Message " + num;

		ofs = table[i];
		let end = table[i + 1];

		this.MEM.file.msg[key] = [];

		while(ofs < end) {
			let byte = view.getUint8(ofs++);
			this.MEM.file.msg[key].push(byte);
		}

		this.API.renderMessage(key, i);
		this.MEM.db.files.put(this.MEM.file);

	}

	function api_readFileAsync(file) {
		
		return new Promise( (resolve, reject) => {

			let reader = new FileReader();
			
			reader.onload = evt => {
				
				resolve(evt.target.result);

			}

			reader.readAsArrayBuffer(file);

		});

	}

	function api_renderFileList() {

		this.DOM.nav.innerHTML = "";

		this.MEM.db.files.each(file => {

			let li = document.createElement('li');
			li.textContent = file.name;
			if(file.dirty) {
				li.classList.add("dirty");
			}
			nav.appendChild(li);

		});

	}

	function api_renderMessage(index, idx) {
		
		this.MEM.index = idx;
		this.DOM.textarea.removeAttribute("disabled");
		this.DOM.label.textContent = index;
		const msg = this.MEM.file.msg[index];
		
		this.MEM.msg = msg;

		let str = "";

		msg.forEach(byte => {

			if(byte === 0x86) {
				str += '\n';
				return;
			}
			
			let c = CHAR[byte];

			str += c || '◆';

		});

		this.MEM.str = str;
		this.DOM.textarea.value = str;

	}

}).apply({});

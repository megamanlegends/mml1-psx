const CHAR = [
"0","1","2","3","4","5","6","7","8","9", 
"ç","ß","'","!","?","「","・","(", ")",":","\0",
"A","B","C","D","E","F","G","H","I","J","K","L",
"M","N","O","P","Q","R","S","T","\0","U","V","W","X","Y","Z", 
"a","b","c","d","e","f","g","h","i","j","k","l","m","n","\0",
"o","p","q","r","s","t","u","v","w","x","y","z","&","(Z)","¥","/",
" ",">","~","-","\0","○","△","X","□","L1","L2","R1","R2",
",", "\"", ".", "…", "->", "%", "Ä", "À", "Â", "È", "Ê", "\0",
"É", "Ï", "Î", "Ö", "Ô", "Ü", "Ù", "Û", "Ç", "ä", "à", 
"â", "è", "ê", "é", "ï", "î", "ö", "ô", "ü", "\0",
"ù", "û", "α", "Ω", ";", "="
]


const MsgEditor = (function() {

	this.MEM = {
		db : new Dexie('mml1-msg'),
	};

	this.DOM = {
		files : document.getElementById('files'),
		nav : document.getElementById('nav'),
		aside : document.getElementById('aside'),
		tbody : document.getElementById('tbody')
	};

	this.EVT = {
		handleFileChange : evt_handleFileChange.bind(this),
		handleNavClick : evt_handleNavClick.bind(this),
		handleAsideClick : evt_handleAsideClick.bind(this)
	};

	this.API = {
		readFileAsync : api_readFileAsync.bind(this),
		renderFileList : api_renderFileList.bind(this),
		renderMessage : api_renderMessage.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

		this.MEM.db.version(1).stores({
			files : '&name'
		});
		this.MEM.db.open();

		this.API.renderFileList();
		this.DOM.files.addEventListener('change', this.EVT.handleFileChange);
		this.DOM.nav.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.aside.addEventListener('click', this.EVT.handleAsideClick);

	}

	async function evt_handleFileChange(evt) {

		let files = evt.target.files;
		for(let i = 0; i < files.length; i++) {

			let buffer = await this.API.readFileAsync(files[i]);
			let view = new DataView(buffer);

			let ofs = 0;
			do {
				
				let short = view.getUint16(ofs + 0x40, true);
				if(short !== 0x2e2e) {
					continue;
				}

				let name = "";
				for(let k = 0; k < 0x20; k++) {
					let byte = view.getUint8(ofs + 0x40 + k);
					if(!byte) {
						break;
					}
					name += String.fromCharCode(byte);
				}

				name = name.toLowerCase();

				if(name.indexOf(".msg") === -1) {
					continue;
				}

				if(name.indexOf("dummy") !== -1) {
					continue;
				}

				let len = view.getUint32(ofs + 4, true);
				ofs += 0x800;
				let data = buffer.slice(ofs, ofs + len);
				await this.MEM.db.files.put({
					name : name,
					data : data
				});

			} while( (ofs += 0x400) < buffer.byteLength);
			
		}
		
		this.API.renderFileList();
	}

	async function evt_handleNavClick(evt) {

		let query = {
			name : evt.target.textContent
		};

		let file = await this.MEM.db.files.get(query);
		if(!file) {
			return;
		}

		if(this.MEM.activeNav) {
			this.MEM.activeNav.classList.remove("active");
		}

		this.MEM.activeNav = evt.target;
		this.MEM.activeNav.classList.add("active");

		let view = new DataView(file.data);
		let first = view.getUint16(0, true);
		let count = first / 2;

		// Populate aside

		this.DOM.aside.innerHTML = "";
		for(let i = 0; i < count; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			let li = document.createElement("li");
			li.setAttribute("data-index", i);
			li.textContent = "Message " + num;
			this.DOM.aside.appendChild(li);
		}

		// Check for converted conversation

		this.MEM.file = file;
		
		if(file.msg) {
			return;
		}

		let table = [];
		let ofs = 0;

		for(let i = 0; i < count; i++) {
			table[i] = view.getUint16(i * 2, true);
		}
		table.push(file.data.byteLength);

		file.msg = [];
		
		let box = {};
		for(let i = 0; i < count; i++) {

			let start = table[i];
			let end = table[i + 1];

			file.msg[i] = {
				leading : [],
				textboxes : []
			};

			let ofs = start;
			let str = "";
			let box = 0;
			
			while(ofs < end && ofs < file.data.byteLength) {
				
				let byte = view.getUint8(ofs++);
				file.msg[i].leading.push(byte);
				if(byte === 0x08) {
					break;
				}

			}

			while(ofs < end && ofs < file.data.byteLength) {
				
				if(!file.msg[i].textboxes[box]) {

					file.msg[i].textboxes[box] = {
						audio : 0,
						pause : 0,
						audio_clip : "",
						pause_time : 0,
						button_wait : 0,
						str : "",
						next : []
					}
					

				}
				

				let byte = view.getUint8(ofs++);

				switch(byte) {
				case 0x86:
					file.msg[i].textboxes[box].str += "\n";
					break;
				case 0xa0:
					file.msg[i].textboxes[box].str += "[center]";
					break;
				case 0xa1:
					file.msg[i].textboxes[box].str += "[voice]";
					break;
				case 0xa2:
					file.msg[i].textboxes[box].str += "[text]";
					break;
				case 0x87:
					//file.msg[i].textboxes[box].str += "[next]";
					file.msg[i].textboxes[box].next.push(view.getUint8(ofs++));
					file.msg[i].textboxes[box].next.push(view.getUint8(ofs++));
					box++;
					break;
				case 0x84:
					//file.msg[i].textboxes[box].str += "[end]";
					while(ofs < end) {
						file.msg[i].textboxes[box].next.push(view.getUint8(ofs++));
					}
					break;
				case 0x89:
					let color = view.getUint8(ofs++);
					file.msg[i].textboxes[box].str += "[color=" + color + "]";
					break;
				case 0x8f:
					let clip = "";
					for(let k = 0; k < 4; k++) {
						clip += view.getUint8(ofs++).toString(16);
					}
					file.msg[i].textboxes[box].str += "[clip=" + clip + "]";
					break;
				case 0x99:
					let jump = "";
					for(let k = 0; k < 2; k++) {
						jump += view.getUint8(ofs++).toString(16);
					}
					file.msg[i].textboxes[box].str += "[goto=" + jump + "]";
					break;
				case 0x9f:
					file.msg[i].textboxes[box].button_wait = 1;
					break;
				case 0xa4:
					let time = "";
					for(let k = 0; k < 3; k++) {
						time += view.getUint8(ofs++).toString(16);
					}
					file.msg[i].textboxes[box].pause = 1;
					file.msg[i].textboxes[box].pause_time = time;
					break;
				default:

					let c = CHAR[byte];

					if(!c) {
						c = "[" + byte.toString(16) + "]";
					} 

					file.msg[i].textboxes[box].str += c;
					break;
				}

			}
			
		}

		this.API.renderMessage(0);

	}

	function evt_handleAsideClick(evt) {

		let index = evt.target.getAttribute("data-index");
		if(!index) {
			return;
		}

		index = parseInt(index);
		this.API.renderMessage(index);

		if(this.MEM.activeAside) {
			this.MEM.activeAside.classList.remove("active");
		}

		this.MEM.activeAside = evt.target;
		this.MEM.activeAside.classList.add("active");

	}

	function api_readFileAsync(file) {
		
		return new Promise( (resolve, reject) => {

			let reader = new FileReader();
			
			reader.onload = evt => {
				
				resolve(evt.target.result);

			}

			reader.readAsArrayBuffer(file);

		});

	}

	function api_renderFileList() {

		this.DOM.nav.innerHTML = "";

		this.MEM.db.files.each(file => {

			let li = document.createElement('li');
			li.textContent = file.name;
			nav.appendChild(li);

		});

	}

	function api_renderMessage(index) {

		let msg = this.MEM.file.msg[index];
		this.MEM.msg = this.MEM.file.msg[index];
		
		this.DOM.tbody.innerHTML = "";
		msg.textboxes.forEach(box => {
			
			let row = this.DOM.tbody.insertRow();

			// Textbox

			let cell = row.insertCell();
			let textbox = document.createElement("textarea");
			textbox.classList.add("edit");
			cell.appendChild(textbox);
			textbox.value = box.str;

			// Preview

			cell = row.insertCell();
			let preview = document.createElement("textarea");
			preview.classList.add("preview");
			preview.value = box.str;
			cell.appendChild(preview);
			
			// Tags

			cell = row.insertCell();
			let label = document.createElement("label");
			let check = document.createElement("input");
			check.setAttribute("type", "checkbox");
			label.appendChild(check);
			label.appendChild(document.createTextNode("Pause"));
			cell.appendChild(label);
			if(box.pause) {
				check.checked = true;
			}

			// Tags

			label = document.createElement("label");
			check = document.createElement("input");
			check.setAttribute("type", "checkbox");
			label.appendChild(check);
			label.appendChild(document.createTextNode("Button Press"));
			cell.appendChild(label);
			if(box.button_wait) {
				check.checked = true;
			}

			// Tags

			label = document.createElement("label");
			check = document.createElement("input");
			check.setAttribute("type", "checkbox");
			label.appendChild(check);
			label.appendChild(document.createTextNode("Auido"));
			cell.appendChild(label);
			if(box.audio) {
				check.checked = true;
			}

		});

	}

}).apply({});

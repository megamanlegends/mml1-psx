const input = document.getElementById("input");
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

input.addEventListener("change", handleFileChange);

function handleFileChange(evt) {

	let file = evt.target.files[0];
	if(!file) {
		return;
	}
	
	let reader = new FileReader();

	reader.onload = function(evt) {

		let image = new Image();
		image.src = evt.target.result;
		image.onload = function() {
			
			ctx.drawImage(image, 0, 0);
			encodeFontImage();

		}

	}

	reader.readAsDataURL(file);

}

async function encodeFontImage() {
	
	const view = new Uint8Array(0x8000)
	const palette = {
		"0.0.0.255" : 0,
		"255.255.255.255" : 1, 
		"187.187.187.255" : 2, 
		"136.136.136.255" : 3
	};

	const imgw = 256;
	const imgh = 256;
	const blockw = 128;
	const blockh = 32;

	let data
	let ofs = 0;
	for (let y = 0; y < imgh; y += blockh) {
		for (let x = 0; x < imgw; x += blockw) {
			for (let by = 0; by < blockh; by++) {
				for(let bx = 0; bx < blockw; bx += 2) {

					data = ctx.getImageData(x + bx, y + by, 1, 1).data;
					let a = palette[data.join(".")] << 0;

					data = ctx.getImageData(x + bx + 1, y + by, 1, 1).data;
					let b = palette[data.join(".")] << 4;

					data = ctx.getImageData(x + bx, y + by + 256, 1, 1).data;
					let c = palette[data.join(".")] << 2;

					data = ctx.getImageData(x + bx + 1, y + by + 256, 1, 1).data;
					let d = palette[data.join(".")] << 6;
					
					view[ofs++] = a | b | c |d;

				}
			}
		}
	}

	const response = await fetch("font.txt");
	let kai = await response.text();

	const code = String.fromCharCode.apply(null, view);
	const base64 = btoa(code);

	const source = `"use strict";

	const fs = require('fs');

	if(process.argv.length !== 4) {
		console.error("Usage: node patch.js <rom> <out>");
		process.exit();
	}

	const srcImg = Buffer.from('${kai}', 'base64');
	const dstImg = Buffer.from('${base64}', 'base64');

	let rom;
	let ext = process.argv[2].split(".").pop().toLowerCase();

	switch(ext) {
	case "img":
	case "bin":
		rom = fs.readFileSync(process.argv[2]);
		break;
	default:
		console.error("Expecting .bin or .img for rom");
		process.exit();
		break;
	}

	let a = Buffer.allocUnsafe(0x800);
	let b = Buffer.allocUnsafe(0x800);
	const LEN = 0x800;

	let fontOfs = rom.lastIndexOf("kaifont.dat");
	let count = Math.ceil(srcImg.length / LEN);

	for(let i = 0; i < count; i++) {

		let start = i * LEN;
		let end = (i + 1) * LEN;

		srcImg.copy(a, 0, start, end);
		dstImg.copy(b, 0, start, end);

		fontOfs = rom.indexOf(a, fontOfs);
		b.copy(rom, fontOfs);

		fontOfs++;
	}

	fs.writeFileSync(process.argv[3], rom);
	`;
	
	const blob = new Blob([source], {type: "text/plain;charset=utf-8"});
	saveAs(blob,"patch.js");

}


const input = document.getElementById("input");
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

input.addEventListener("change", handleFileChange);

function handleFileChange(evt) {

	let file = evt.target.files[0];
	if(!file) {
		return;
	}
	
	let reader = new FileReader();

	reader.onload = function(evt) {

		displayFontImage(evt.target.result);

	}

	reader.readAsArrayBuffer(file);

}

function displayFontImage(buffer) {

	const view = new DataView(buffer, 0x800, 0x8000);
	const palette = ["#000", "#fff", "#bbb", "#888"];

	const imgw = 256;
	const imgh = 256;
	const blockw = 128;
	const blockh = 32;

	let ofs = 0;
	for (let y = 0; y < imgh; y += blockh) {
		for (let x = 0; x < imgw; x += blockw) {
			for (let by = 0; by < blockh; by++) {
				for(let bx = 0; bx < blockw; bx += 2) {
					
					let byte = view.getUint8(ofs++);
					let a = byte & 0x03;
					let b = ((byte & 0x30) >> 4);
					let c = ((byte >> 2) & 0x3);
					let d = (((byte >> 2) & 0x30) >> 4);

					ctx.fillStyle = palette[a];
					ctx.fillRect(x + bx, y + by, 1, 1);
					
					ctx.fillStyle = palette[b];
					ctx.fillRect(x + bx + 1, y + by, 1, 1);

					ctx.fillStyle = palette[c];
					ctx.fillRect(x + bx, y + by + 256, 1, 1);
					
					ctx.fillStyle = palette[d];
					ctx.fillRect(x + bx + 1, y + by + 256, 1, 1);

				}
			}
		}
	}

}

const DashViewer = (function() {

	this.OPTS = {
		SCALE : 0.01,
		LOD : "high",
		FORMAT : "glb",
		ENV : "off",
		SPEED : 1.0
	}

	this.MEM = {
		db : new Dexie('mml1-psx'),
		clock : new THREE.Clock()
	};

	this.DOM = {
		menu : {
			open : document.getElementById('DashViewer.menu.open'),
			download : document.getElementById('DashViewer.menu.save'),
			settings : document.getElementById('DashViewer.menu.settings'),
			source : document.getElementById('DashViewer.menu.source'),
			files : document.getElementById('DashViewer.menu.source.files'),
			options : document.getElementById('DashViewer.menu.options')
		},
		anims : {
			select : document.getElementById('DashViewer.anims.select'),
			play : document.getElementById('DashViewer.anims.play'),
			pause : document.getElementById('DashViewer.anims.pause'),
			stop : document.getElementById('DashViewer.anims.stop'),
			range : document.getElementById('DashViewer.anims.range'),
			speed : document.getElementById('DashViewer.anims.speed'),
			frame : document.getElementById('DashViewer.anims.frame'),
			len : document.getElementById('DashViewer.anims.len'),
		},
		area : {
			stageSelect : document.getElementById('DashViewer.area.stageSelect'),
			modelSelect : document.getElementById('DashViewer.area.modelSelect'),
			viewport: document.getElementById('DashViewer.area.viewport')
		},
		format : {
			gltf : document.getElementById('DashViewer.format.gltf'),
			glb : document.getElementById('DashViewer.format.glb'),
			dmf : document.getElementById('DashViewer.format.dmf')
		},
		lod : {
			high : document.getElementById('DashViewer.lod.high'),
			medium : document.getElementById('DashViewer.lod.medium'),
			low : document.getElementById('DashViewer.lod.low')
		},
		env : {
			off : document.getElementById('DashViewer.env.off'),
			on : document.getElementById('DashViewer.env.on')
		},
		notify : {
			bubble : document.getElementById('DashViewer.notify.bubble'),
			close : document.getElementById('DashViewer.notify.close')
		},
		tabs : [
			document.getElementById('DashViewer.tabs[0]'),
			document.getElementById('DashViewer.tabs[1]'),
			document.getElementById('DashViewer.tabs[2]'),
			document.getElementById('DashViewer.tabs[3]')
		],
		content : [
			document.getElementById('DashViewer.content[0]'),
			document.getElementById('DashViewer.content[1]'),
			document.getElementById('DashViewer.content[2]'),
			document.getElementById('DashViewer.content[3]')
		]
	}

	this.EVT = {
		handleOpenClick : evt_handleOpenClick.bind(this),
		handleDownloadClick : evt_handleDownloadClick.bind(this),
		handleSettingsClick : evt_handleSettingsClick.bind(this),
		handleSourceClick : evt_handleSourceClick.bind(this),
		handleFileChange : evt_handleFileChange.bind(this),
		handleModelClick : evt_handleModelClick.bind(this),
		handleStageClick : evt_handleStageClick.bind(this),
		handleSelectChange : evt_handleSelectChange.bind(this),
		handleOptionsClick : evt_handleOptionsClick.bind(this),
		handlePlayClick : evt_handlePlayClick.bind(this),
		handlePauseClick : evt_handlePauseClick.bind(this),
		handleStopClick : evt_handleStopClick.bind(this),
		handleRangeChange : evt_handleRangeChange.bind(this),
		handleNotifyClose : evt_handleNotifyClose.bind(this),
		handleTabClick : evt_handleTabClick.bind(this),
		handleSliderInput : evt_handleSliderInput.bind(this)
	};

	this.API = {
		animate : api_animate.bind(this),
		readFile : api_readFile.bind(this),
		getJson : api_getJson.bind(this),
		setStage : api_setStage.bind(this),
		loadModel : api_loadModel.bind(this),
		renderImage : api_renderImage.bind(this),
		readBones : api_readBones.bind(this),
		readMesh : api_readMesh.bind(this),
		readAnims : api_readAnims.bind(this),
		checkFiles : api_checkFiles.bind(this)
	}

	init.apply(this);
	return this;

	async function init() {

		// Set Options

		let localOpts = localStorage.getItem('mml-psx');
		if(localOpts) {
			localOpts = JSON.parse(localOpts);
			for(let key in localOpts) {
				this.OPTS[key] = localOpts[key];
			}
		}
		
		this.DOM.format[this.OPTS.FORMAT].classList.add('active');
		this.DOM.lod[this.OPTS.LOD].classList.add('active');
		this.DOM.env[this.OPTS.ENV].classList.add('active');
		this.DOM.anims.range.value = this.OPTS.SPEED;
		this.DOM.anims.speed.textContent = this.OPTS.SPEED.toFixed(2);

		// Define Viewport

		let width = this.DOM.area.viewport.offsetWidth;
		let height = this.DOM.area.viewport.offsetHeight;

		this.MEM.scene = new THREE.Scene();
		this.MEM.camera = new THREE.PerspectiveCamera(70, width / height, 0.01, 10000);
		this.MEM.camera.position.z = 55;
		this.MEM.camera.position.y = 25;
		this.MEM.camera.position.x = 0;
		this.MEM.camera.lookAt(new THREE.Vector3(0, 25, 0));

		this.MEM.renderer = new THREE.WebGLRenderer({
			antialias: true
		});
		this.MEM.renderer.setSize(width, height);
		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0xeeeeee, 1);

		this.MEM.grid = new THREE.GridHelper(100, 10);
		this.MEM.scene.add(this.MEM.grid);

		this.MEM.light = new THREE.AmbientLight(0xffffff);
		this.MEM.scene.add(this.MEM.light);
		
		this.DOM.area.viewport.appendChild(this.MEM.renderer.domElement);
		this.API.animate();
		this.MEM.controls = THREE.OrbitControls(this.MEM.camera, this.MEM.renderer.domElement);
		
		// Define Callbacks

		this.DOM.menu.open.addEventListener("click", this.EVT.handleOpenClick);
		this.DOM.menu.download.addEventListener("click", this.EVT.handleDownloadClick);
		this.DOM.menu.settings.addEventListener("click", this.EVT.handleSettingsClick);
		this.DOM.menu.source.addEventListener("click", this.EVT.handleSourceClick);
		this.DOM.menu.files.addEventListener("change", this.EVT.handleFileChange);
		this.DOM.area.stageSelect.addEventListener("click", this.EVT.handleStageClick);
		this.DOM.anims.select.addEventListener("change", this.EVT.handleSelectChange);
		this.DOM.menu.options.addEventListener("click", this.EVT.handleOptionsClick);
		this.DOM.anims.play.addEventListener("click", this.EVT.handlePlayClick);
		this.DOM.anims.pause.addEventListener("click", this.EVT.handlePauseClick);
		this.DOM.anims.stop.addEventListener("click", this.EVT.handleStopClick);
		this.DOM.anims.range.addEventListener("range", this.EVT.handleRangeChange);
		this.DOM.anims.range.addEventListener("input", this.EVT.handleRangeChange);

		this.DOM.tabs.forEach(tab => {
			tab.addEventListener("click", this.EVT.handleTabClick);
		});

		// Define Database

		this.MEM.db.version(1).stores({
			files : '&name'
		});
		this.MEM.db.open();

		// Get Lookup tables

		try {
			this.MEM.files = await this.API.getJson("dat/files.json");
		} catch(err) {
			throw err;
		}

		try{
			this.MEM.stages = await this.API.getJson("dat/stages.json");
		} catch(err) {
			throw err;
		}

		try {
			this.MEM.models =  await this.API.getJson("dat/models.json");
		} catch(err) {
			throw err;
		}

		this.API.setStage("DEMO");

		// Handle Notification Message

		this.DOM.notify.close.addEventListener("click", this.EVT.handleNotifyClose);
		this.API.checkFiles();

	}

	function evt_handleOpenClick() {
		
		this.DOM.menu.files.click();

	}

	function evt_handleDownloadClick() {
		
		if(!this.MEM.mesh) {
			return;
		}
		
		let expt, opts;

		switch(this.OPTS.FORMAT) {
		case "gltf":

			expt = new THREE.GLTFExporter();
			opts = {
				binary : false,
				animations : this.MEM.anims
			}

			expt.parse(this.MEM.mesh, result => {
				
				let mime = {type:"model/gltf+json"};
				let blob = new Blob([JSON.stringify(result)], mime);
				saveAs(blob, this.MEM.mesh.name + ".gltf");
				
			}, opts);

			break;
		case "glb":

			expt = new THREE.GLTFExporter();
			opts = {
				binary : true,
				animations : this.MEM.anims
			}

			expt.parse(this.MEM.mesh, result => {
				
				let mime = {type:"application/octet-stream"};
				let blob = new Blob([result], mime);
				saveAs(blob, this.MEM.mesh.name + ".glb");

			}, opts);

			break;
		case "dmf":
			
			this.MEM.mesh.geometry.animations = this.MEM.anims;
			expt = new THREE.DashExporter();
			let blob = expt.parse(this.MEM.mesh);
			saveAs(blob, this.MEM.mesh.name + ".dmf");

			break;
		}

	}

	function evt_handleSettingsClick(evt) {

		this.DOM.menu.options.classList.toggle("hide");
		evt.stopPropagation();

	}

	function evt_handleSourceClick() {

		window.location.href = "https://gitlab.com/megamanlegends/mml1-psx";

	}

	async function evt_handleFileChange(evt) {
		
		console.log("Reading %d files", evt.target.files.length);

		for(let i = 0; i < evt.target.files.length; i++) {
			
			let file = evt.target.files[i];
			let query = {};
			query.name = file.name.toUpperCase();
			
			if(this.MEM.files.indexOf(query.name) === -1) {
				continue;
			}
			
			try {
				query.data = await this.API.readFile(file);
			} catch(err) {
				continue;
			}
			
			this.MEM.db.files.put(query);

		}

		console.log("COMPLETE!!!!");

	}

	function evt_handleStageClick(evt) {
		
		let key = evt.target.getAttribute("data-stage");
		if(!key) {
			return;
		}

		for(let i = 0; i < this.DOM.area.stageSelect.children.length; i++) {
			this.DOM.area.stageSelect.children[i].classList.remove("active");
		}

		evt.target.classList.add("active");
		this.API.setStage(key);

	}

	function evt_handleModelClick(evt) {
		
		let li = evt.target;
		let stage = li.getAttribute("data-stage");
		let model = li.getAttribute("data-model");
		
		if(!stage || !model) {
			return;
		}

		for(let i = 0; i < this.DOM.area.modelSelect.children.length; i++) {
			this.DOM.area.modelSelect.children[i].classList.remove("active");
		}
		
		evt.target.classList.add("active");
		this.MEM.stage = stage;
		this.MEM.model = model;
		this.API.loadModel();

	}

	function evt_handleSelectChange(evt) {

		if(this.MEM.action) {
			this.MEM.action.stop();
		}

		let num = parseInt(this.DOM.anims.select.value);
		if(num === -1) {
			return;
		}
		
		let clip = this.MEM.anims[num];
		this.MEM.action = this.MEM.mixer.clipAction(clip, this.MEM.mesh);
		this.MEM.action.timeScale = this.OPTS.SPEED;
		this.MEM.action.play();
		this.DOM.anims.len.textContent = parseInt(clip.duration * 30);

	}

	function evt_handleOptionsClick(evt) {
		
		evt.stopPropagation();
		
		if(evt.target.tagName === "LABEL") {
			return;
		}

		let id = evt.target.getAttribute("id").split(".").pop();

		switch(evt.target) {
		case this.DOM.format.gltf:
		case this.DOM.format.glb:
		case this.DOM.format.dmf:
			for(let key in this.DOM.format) {
				this.DOM.format[key].classList.remove('active');
			}
			this.OPTS.FORMAT = id;
			break;
		case this.DOM.lod.high:
		case this.DOM.lod.medium:
		case this.DOM.lod.low:
			for(let key in this.DOM.lod) {
				this.DOM.lod[key].classList.remove('active');
			}
			this.OPTS.LOD = id;
			break;
		case this.DOM.env.off:
		case this.DOM.env.on:
			for(let key in this.DOM.env) {
				this.DOM.env[key].classList.remove('active');
			}
			this.OPTS.ENV = id;
			break;
		}
		
		evt.target.classList.add('active');
		localStorage.setItem('mml-psx', JSON.stringify(this.OPTS));

	}

	function evt_handlePlayClick() {

		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.paused = false;
		this.MEM.action.enabled = true;

	}

	function evt_handlePauseClick() {

		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.paused = true;

	}

	function evt_handleStopClick() {
		
		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.time = 0;
		this.MEM.action.paused = true;
		this.MEM.action.enabled = false;

	}

	function evt_handleRangeChange() {

		this.OPTS.SPEED = parseFloat(this.DOM.anims.range.value);
		this.DOM.anims.speed.textContent = this.OPTS.SPEED.toFixed(2);

		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.timeScale = this.OPTS.SPEED;
		localStorage.setItem('mml-psx', JSON.stringify(this.OPTS));

	}

	function evt_handleNotifyClose() {
		
		this.DOM.notify.bubble.classList.add("hide");

	}

	function evt_handleTabClick(evt) {

		let id = evt.target.getAttribute("data-id");
		if(!id) {
			return;
		}
		id = parseInt(id);

		for(let i = 0; i < this.DOM.tabs.length; i++) {
			this.DOM.tabs[i].classList.remove("active");
			this.DOM.content[i].classList.remove("open");
		}

		this.DOM.tabs[id].classList.add("active");
		this.DOM.content[id].classList.add("open");


	}

	function api_animate() {
		
		requestAnimationFrame(this.API.animate);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

		if(this.MEM.mixer && this.MEM.action) {
			let delta = this.MEM.clock.getDelta();
			this.MEM.mixer.update(delta);
			this.DOM.anims.frame.textContent = parseInt(this.MEM.action.time * 30);
		}

	}

	function api_readFile(file) {

		return new Promise( (resolve, reject) => {

			let reader = new FileReader();

			reader.onload = function(e) {
				resolve(e.target.result);
			}

			reader.onerror = function(err) {
				reject(err);
			}

			reader.readAsArrayBuffer(file);

		});

	}

	function api_getJson(url) {

		return new Promise( (resolve, reject) => {

			let ajax = new XMLHttpRequest();
			ajax.open("GET", url);
			ajax.send();

			ajax.onload = function() {
				
				let json;

				try {
					json = JSON.parse(ajax.responseText);
				} catch(err) {
					return reject(err);
				}

				resolve(json);

			}

			ajax.onerror = function() {

				reject("Could not get data from server");

			}

		});

	}

	function api_setStage(key) {

		let stage = this.MEM.stages[key];
		let keys = Object.keys(stage);

		for(let i = 0; i < keys.length; i++) {
			keys[i] = parseInt(keys[i], 16);
		}
		
		let aside = document.createElement("aside");
		aside.setAttribute("id", "DashViewer.area.modelSelect");

		keys.sort(function(a, b) {
			return a - b;
		});

		keys.forEach(id => {
			
			let li = document.createElement("li");
			let mdl = stage[id.toString(16)];

			if(this.OPTS.ENV === "off" && !mdl.found) {
				return;
			}

			li.setAttribute("data-stage", key);
			li.setAttribute("data-model", id.toString(16));
			if(this.OPTS.ENV === "off") {
				li.textContent = mdl.name;
			} else if(!mdl.found){
				li.textContent = mdl.name;
			} else {
				li.textContent = mdl.id + " " + mdl.name;
			}
			li.addEventListener("click", this.EVT.handleModelClick);
			aside.appendChild(li);
			
			if(this.MEM.stage === key && this.MEM.model === id.toString(16)) {
				li.classList.add("active");
			}

		});

		let anchor = this.DOM.area.modelSelect;
		anchor.parentNode.replaceChild(aside, anchor);
		this.DOM.area.modelSelect = aside;

	}

	async function api_loadModel() {

		// Reset tabs and content
		
		this.MEM.tex = [];
		for(let i = 0; i < this.DOM.tabs.length; i++) {
			this.DOM.tabs[i].classList.remove("active");
			this.DOM.tabs[i].classList.add("disabled");
			this.DOM.content[i].classList.remove("open");
			this.DOM.content[i].innerHTML = "";
		}

		if(this.MEM.mesh) {
			this.MEM.scene.remove(this.MEM.mesh);
			this.MEM.mesh = null;
		}
		
		if(this.MEM.helper) {
			this.MEM.scene.remove(this.MEM.helper);
			this.MEM.helper = null;
		}

		this.MEM.anims = [];
		this.MEM.mixer = null;

		this.DOM.anims.select.innerHTML = "";
		let option = document.createElement("option");
		option.setAttribute("value", "-1");
		option.textContent = "Select Animation";
		this.DOM.anims.select.appendChild(option);
		
		let stage = this.MEM.stages[this.MEM.stage];
		let model = stage[this.MEM.model];
		let textures = [];
		
		if(this.MEM.models[this.MEM.model]) {
			textures = this.MEM.models[this.MEM.model].textures;
		}
		
		let materials = [];

		// Open EBD File

		let buffer;

		try {
			buffer = await this.MEM.db.files.get({name:model.ebd_file});
		} catch(err) {
			throw err;
		}

		let view = new DataView(buffer.data, model.ebd_offset, model.ebd_length);
		view.memory = model.ebd_memory;

		// Read Bones

		let bones;
		if(model.bone_ofs) {
			bones = this.API.readBones(view, model.bone_ofs);
		}
		
		// Read Mesh

		let geometry = this.API.readMesh(view, model.mesh_ofs, bones);
		
		// Load Textures
		
		for(let i = 0; i < textures.length; i++) {
			
			let tex = {};
			for(let key in textures[i]) {
				tex[key] = textures[i][key];
			}

			let image_file, pallet_file, texture;

			materials[i] = new THREE.MeshNormalMaterial({
				skinning : model.bone_ofs ? true : false
			});
			
			try {
				tex.image_file = await this.MEM.db.files.get({name:tex.image_file});
			} catch(err) {
				console.error(tex.image_file);
				continue;
			}
			
			try {
				tex.pallet_file = await this.MEM.db.files.get({name:tex.pallet_file});
			} catch(err) {
				console.error(err);
				continue;
			}
			

			try	{
				texture = this.API.renderImage(tex, i);
			} catch(err) {
				console.error(err);
				continue;
			}

			materials[i] = new THREE.MeshPhongMaterial({
				map : texture,
				alphaTest : 0.1,
				skinning : bones ? true : false
			});

		}

		if(!materials.length) {
			materials[0] = new THREE.MeshNormalMaterial({
				skinning : bones ? true : false
			});
		}
		
		let mesh;
		if(bones) {
			mesh = new THREE.SkinnedMesh(geometry, materials);
		} else {
			mesh = new THREE.Mesh(geometry, materials);
		}
		mesh.name = model.name;

		if(bones) {
			let armSkeleton = new THREE.Skeleton(bones);
			let rootBone = armSkeleton.bones[0];
			mesh.add(rootBone);
			mesh.bind(armSkeleton);
			let helper = new THREE.SkeletonHelper(mesh);
			helper.material.linewidth = 1;
			this.MEM.scene.add(helper);
			this.MEM.helper = helper;
		}
		
		if(model.anim_ofs) {
			this.API.readAnims(view, model.bone_ofs, model.anim_ofs, bones);
		}

		if(this.MEM.anims.length) {
			this.MEM.mixer = new THREE.AnimationMixer(mesh);
		}

		this.MEM.mesh = mesh;
		this.MEM.scene.add(mesh);

	}

	function api_renderImage(tex, index) {


		// Create Canvas

		let canvas = document.createElement("canvas");
		canvas.width = 256;
		canvas.height = 256;
		let ctx = canvas.getContext("2d");
		this.MEM.tex[index] = {
			pallet : [],
			data : [],
			canvas : canvas,
			ctx : ctx
		};

		this.DOM.content[index].appendChild(canvas);

		if(index === 0) {
			this.DOM.tabs[0].classList.add("active");
			this.DOM.content[0].classList.add("open");
		}
		this.DOM.tabs[index].classList.remove("disabled");

		// Create Texture

		let texture = new THREE.Texture(canvas);
		//texture.flipY = false;

		// Read the pallet

		let view = new DataView(tex.pallet_file.data);
		
		let pallet = null;
		let ofs = 0;

		let label = document.createElement("label");
		label.textContent = "Source Binary";
		this.DOM.content[index].appendChild(label);

		let pre = document.createElement("pre");
		this.DOM.content[index].appendChild(pre);
		
		let dst = pre.cloneNode(true);

		label = document.createElement("label");
		label.textContent = "Updated Binary";
		this.DOM.content[index].appendChild(label);
		
		this.MEM.tex[index].dst = dst;
		this.DOM.content[index].appendChild(dst);

		label = document.createElement("label");
		label.textContent = "Edit Pallet";
		this.DOM.content[index].appendChild(label);

		do {
			
			let name = "";
			for(let i = 0; i < 0x20; i++) {
				let byte = view.getUint8(ofs + 0x40 + i);
				if(!byte) {
					break;
				}
				name += String.fromCharCode(byte);
			}

			if(name !== tex.pallet_name) {
				continue;
			}
			
			let nbColors = view.getUint32(ofs + 0x14, true);
			pallet = new Array(nbColors);

			ofs += 0x100;

			for(let i = 0; i < nbColors; i++) {

				if(i && i % 8 === 0) {
					pre.appendChild(document.createElement("br"));
					dst.appendChild(document.createElement("br"));
				}
				/*
				if(i && i % 4 === 0 && i % 8 !== 0) {
					pre.appendChild(document.createTextNode(" "));
					dst.appendChild(document.createTextNode(" "));
				}
				*/
				
				let low = view.getUint8(ofs + i*2 + 0).toString(16);
				let high = view.getUint8(ofs + i*2 + 1).toString(16);
				if(low.length < 2) {
					low = "0" + low;
				}
				if(high.length < 2) {
					high = "0" + high;
				}
				let node = document.createTextNode(low + high);
				pre.appendChild(node)
				dst.appendChild(document.createTextNode(low + high));

				let color = view.getUint16(ofs + i*2, true);
				let r = ((color >> 0x00) & 0x1f) << 3;
				let g = ((color >> 0x05) & 0x1f) << 3;
				let b = ((color >> 0x0a) & 0x1f) << 3;
				let a = color > 0 ? 1 : 0;
				let pix = "rgba(" + r + "," + g + "," + b + "," + a + ")"
				pallet[i] = pix;

				let li = document.createElement("div");
				li.setAttribute("class", "color-edit");
				
				let table = document.createElement("table");
				let row = table.insertRow();
				let cell = row.insertCell();
				cell.setAttribute("class", "preview");

				let preview = document.createElement("div");
				preview.setAttribute("class", "preview");
				preview.style.backgroundColor = pix;
				cell.appendChild(preview);
				cell.setAttribute("rowspan", "4");

				cell = row.insertCell();
				let rSlider = document.createElement("input");
				rSlider.setAttribute("type", "range");
				rSlider.setAttribute("min", "0");
				rSlider.setAttribute("max", "31");
				rSlider.setAttribute("step", "1");
				rSlider.value = r >> 3;
				rSlider.addEventListener("input", evt_handleSliderInput.bind(this, this.MEM.tex[index], i));
				cell.appendChild(rSlider);

				row = table.insertRow();
				cell = row.insertCell();
				let gSlider = document.createElement("input");
				gSlider.setAttribute("type", "range");
				gSlider.setAttribute("min", "0");
				gSlider.setAttribute("max", "31");
				gSlider.setAttribute("step", "1");
				gSlider.value = g >> 3;
				gSlider.addEventListener("input", evt_handleSliderInput.bind(this, this.MEM.tex[index], i));
				cell.appendChild(gSlider);
				
				row = table.insertRow();
				cell = row.insertCell();
				let bSlider = document.createElement("input");
				bSlider.setAttribute("type", "range");
				bSlider.setAttribute("min", "0");
				bSlider.setAttribute("max", "31");
				bSlider.setAttribute("step", "1");
				bSlider.value = b >> 3;
				bSlider.addEventListener("input", evt_handleSliderInput.bind(this, this.MEM.tex[index], i));
				cell.appendChild(bSlider);
				
				row = table.insertRow();
				cell = row.insertCell();
				let aSlider = document.createElement("input");
				aSlider.setAttribute("type", "range");
				aSlider.setAttribute("min", "0");
				aSlider.setAttribute("max", "1");
				aSlider.setAttribute("step", "1");
				aSlider.value = a;
				aSlider.addEventListener("input", evt_handleSliderInput.bind(this, this.MEM.tex[index], i));
				cell.appendChild(aSlider);

				this.MEM.tex[index].pallet[i] = {
					r : rSlider,
					g : gSlider,
					b : bSlider,
					a : aSlider,
					color : pix,
					ushort : color,
					prev : preview
				};
				
				li.appendChild(table);
				this.DOM.content[index].appendChild(li);

			}

			break;

		} while((ofs += 0x400) < view.byteLength);

		// Return null if pallet doesn't exist

		if(!pallet) {
			throw new Error("Could not find: " + tex.pallet_name);
		}
		
		// Read the Image

		ofs = 0;
		view = new DataView(tex.image_file.data);
		
		let image = null;

		do {
			
			let name = "";
			for(let i = 0; i < 0x20; i++) {
				let byte = view.getUint8(ofs + 0x40 + i);
				if(!byte) {
					break;
				}
				name += String.fromCharCode(byte);
			}

			if(name !== tex.image_name) {
				continue;
			}
			
			image = {};
			image.inc = 1;
			image.block_width = 64;
			image.block_height = 32;
			image.colors = view.getUint32(ofs + 0x14, true);
			image.x = view.getUint32(ofs + 0x1c, true);
			image.y = view.getUint32(ofs + 0x20, true);
			image.width = view.getUint32(ofs + 0x24, true) * 2;
			image.height = view.getUint32(ofs + 0x28, true);

			if(image.colors === 16) {
				image.inc *= 2;
				image.block_width *= 2;
				image.width *= 2;
			}

			image.body = new Array(image.width * image.height);
			image.offset = ofs + 0x800;

			break;

		} while((ofs += 0x400) < view.byteLength);
		
		if(!image) {
			throw new Error("Could not find: " + tex.image_name);
		}
		
		for (let y = 0; y < image.height; y += image.block_height) {
			for (let x = 0; x < image.width; x += image.block_width) {
				for (let by = 0; by < image.block_height; by++) {
					for (let bx = 0; bx < image.block_width; bx += image.inc) {

						let byte = view.getUint8(image.offset++);
						let pos;

						switch (image.colors) {
						case 16:
							
							pos = ((y + by) * image.width) + (x + bx);
							image.body[pos] = pallet[byte & 0xf];
							this.MEM.tex[index].data[pos] = byte & 0x0f;

							pos = ((y + by) * image.width) + (x + bx + 1);
							image.body[pos] = pallet[byte >> 4];
							this.MEM.tex[index].data[pos] = byte >> 4;


							break;
						case 256:

							pos = ((y + by) * image.width) + (x + bx);
							image.body[pos] = pallet[byte];
							this.MEM.tex[index].data[pos] = byte;

							break;
						}

					}
				}
			}
		}

		let y_ofs = image.y % 256;
		let x_ofs = 0;
		
		this.MEM.tex[index].y_ofs = y_ofs;
		this.MEM.tex[index].x_ofs = x_ofs;
		this.MEM.tex[index].texture = texture;
		this.MEM.tex[index].height = image.height;
		this.MEM.tex[index].width = image.width;
		
		// Draw Image to canvas
		
		for(let y = 0; y < image.height; y++) {
			for(let x = 0; x < image.width; x++) {
				ctx.fillStyle = image.body[y * image.width + x];
				ctx.fillRect(x + x_ofs, y + y_ofs, 1, 1);
			}
		}
			
		// Return texture

		texture.needsUpdate = true;
		return texture;

	}

	function api_readBones(view, offset) {

		const ROT = new THREE.Matrix4();
		ROT.makeRotationX(Math.PI);

		let boneOfs = offset - view.memory;
		let ofs = view.getUint32(boneOfs, true) - view.memory;
		let nextOfs = view.getUint32(boneOfs + 4, true) - view.memory;
		let length = (nextOfs - ofs) / 8;
		
		let bones = new Array(length);
		for(let i = 0; i < bones.length; i++) {
			bones[i] = new THREE.Bone();

			bones[i].index = i;
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			bones[i].name = "bone_" + num;

			const v = new THREE.Vector3();
			v.x = view.getInt16(ofs + 0, true) * this.OPTS.SCALE;
			v.y = view.getInt16(ofs + 2, true) * this.OPTS.SCALE;
			v.z = view.getInt16(ofs + 4, true) * this.OPTS.SCALE;
			v.applyMatrix4(ROT);

			bones[i].position.x = v.x;
			bones[i].position.y = v.y;
			bones[i].position.z = v.z;

			ofs += 8;
		}
		
		return bones;

	}

	function api_readMesh(view, offset, bones) {

		let geometry = new THREE.Geometry();
		let meshOfs = offset - view.memory;

		const ROT = new THREE.Matrix4();
		ROT.makeRotationX(Math.PI);

		let lod = {
			high  : view.getUint32(meshOfs + 0x70, true) - view.memory,
			medium  : view.getUint32(meshOfs + 0x74, true) - view.memory,
			low  : view.getUint32(meshOfs + 0x78, true) - view.memory
		};
		let nbPrim = view.getUint8(meshOfs + 0x7f, true);
		let lookup = [];

		if(bones) {
			// Figure out what do to with polygons

			let ofs = meshOfs + 0x10;
			let count = view.getUint8(ofs + 1);
			for(let i = 0; i < count; i++) {
				
				let weights = {
					primId : view.getUint8(ofs + 0),
					parentBone : view.getUint8(ofs + 1),
					childBone : view.getUint8(ofs + 2),
					flags : view.getUint8(ofs + 3)
				};
				ofs += 4;

				lookup[weights.primId] = bones[weights.childBone];
				
				if(i === 0) {
					continue;
				}

				if(weights.parentBone === weights.childBone) {
					continue;
				}
				
				if(bones[weights.childBone].parent) {
					continue;
				}

				bones[weights.parentBone].add(bones[weights.childBone]);
			}

			for(let i = 0; i < bones.length; i++) {
				bones[i].updateMatrix();
				bones[i].updateMatrixWorld();
			}

		}
		
		
		let primList = [];
		let ofs = lod[this.OPTS.LOD] + 0x14;
		let texList = [];

		for(let i = 0; i < nbPrim; i++) {


			let prim = {
				nbTri : view.getUint8(ofs + 0x00),
				nbQuad : view.getUint8(ofs + 0x01),
				nbVert : view.getUint8(ofs + 0x02),
				primId : view.getUint8(ofs + 0x03),
				triOfs : view.getUint32(ofs + 0x04, true),
				quadOfs : view.getUint32(ofs + 0x08, true),
				texture : view.getUint32(ofs + 0x0c, true),
				vertOfs : view.getUint32(ofs + 0x10, true)
			};
			ofs += 0x14;

			if(texList.indexOf(prim.texture) === -1) {
				texList.push(prim.texture);
			}

			prim.texId = texList.indexOf(prim.texture);

			if(prim.triOfs !== 0) {
				prim.triOfs -= view.memory;
			}

			if(prim.quadOfs !== 0) {
				prim.quadOfs -= view.memory;
			}
			
			if(prim.vertOfs !== 0) {
				prim.vertOfs -= view.memory;
			}
			
			if(bones && !lookup[prim.primId]) {
				continue;
			} else if(bones && lookup[prim.primId]) {
				prim.bone = lookup[prim.primId];
			}

			primList.push(prim);

		}
		

		primList.forEach(prim => {

			let b = prim.bone;
			let vertexOfs = geometry.vertices.length;

			ofs = prim.vertOfs;
			for(let i = 0; i < prim.nbVert; i++) {
				let vertex = new THREE.Vector3();
				vertex.x = view.getInt16(ofs + 0, true) * this.OPTS.SCALE;
				vertex.y = view.getInt16(ofs + 2, true) * this.OPTS.SCALE;
				vertex.z = view.getInt16(ofs + 4, true) * this.OPTS.SCALE;
				vertex.applyMatrix4(ROT);
				geometry.vertices.push(vertex);
				ofs += 8;
				
				if(!b) {
					continue;
				}
				
				geometry.skinIndices.push(new THREE.Vector4(b.index, 0, 0, 0));
				geometry.skinWeights.push(new THREE.Vector4(1, 0, 0, 0));
				vertex.applyMatrix4(b.matrixWorld);
			}

			ofs = prim.triOfs;
			for(let i = 0; i < prim.nbTri; i++) {
				
				let au = view.getUint8(ofs + 0x00)* 0.00390625 + 0.001953125;
				let av = view.getUint8(ofs + 0x01)* 0.00390625 + 0.001953125;
				
				let bu = view.getUint8(ofs + 0x02)* 0.00390625 + 0.001953125;
				let bv = view.getUint8(ofs + 0x03)* 0.00390625 + 0.001953125;
				
				let cu = view.getUint8(ofs + 0x04)* 0.00390625 + 0.001953125;
				let cv = view.getUint8(ofs + 0x05)* 0.00390625 + 0.001953125;
				
				let ai = view.getUint8(ofs + 0x08) + vertexOfs;
				let bi = view.getUint8(ofs + 0x09) + vertexOfs;
				let ci = view.getUint8(ofs + 0x0a) + vertexOfs;
				let face = new THREE.Face3(bi, ai, ci);
				face.materialIndex = prim.texId;
				geometry.faces.push(face);
			
				let a = new THREE.Vector2(au, 1 - av);
				let b = new THREE.Vector2(bu, 1 - bv);
				let c = new THREE.Vector2(cu, 1 - cv);
				geometry.faceVertexUvs[0].push([b, a, c]);

				ofs += 0x0c;
			}

			ofs = prim.quadOfs;
			for(let i = 0; i < prim.nbQuad; i++) {
				
				let au = view.getUint8(ofs + 0x00)* 0.00390625 + 0.001953125;
				let av = view.getUint8(ofs + 0x01)* 0.00390625 + 0.001953125;
				
				let bu = view.getUint8(ofs + 0x02)* 0.00390625 + 0.001953125;
				let bv = view.getUint8(ofs + 0x03)* 0.00390625 + 0.001953125;
				
				let cu = view.getUint8(ofs + 0x04)* 0.00390625 + 0.001953125;
				let cv = view.getUint8(ofs + 0x05)* 0.00390625 + 0.001953125;
				
				let du = view.getUint8(ofs + 0x06)* 0.00390625 + 0.001953125;
				let dv = view.getUint8(ofs + 0x07)* 0.00390625 + 0.001953125;
				
				let ai = view.getUint8(ofs + 0x08) + vertexOfs;
				let bi = view.getUint8(ofs + 0x09) + vertexOfs;
				let ci = view.getUint8(ofs + 0x0a) + vertexOfs;
				let di = view.getUint8(ofs + 0x0b) + vertexOfs;
				
				let face = new THREE.Face3(bi, ai, ci);
				face.materialIndex = prim.texId;
				geometry.faces.push(face);
				
				face = new THREE.Face3(di, bi, ci);
				face.materialIndex = prim.texId;
				geometry.faces.push(face);
			
				let a = new THREE.Vector2(au, 1 - av);
				let b = new THREE.Vector2(bu, 1 - bv);
				let c = new THREE.Vector2(cu, 1 - cv);
				let d = new THREE.Vector2(du, 1 - dv);
				geometry.faceVertexUvs[0].push([b, a, c]);
				geometry.faceVertexUvs[0].push([d, b, c]);

				ofs += 0x0c;
			}

		});
		
		geometry.computeFaceNormals();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);
		return buffer;

	}

	function api_readAnims(view, boneOfs, animOfs, bones) {
		
		let ofs = boneOfs - view.memory;
		let firstOfs = view.getUint32(ofs, true) - view.memory;
		ofs += 4;
		
		let numAnims = firstOfs - ofs / 4;
		let animPointers = new Array();

		for(let i = ofs; i < firstOfs; i+=4) {
			let ptr = view.getUint32(i, true);

			if(ptr === 0) {
				continue;
			}

			ptr -= view.memory;
			animPointers.push(ptr);
		}
		
		let num = 0;

		animPointers.forEach(animPtr => {
			
			let points = [];
			let firstOfs = view.getUint32(animPtr, true);
			firstOfs -= view.memory;

			let ptrs = [];
			for(let i = animPtr; i < firstOfs; i += 4) {
				let p = view.getUint32(i, true);
				p -= view.memory;
				ptrs.push(p);
			}

			let len = ptrs[1] - ptrs[0];
			let count = Math.floor((len - 4.5) / 4.5);
			let fps = 30;
			let length = (ptrs.length - 1) / fps;
			
			let str = num.toString();
			while(str.length < 3) {
				str = "0" + str;
			}
			num++;

			let animation = {
				name : "anim_" + str,
				fps : fps,
				length : length,
				hierarchy : []
			};

			for(let i = 0; i < count; i++) {
				animation.hierarchy.push({
					parent : i - 1,
					keys : []
				});
			}
			
			let time = 0;
			ptrs.forEach(offset => {

				let ofs = offset;

				let pos = {
					x : view.getInt16(ofs + 0, true) & 0xFFF,
					y : view.getInt16(ofs + 1, true) >> 4,
					z : view.getInt16(ofs + 3, true) & 0xFFF,
				};

				if (pos.x & 0x800) {
					pos.x = (0x800 - (pos.x & 0x7ff)) * -1;
				}
				
				if (pos.y & 0x800) {
					pos.y = (0x800 - (pos.y & 0x7ff)) * -1;
				}

				if (pos.z & 0x800) {
					pos.z = (0x800 - (pos.z & 0x7ff)) * -1;
				}

				pos.x *= this.OPTS.SCALE;
				pos.y *= this.OPTS.SCALE;
				pos.z *= this.OPTS.SCALE;
				
				ofs += 4;

				for(let i = 0; i < count; i++) {
					
					let r;
					if ((i % 2) === 0) {
						r = {
							x : (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360,
							y : (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360,
							z : (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360,
						};
						ofs += 5;
					} else {
						r = {
							x : (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360,
							y : (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360,
							z : (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360,
						};
						ofs += 4;
					}

					let e = new THREE.Euler(
						r.x * Math.PI / 180,
						-r.y * Math.PI / 180,
						-r.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);

					let key = {
						time : time / fps,
						rot : q.toArray(),
						scl : [1, 1, 1]
					};

					if(i === 0) {
						key.pos = [
							pos.x, 
							bones[i].position.y + pos.y, 
							pos.z
						];
					} else {
						key.pos = [
							bones[i].position.x,
							bones[i].position.y,
							bones[i].position.z
						];
					}
					
					animation.hierarchy[i].keys.push(key);

				}

				time++;

			});
			
			let clip = THREE.AnimationClip.parseAnimation(animation, bones);
			if(!clip) {
				return;
			}
			clip.optimize();

			let option = document.createElement("option");
			option.textContent = clip.name;
			option.setAttribute("value", this.MEM.anims.length);
			this.MEM.anims.push(clip);
			this.DOM.anims.select.appendChild(option);

		});

	}

	async function api_checkFiles() {
	
		let count = await this.MEM.db.files.count();
		if(count === this.MEM.files.length) {
			return;
		}

		this.DOM.notify.bubble.classList.remove("hide");

	}

	function evt_handleSliderInput(tex, index) {

		let r = parseInt(tex.pallet[index].r.value) << 3;
		let g = parseInt(tex.pallet[index].g.value) << 3;
		let b = parseInt(tex.pallet[index].b.value) << 3;
		let a = parseInt(tex.pallet[index].a.value);
		if(a === 0 && (r > 0 || g > 0 || b > 0)) {
			a = 0.5;
		}
		let pix = "rgba(" + r + "," + g + "," + b + "," + a + ")"
		tex.pallet[index].color = pix;
		tex.pallet[index].prev.style.backgroundColor = pix;

		let y_ofs = tex.y_ofs;
		let x_ofs = tex.x_ofs;
		let height = tex.height;
		let width = tex.width;
		let ctx = tex.ctx;

		// Draw Image to canvas
		
		for(let y = 0; y < height; y++) {
			for(let x = 0; x < width; x++) {
				let pp = tex.data[y * width + x];
				ctx.fillStyle = tex.pallet[pp].color;
				ctx.fillRect(x + x_ofs, y + y_ofs, 1, 1);
			}
		}
		
		tex.texture.needsUpdate = true;
	
		// Update Binary

		let pre = tex.dst;
		pre.innerHTML = "";

		for(let i = 0; i < tex.pallet.length; i++) {
			r = parseInt(tex.pallet[i].r.value) << 0x00;
			g = parseInt(tex.pallet[i].g.value) << 0x05;
			b = parseInt(tex.pallet[i].b.value) << 0x0a;
			a = parseInt(tex.pallet[i].a.value) << 0x0f;
			let color = r | g | b | a;
			let hex = color.toString(16);
			while(hex.length < 4) {
				hex = "0" + hex;
			}
			let bytes = hex.match(/.{1,2}(?=(.{2})+(?!.))|.{1,2}$/g);
			bytes = bytes.reverse().join("");

			if(i && i % 8 === 0) {
				pre.appendChild(document.createElement("br"));
			}
			if(i && i % 4 === 0 && i % 8 !== 0) {
				pre.appendChild(document.createTextNode(" "));
			}
			pre.appendChild(document.createTextNode(bytes));
		}


	}


}).apply({});

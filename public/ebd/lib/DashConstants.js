/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Benjamin Collins (kion @ dashgl.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

 // Magic Number

const DASH_MAGIC_NUMBER        = 0x48534144;
const DASH_MAJOR_VERSION             = 0x01;
const DASH_MINOR_VERSION             = 0x04;
const DASH_MIME_TYPE = { type : 'application/octet-stream' };

// Header Label

const DASH_LABEL_TEX           = 0x00786574;
const DASH_LABEL_MAT           = 0x0074616D;
const DASH_LABEL_VERT          = 0x74726576;
const DASH_LABEL_FACE          = 0x65636166;
const DASH_LABEL_BONE          = 0x656E6F62;
const DASH_LABEL_ANIM          = 0x6D696E61;

// Texture Wrapping

const DASH_TEXTURE_WRAP                 = 0;
const DASH_TEXTURE_CLAMP                = 1;
const DASH_TEXTURE_MIRROR               = 2;

// Image Formats

const DASH_TEXTURE_PNG                  = 0;
const DASH_TEXTURE_JPG                  = 1;
const DASH_TEXTURE_TGA                  = 2;

// Pixel Format

const DASH_TEXTURE_RGBA                 = 0;
const DASH_TEXTURE_RGB                  = 1;

// Render Side

const DASH_MATERIAL_FRONTSIDE           = 0;
const DASH_MATERIAL_BACKSIDE            = 1;
const DASH_MATERIAL_DOUBLESIDE          = 2;

// Shader Types

const DASH_MATERIAL_BASIC               = 0;
const DASH_MATERIAL_LAMBERT             = 1;
const DASH_MATERIAL_PHONG               = 2;

// Blend Equations

const DASH_MATERIAL_NORMAL              = 0;
const DASH_MATERIAL_ADDITIVE            = 1;
const DASH_MATERIAL_SUBTRACT            = 2;
const DASH_MATERIAL_MULTIPLY            = 3;

// Blend Contansts

const DASH_MATERIAL_ZERO                = 0;
const DASH_MATERIAL_ONE                 = 1;
const DASH_MATERIAL_SRC_COLOR           = 2;
const DASH_MATERIAL_ONE_MINUS_SRC_COLOR = 3;
const DASH_MATERIAL_SRC_ALPHA           = 4;
const DASH_MATERIAL_ONE_MINUS_SRC_ALPHA = 5;
const DASH_MATERIAL_DST_ALPHA           = 6;
const DASH_MATERIAL_ONE_MINUS_DST_ALPHA = 7;
const DASH_MATERIAL_DST_COLOR           = 8;
const DASH_MATERIAL_ONE_MINUS_DST_COLOR = 9;
const DASH_MATERIAL_SRC_ALPHA_SATURATE  = 10;

// Animation Flags

const DASH_ANIMATION_POS             = 0x01;
const DASH_ANIMATION_ROT             = 0x02;
const DASH_ANIMATION_SCL             = 0x04;

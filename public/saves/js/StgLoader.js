/*
 * @author - Kion
 */

"use strict";

const StgLoader = (function() {

	this.MEM = {
		MAX_TILE : 0
	};

	this.DOM = {};

	this.EVT = {
		handleMenuClick : evt_handleMenuClick.bind(this)
	};

	this.API = {
		prepare : api_prepare.bind(this),
		appendOffsets : api_appendOffsets.bind(this),
		renderMenu : api_renderMenu.bind(this),
		renderModel : api_renderModel.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

	}

	function evt_handleMenuClick(evt) {

        for(let i = 0; i < this.MEM.list.length; i++) {
            this.MEM.list[i].classList.remove("active");
        }

        evt.target.classList.add("active");
        let index = this.MEM.list.indexOf(evt.target);
        let mesh = this.API.renderModel(index);
        StateViewer.API.setModel(mesh);

	}

	function api_prepare(mem) {

		this.MEM.mem = mem.buffer;
		this.MEM.offsets = [];

		const OFFSET = 0x00194000;
		let view = new DataView(this.MEM.mem);
		this.MEM.view = view;
		let firstOfs = view.getUint32(OFFSET + 0x18, true);
		firstOfs &= 0xffffff;

		let count = (firstOfs - OFFSET) / 0x30;
		this.MEM.MAX_TILE = count;
		this.MEM.tiles = new Array();
		
		let st = view.getUint8(0xC356E).toString(16);
		if(st.length < 2) {
			st = "0" + st;
		}
		st = "st" + st + "_t";

		let ofs = OFFSET;
		for(let i = 0; i < count; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			let name = st + num;

			let tile = {
				name : name,
				highOfs : view.getUint32(ofs + 0x18, true) & 0xffffff,
				medOfs : view.getUint32(ofs + 0x1c, true) & 0xffffff,
				lowOfs : view.getUint32(ofs + 0x20, true) & 0xffffff
			};

			let alt = {
				name : name + "a",
				highOfs : view.getUint32(ofs + 0x24, true) & 0xffffff,
				medOfs : view.getUint32(ofs + 0x28, true) & 0xffffff,
				lowOfs : view.getUint32(ofs + 0x2c, true) & 0xffffff
			};
			this.API.appendOffsets(tile, alt);

			this.MEM.tiles.push(tile);
			if(alt.highOfs !== 0 && alt.highOfs !== tile.highOfs) {
				this.MEM.tiles.push(alt);
			}

			ofs += 0x30;

		}

		this.MEM.offsets.sort();
		this.MEM.LAST_TILE = this.MEM.tiles[this.MEM.tiles.length - 1];
		return this.MEM.tiles.length;

	}

	function api_appendOffsets(tile, alt) {

		let list = [
			tile.highOfs,
			tile.medOfs,
			tile.lowOfs,
			alt.highOfs,
			alt.medOfs,
			alt.lowOfs
		];

		list.forEach(ofs => {
			if(this.MEM.offsets.indexOf(ofs) !== -1) {
				return;
			}
			this.MEM.offsets.push(ofs);
		});

	}

	function api_renderMenu() {
		
		const ul = document.createElement("ul");

		this.MEM.list = [];
		for(let i = 0; i < this.MEM.tiles.length; i++) {

			let li = document.createElement("li");
			li.textContent = this.MEM.tiles[i].name;
			li.addEventListener("click", this.EVT.handleMenuClick);
			ul.appendChild(li);
			this.MEM.list.push(li);

		}

		return ul;

	}

	function api_renderModel(index) {
	
		let ofs = this.MEM.tiles[index].highOfs;
		console.log("Reading tile: 0x%s", ofs.toString(16));
		console.log("Med: 0x%s", this.MEM.tiles[index].medOfs);

		let textures = {
			dword : []
		};

		let polyList = [];
		let nbTex = this.MEM.view.getUint8(ofs + 0);
		let tileIndex = this.MEM.offsets.indexOf(ofs);
		let nextTile = this.MEM.offsets[tileIndex + 1];

		let nbPoly = this.MEM.view.getUint8(ofs+ 1) & 0x0f;

		if(tileIndex !== -1) {
			let calcPoly = ((nextTile - ofs) - (nbTex + 1)*4) / 8;
			nbPoly = calcPoly || nbPoly;
		}
		switch(this.MEM.tiles[index].name){
		case "st0f_t008":
			nbPoly = 19;
			break;
		}
		console.log("number poly: %d", nbPoly);
		ofs += 4;
			
		let materials = [];
		const images = [];

		for(let i = 0; i < nbTex; i++) {

			let tex = this.MEM.view.getUint32(ofs, true);
			textures.dword.push(tex);
			ofs += 4;

			let canvas = Framebuffer.API.renderTexture(tex);
			let ctx = canvas.getContext("2d");
			images.push(ctx);

            let texture = new THREE.Texture(canvas);
			texture.flipY = false;
            texture.needsUpdate = true;
            materials[i] = new THREE.MeshBasicMaterial({
                map : texture,
				depthTest : true,
				polygonOffset: true,
				polygonOffsetFactor: 1.0,
				polygonOffsetUnits: 4.0
            });

		}
		

		for(let i = 0; i < nbPoly; i++) {
				
			let uvOfs = this.MEM.view.getUint32(ofs, true);
			let scale = this.MEM.view.getUint8(ofs + 3) & 0xf0;
			let texId = this.MEM.view.getUint8(ofs + 3) & 0x0f;
			scale = scale >> 3;

			let quadOfs = this.MEM.view.getUint32(ofs + 4, true);
			let nbQuad = this.MEM.view.getUint8(ofs + 7);
				
			polyList.push({
				scale : scale,
				quadOfs : quadOfs & 0xffffff,
				uvOfs : uvOfs & 0xffffff,
				nbQuad : nbQuad,
				texId : texId
			});
			
			ofs += 8;

		}

		let geometry = new THREE.Geometry();
		let lookup = {};

		polyList.forEach(poly => {
			ofs = poly.quadOfs;
			console.log("POLY");
			console.log(poly);

			let scl = 0.1;
			switch(poly.scale) {
			case 4:
				scl = 0.025;
				break;
			case 6:
				scl = 0.05;
				break;
			case 8:
				scl = 0.1;
				break;
			case 10:
				scl = 0.2;
				break;
			default:
				console.log("NEW SCALE VALUE %d", poly.scale);
				scl = 0.1;
				break;
			}

			let flags = [];

			for(let k = 0; k < poly.nbQuad; k++) {
				
				flags[k] = this.MEM.view.getUint8(ofs + 3);
				ofs += 4;

				let points = [{
						x : this.MEM.view.getInt8(ofs + 0),
						y : this.MEM.view.getUint8(ofs + 1),
						z : this.MEM.view.getInt8(ofs + 2)
					}, {
						x : this.MEM.view.getInt8(ofs + 3),
						y : this.MEM.view.getUint8(ofs + 4),
						z : this.MEM.view.getInt8(ofs + 5)
					}, {
						x : this.MEM.view.getInt8(ofs + 6),
						y : this.MEM.view.getUint8(ofs + 7),
						z : this.MEM.view.getInt8(ofs + 8)
					}, {
						x : this.MEM.view.getInt8(ofs + 9),
						y : this.MEM.view.getUint8(ofs + 10),
						z : this.MEM.view.getInt8(ofs + 11)
					}];

					let length = geometry.vertices.length;
					let p = new Array(4);

					for(let i = 0; i < points.length; i++) {
						let vertex = new THREE.Vector3();
						vertex.x = points[i].x * scl;
						vertex.y = points[i].y * scl;
						vertex.z = -points[i].z * scl;

						let key = [
							vertex.x.toFixed(2),
							vertex.y.toFixed(2),
							vertex.z.toFixed(2)
						].join(",");
						
						if(!lookup.hasOwnProperty(key)) {
							lookup[key] = geometry.vertices.length;
							geometry.vertices.push(vertex);
						}

						p[i] = lookup[key];
					}

					let a = p[0];
					let b = p[1];
					let c = p[2];
					let d = p[3];

					let face_a = new THREE.Face3(b, a, c);
					let face_b = new THREE.Face3(d, b, c);

					face_a.materialIndex = poly.texId;
					face_b.materialIndex = poly.texId;

					geometry.faces.push(face_a);
					geometry.faces.push(face_b);
				
					ofs += 0x0c;
				
					if(flags[k] & 0x04) {
						materials[poly.texId].side = THREE.DoubleSide;
					}
			}

			ofs = poly.uvOfs;
			for(let k = 0; k < poly.nbQuad; k++) {
				
				let xmin = 255;
				let ymin = 255;
				let xmax = 0;
				let ymax = 0;

				let coords = [{
					u : this.MEM.view.getUint8(ofs + 0),
					v : this.MEM.view.getUint8(ofs + 1)
				},{
					u : this.MEM.view.getUint8(ofs + 2),
					v : this.MEM.view.getUint8(ofs + 3)
				},{
					u : this.MEM.view.getUint8(ofs + 4),
					v : this.MEM.view.getUint8(ofs + 5)
				},{
					u : this.MEM.view.getUint8(ofs + 6),
					v : this.MEM.view.getUint8(ofs + 7)
				}];
				
				coords.forEach(coord => {
					
					if(coord.u < xmin) {
						xmin = coord.u;
					}
					
					if(coord.v < ymin) {
						ymin = coord.v;
					}
					
					if(coord.u > xmax) {
						xmax = coord.u;
					}
					
					if(coord.v > ymax) {
						ymax = coord.v;
					}

				});

				let a = new THREE.Vector2(
					coords[0].u * 0.00390625 + 0.001953125, 
					coords[0].v * 0.00390625 + 0.001953125
				);
				let b = new THREE.Vector2(
					coords[1].u * 0.00390625 + 0.001953125, 
					coords[1].v * 0.00390625 + 0.001953125
				);
				let c = new THREE.Vector2(
					coords[2].u * 0.00390625 + 0.001953125, 
					coords[2].v * 0.00390625 + 0.001953125
				);
				let d = new THREE.Vector2(
					coords[3].u * 0.00390625 + 0.001953125, 
					coords[3].v * 0.00390625 + 0.001953125
				);
				
				geometry.faceVertexUvs[0].push([b, a, c]);
				geometry.faceVertexUvs[0].push([d, b, c]);

				ofs += 8;
				
				let width = xmax - xmin;
				let height = ymax - ymin;
				let ctx = images[poly.texId];
				
				width = width || 1;
				height = height || 1;

				let data = ctx.getImageData(xmin, ymin, width, height);

				for(let i = 0; i < data.data.length; i+= 4) {
					let r = data.data[i + 0];
					let g = data.data[i + 1];
					let b = data.data[i + 2];
					let a = data.data[i + 3];
						
					if(a !== 0) {
						continue;
					}
						
					materials[poly.texId].transparent = true;
					materials[poly.texId].alphaTest = 0.05;
					break;
				}

			}

		});

		geometry.computeFaceNormals();
		geometry.computeBoundingBox();

		let box = geometry.boundingBox;
		let mat = new THREE.MeshNormalMaterial();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);
		let mesh = new THREE.Mesh(buffer, materials);
		mesh.name = this.MEM.tiles[index].name;
		return mesh;

	}

}).apply({});

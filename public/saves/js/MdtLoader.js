/*
 * @author - Kion
 */

"use strict";

const MdtLoader = (function() {

	this.MEM = {};

	this.DOM = {};

	this.EVT = {
		handleMenuClick : evt_handleMenuClick.bind(this)
	};

	this.API = {
		prepare : api_prepare.bind(this),
		renderMenu : api_renderMenu.bind(this),
		renderModel : api_renderModel.bind(this),
		renderJson : api_renderJson.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

	}

	function evt_handleMenuClick(evt) {

        for(let i = 0; i < this.MEM.list.length; i++) {
            this.MEM.list[i].classList.remove("active");
        }

        evt.target.classList.add("active");
        let index = this.MEM.list.indexOf(evt.target);
        let ml = this.API.renderModel(index);
        StateViewer.API.setModel(ml);

	}

	function api_prepare(mem) {

		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);

		const OFFSET = 0x00164000;
		let roomCount = this.MEM.view.getUint32(OFFSET, true);
		
		this.MEM.rooms = [];
		let ofs = OFFSET + 4;
		for(let i = 0; i < roomCount; i++) {
			this.MEM.rooms[i] = {
				x_len : this.MEM.view.getUint8(ofs + 2),
				z_len : this.MEM.view.getUint8(ofs + 3),
				ofs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff
			}
			ofs += 8;
		}

		return roomCount;

	}

	function api_renderMenu() {
		
		const ul = document.createElement("ul");

		this.MEM.list = [];
		for(let i = 0; i < this.MEM.rooms.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			let li = document.createElement("li");
			li.textContent = "room " + num;
			li.addEventListener("click", this.EVT.handleMenuClick);
			ul.appendChild(li);
			this.MEM.list.push(li);

		}

		return ul;

	}

	function api_renderJson(index) {
		
		const room = this.MEM.rooms[index];
		
		let ofs = room.ofs;
		const floor = [];

		for(let z = 0; z < room.z_len; z++) {
			for(let x = 0; x < room.x_len; x++) {

				let type = this.MEM.view.getUint8(ofs);
				ofs += 2;

				let num = type.toString(16);
				if(num.length < 2) {
					num = "0" + num;
				}

				floor.push({
					x : x,
					z : z,
					type : type,
					height : this.MEM.view.getUint8(ofs + 1),
					key : '0x' + num
				})

			}
		}
		
		return floor;

	}

	function api_renderModel(index) {
		
		const room = this.MEM.rooms[index];
		
		let ofs = room.ofs;
		const floor = [];
		const types = [];

		const materials = [];
		const meshlist = {};

		for(let z = 0; z < room.z_len; z++) {
			for(let x = 0; x < room.x_len; x++) {

				let type = this.MEM.view.getUint8(ofs);
				ofs += 2;

				let num = type.toString(16);
				if(num.length < 2) {
					num = "0" + num;
				}

				floor.push({
					x : x,
					z : z,
					type : type,
					height : this.MEM.view.getUint8(ofs + 1),
					key : '0x' + num
				})

				if(types.indexOf(type) === -1) {
					types.push(type);
				}

			}
		}
		
		const tiles = [];
		
		let group = new THREE.Group();
		floor.forEach(tile => {

			let str = tile.type.toString();
			while(str.length < 3) {
				str = "0" + str;
			}

			let canvas = document.createElement("canvas");
			canvas.width = 256;
			canvas.height = 256;
			let ctx = canvas.getContext("2d");
			
			if(tile.type === 0) {
				ctx.fillStyle = "#444";
			} else if(tile.height === 0) {
				ctx.fillStyle = "#f00";
			} else {
				ctx.fillStyle = "#fff";
			}
			ctx.fillRect(0,0,256,256);

			ctx.font = "80px Verdana";
			ctx.fillStyle = 'black';
			ctx.textBaseline = "middle";
			ctx.textAlign = "center";
			ctx.fillText(str, 128, 128);

			ctx.lineWidth = 5;
			ctx.strokeRect(0, 0, 256, 256);

			let texture = new THREE.Texture(canvas);
			texture.needsUpdate = true;

			let geometry = new THREE.PlaneBufferGeometry(canvas.width, canvas.height);
			let material = new THREE.MeshBasicMaterial({
				side : THREE.DoubleSide,
				map : texture
			});
			
			let m = new THREE.Mesh( geometry, material );
			m.rotation.x = -Math.PI / 2;
			m.position.x = tile.x * 256;
			m.position.z = tile.z * 256;
			group.add(m);

		});

		let num = index.toString();
		while(num.length < 3) {
			num = "0" + num;
		}
		group.name = "room_" + num;
		return group;

	}

}).apply({});

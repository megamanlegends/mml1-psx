/*
 * @author Kion (Benjamin Collins) - dashgl.com
 */

const StateViewer = (function() {

	"use strict";

	this.MEM = {
		db : new Dexie("mml1-sstate"),
		batch : false,
		meshlist : [],
		mixers : [],
		clock : new THREE.Clock()
	};

	this.DOM = {
		type: {
			ebd : document.getElementById('StateViewer.type.ebd'),
			stg : document.getElementById('StateViewer.type.stg'),
			mdt : document.getElementById('StateViewer.type.mdt'),
			pbd : document.getElementById('StateViewer.type.pbd'),
			idx : document.getElementById('StateViewer.type.idx')
		},
		input : {
			label : document.getElementById('StateViewer.input.label'),
			file : document.getElementById('StateViewer.input.file'),
			tray : document.getElementById('StateViewer.input.tray'),
			view : document.getElementById('StateViewer.input.view'),
			download : document.getElementById('StateViewer.input.download'),
			one : document.getElementById('StateViewer.input.one'),
			progress : document.getElementById('StateViewer.input.progress')
		},
		export : {
			psxm : document.getElementById('StateViewer.export.psxm'),
			vram : document.getElementById('StateViewer.export.vram'),
			pic : document.getElementById('StateViewer.export.pic')
		},
		controls : {
			select : document.getElementById('StateViewer.controls.select')
		}
	};

	this.EVT = {
		handleTypeClick : evt_handleTypeClick.bind(this),
		handleFileChange : evt_handleFileChange.bind(this),
		handleVramClick : evt_handleVramClick.bind(this),
		handlePsxmClick : evt_handlePsxmClick.bind(this),
		handlePicClick : evt_handlePicClick.bind(this),
		handleDownloadClick : evt_handleDownloadClick.bind(this),
		handleOneClick : evt_handleOneClick.bind(this),
		handleAnimationSelect : evt_handleAnimationSelect.bind(this)
	};

	this.API = {
		prepareSaveState : api_prepareSaveState.bind(this),
		animate : api_animate.bind(this),
		setModel : api_setModel.bind(this),
		setStage : api_setStage.bind(this),
		gltfExport : api_gltfExport.bind(this)
	};

	init.apply(this);
	return this;

	async function init() {

		for(let key in this.DOM.type) {
			this.DOM.type[key].addEventListener("click", this.EVT.handleTypeClick);
		}

		this.MEM.db.version(1).stores({
			cache : '&name'
		});

		this.DOM.input.file.addEventListener("change", this.EVT.handleFileChange);
		this.DOM.input.download.addEventListener("click", this.EVT.handleDownloadClick);
		this.DOM.input.one.addEventListener("click", this.EVT.handleOneClick);

		let state = await this.MEM.db.cache.get({name:'sstate'});
		if(state) {
			//saveAs(new Blob([state.psxm]), "sstate.bin");
			this.MEM.sstate = state;
			this.API.prepareSaveState();
		}

		// Init Threejs

		let width = this.DOM.input.view.offsetWidth;
		let height = this.DOM.input.view.offsetHeight;

		this.MEM.scene = new THREE.Scene();
		this.MEM.camera = new THREE.PerspectiveCamera(70, width / height);
		this.MEM.camera.position.z = -10;
		this.MEM.camera.position.y = 10;
		this.MEM.camera.position.x = 0;
		this.MEM.camera.lookAt(new THREE.Vector3(0, 10, 0));

		this.MEM.renderer = new THREE.WebGLRenderer({
			antialias: true,
			alpha: true,
			preserveDrawingBuffer: true,
			logarithmicDepthBuffer : true
		});

		this.MEM.renderer.setSize(width, height);
		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0x000000, 0);

		this.MEM.light = new THREE.AmbientLight(0xffffff);
		this.MEM.scene.add(this.MEM.light);
		
		this.MEM.controls = THREE.OrbitControls(this.MEM.camera, this.MEM.renderer.domElement);
		this.DOM.input.view.appendChild(this.MEM.renderer.domElement);
		this.API.animate();

		// Check for caches stage files

		this.DOM.export.psxm.addEventListener('click', this.EVT.handlePsxmClick);
		this.DOM.export.vram.addEventListener('click', this.EVT.handleVramClick);
		this.DOM.export.pic.addEventListener('click', this.EVT.handlePicClick);

		var gridHelper = new THREE.GridHelper( 10, 10 );
		this.MEM.scene.add(gridHelper);
		this.MEM.grid = gridHelper;

		this.DOM.controls.select.addEventListener('change', this.EVT.handleAnimationSelect);
	}

	function evt_handleTypeClick(evt) {

		let id = evt.target.getAttribute("id");
		let leaf = id.split(".").pop();

		for(let key in this.DOM.type) {
			this.DOM.type[key].classList.remove("active");
		}

		let ul = this.DOM.input.tray.children[0];
		this.DOM.input.tray.replaceChild(this.MEM.tray[leaf], ul);
		this.DOM.type[leaf].classList.add("active");

	}

	function evt_handleFileChange(evt) {

		let file = evt.target.files[0];

		let reader = new FileReader();

		reader.onload = (evt) => {

			let array = evt.target.result;
			const compressed = new Uint8Array(array);
			const gunzip = new Zlib.Gunzip(compressed);
			const sstate = gunzip.decompress();
			
			const query = {
				name : 'sstate',
				psxm : new Uint8Array(0x200000),
				vram : new Uint8Array(0x100000)
			};

			const cpu_ofs = 0x9025;
			const gpu_ofs = 0x29B749;

			for(let i = 0; i < query.psxm.length; i++) {
				query.psxm[i] = sstate[cpu_ofs + i];
			}

			for(let i = 0; i < query.vram.length; i++) {
				query.vram[i] = sstate[gpu_ofs + i];
			}

			// Check against stg in db
			let len = 0;

			const STAGE = query.psxm[0xC356E].toString(16);
			const ROOM = query.psxm[0xC356F].toString(16);
		
			let str = "ST";
			if(STAGE.length < 2) {
				str += "0";
			}
			str += STAGE;
			str += "_";
			if(ROOM.length < 2) {
				str += "0";
			}
			str += ROOM;
			query.scene = str;
			
			this.MEM.db.cache.put(query);
			this.MEM.sstate = query;
			this.API.prepareSaveState();
			this.DOM.input.file.value = "";

		}

		reader.readAsArrayBuffer(file);

	}

	function evt_handleVramClick() {
		
		if(!this.MEM.sstate) {
			return;
		}

		console.log("Handle vram click!!!");
		Framebuffer.API.renderAll(this.MEM.sstate.scene);

	}

	function evt_handlePicClick() {
		
		if(!this.MEM.sstate) {
			return;
		}

		this.MEM.saveImage = true;

	}

	function evt_handlePsxmClick() {
		
		if(!this.MEM.sstate) {
			return;
		}

		saveAs(new Blob([this.MEM.sstate.psxm]), this.MEM.sstate.scene + ".mem");

	}

	async function evt_handleOneClick() {

		if(!this.MEM.sstate) {
			return;
		}

		if(this.MEM.batch) {
			return;
		}
		
		let blob = await this.API.gltfExport(this.MEM.mesh);
		saveAs(blob, this.MEM.mesh.name + ".glb");

	}

	async function evt_handleDownloadClick() {

		if(!this.MEM.sstate) {
			return;
		}

		if(this.MEM.batch) {
			return;
		}

		this.MEM.batch = true;

		// Create Zip

		const zip = new JSZip();
		const TOTAL = this.MEM.count.ebd +
			this.MEM.count.stg +
			this.MEM.count.mdt;
		let count = 0;

		// EBD

		for(let i = 0; i < this.MEM.count.ebd; i++) {
			let mesh = EbdLoader.API.renderModel(i);
			let blob = await this.API.gltfExport(mesh);
			zip.folder("ebd").file(mesh.name + ".glb", blob);

			this.DOM.input.progress.value = parseInt(count / TOTAL * 90);
			count++;
		}
		
		// STG

		for(let i = 0; i < this.MEM.count.stg; i++) {
			let mesh = StgLoader.API.renderModel(i);
			let blob = await this.API.gltfExport(mesh);
			zip.folder("stg").file(mesh.name + ".glb", blob);
			
			this.DOM.input.progress.value = parseInt(count / TOTAL * 90);
			count++;
		}
		
		// MDT

		for(let i = 0; i < this.MEM.count.mdt; i++) {
			let mesh = MdtLoader.API.renderModel(i);
			let blob = await this.API.gltfExport(mesh);
			zip.folder("mdt").file(mesh.name + ".glb", blob);

			let json = MdtLoader.API.renderJson(i);
			zip.folder("mdt").file(mesh.name + ".json", JSON.stringify(json, null, 4));
			
			this.DOM.input.progress.value = parseInt(count / TOTAL * 90);
			count++;
		}
		
		// Export Zip

		let blob = await zip.generateAsync({type:"blob"});
		this.DOM.input.progress.value = 100;
		saveAs(blob, this.MEM.sstate.scene + ".zip");
		
		this.MEM.batch = false;

	}

	function evt_handleAnimationSelect() {

		this.MEM.mixers = [];
		let index = this.DOM.controls.select.selectedIndex - 1;

		if(index === -1) {
			return;
		}
		
		let mixer = new THREE.AnimationMixer(this.MEM.mesh);
		let clip = this.MEM.mesh.geometry.animations[index];
		let action = mixer.clipAction(clip, this.MEM.mesh);
		this.MEM.action = action;
		action.play();
		this.MEM.mixers.push(mixer);

		console.log("change!!");

	}

	function api_prepareSaveState() {

		Framebuffer.API.prepare(this.MEM.sstate.vram);
		this.DOM.input.label.textContent = this.MEM.sstate.scene;
		
		let count;
		this.MEM.tray = {};
		this.MEM.count = {};

		count = EbdLoader.API.prepare(this.MEM.sstate.psxm);
		this.DOM.type.ebd.setAttribute("data-count", count);
		this.MEM.tray.ebd = EbdLoader.API.renderMenu();
		this.MEM.count.ebd = count;

		count = MdtLoader.API.prepare(this.MEM.sstate.psxm);
		this.DOM.type.mdt.setAttribute("data-count", count);
		this.MEM.tray.mdt = MdtLoader.API.renderMenu();
		this.MEM.count.mdt = count;

		count = StgLoader.API.prepare(this.MEM.sstate.psxm);
		this.DOM.type.stg.setAttribute("data-count", count);
		this.MEM.tray.stg = StgLoader.API.renderMenu();
		this.MEM.count.stg = count;

		count = PbdLoader.API.prepare(this.MEM.sstate.psxm);
		this.DOM.type.pbd.setAttribute("data-count", count);
		this.MEM.tray.pbd = PbdLoader.API.renderMenu();
		this.MEM.count.pbd = count;

		count = IdxLoader.API.prepare(this.MEM.sstate.psxm);
		this.DOM.type.idx.setAttribute("data-count", count);
		this.MEM.tray.idx = IdxLoader.API.renderMenu();
		this.MEM.count.idx = count;
		
		this.DOM.input.tray.innerHTML = "";
		this.DOM.input.tray.appendChild(this.MEM.tray.ebd);

		for(let key in this.DOM.type) {
			this.DOM.type[key].classList.remove("active");
		}

		this.DOM.type.ebd.classList.add("active");

	}

	function api_animate() {

		requestAnimationFrame(this.API.animate);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);
		
		let delta = this.MEM.clock.getDelta();
		this.MEM.mixers.forEach(mixer => {
			mixer.update(delta);
		});

		if(this.MEM.saveImage) {
			this.MEM.saveImage = false;
			this.MEM.renderer.domElement.toBlob(function (blob) {
				saveAs(blob, Date.now() + ".png");
			});
		}

	}

	function api_setModel(mesh) {
		
		for(let i = 0; i < this.MEM.meshlist.length; i++) {
			this.MEM.scene.remove(this.MEM.meshlist[i]);
		}

		this.MEM.meshlist = [mesh];
		
		//mesh.scale.x = -1;
		this.MEM.scene.add(mesh);
		this.MEM.mesh = mesh;
		
		var box = new THREE.BoxHelper( mesh, 0xff0000 );
		this.MEM.box = box;
		mesh.add(box);

		// Init animations
		const html = '<option value="-1">Select an animation</option>';
		this.DOM.controls.select.innerHTML = html;

		let anims = [];
		
		if(mesh.geometry) {
			anims = mesh.geometry.animations || [];
		}

		if(anims.length === 0) {
			this.DOM.controls.select.setAttribute("disabled", "disabled");
		} else {
			this.DOM.controls.select.removeAttribute("disabled");
		}
		
		anims.forEach(anim => {
			
			let option = document.createElement("option");
			option.textContent = anim.name;
			this.DOM.controls.select.appendChild(option);

		});

		if(!mesh.skeleton) {
			return;
		}
		
		let helper = new THREE.SkeletonHelper(mesh);
		helper.material.linewidth = 1;
		this.MEM.helper = helper;
		mesh.add(helper);

		console.log(mesh.geometry.animations);

	}

	function api_setStage(meshes) {

		console.log(this.MEM);
		for(let i = 0; i < this.MEM.meshlist.length; i++) {
			this.MEM.scene.remove(this.MEM.meshlist[i]);
		}

		this.MEM.meshlist = meshes;
	
		meshes.forEach(mesh => {
			this.MEM.scene.add(mesh);
		});

	}

	function api_gltfExport(mesh) {

		return new Promise( (resolve, reject) => {

			let anims = [];
			if(mesh.geometry) {
				anims = mesh.geometry.animations || []
			}

			let expt = new THREE.GLTFExporter();
			let opt = {
				binary : true,

				animations : anims
			};

			let dir = mesh.userData.folder;

			expt.parse(mesh, result => {
				
				let mime = {type:"application/octet-stream"};
				let blob = new Blob([result], mime);
				resolve(blob);

			}, opt);

		});

	}

}).apply({});

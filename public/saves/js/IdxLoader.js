/*
 * @author - Kion
 */

"use strict";

const IdxLoader = (function() {

	this.MEM = {}

	this.DOM = {}

	this.EVT = {
		handleMenuClick : evt_handleMenuClick.bind(this)
	}

	this.API = {
		prepare : api_prepare.bind(this),
		renderMenu : api_renderMenu.bind(this),
		renderModel : api_renderModel.bind(this),
		renderFloor : api_renderFloor.bind(this),
		renderTerrain : api_renderTerrain.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

	}
	
	function evt_handleMenuClick(evt) {

		console.log("click my nubble");

		for(let i = 0; i < this.MEM.list.length; i++) {
			this.MEM.list[i].classList.remove("active");
		}

		evt.target.classList.add("active");
		let index = this.MEM.list.indexOf(evt.target);
		let ml = this.API.renderModel(index);
		StateViewer.API.setModel(ml);

	}

	function api_prepare(mem) {

		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);

		const OFFSET = 0x00164000;
		let roomCount = this.MEM.view.getUint32(OFFSET, true);

		this.MEM.rooms = [];
		let ofs = OFFSET + 4;
		for(let i = 0; i < roomCount; i++) {
			this.MEM.rooms[i] = {
				x_pos : this.MEM.view.getInt8(ofs + 0),
				z_pos : this.MEM.view.getInt8(ofs + 1),
				x_len : this.MEM.view.getUint8(ofs + 2),
				z_len : this.MEM.view.getUint8(ofs + 3),
				ofs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff
			}
			ofs += 8;
		}

		return roomCount;

	}
	
	function api_renderMenu() {
	
		const ul = document.createElement("ul");

		this.MEM.list = [];
		for(let i = 0; i < this.MEM.rooms.length; i++) {

			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			let li = document.createElement("li");
			li.textContent = "room " + num;
			li.addEventListener("click", this.EVT.handleMenuClick);
			ul.appendChild(li);
			this.MEM.list.push(li);

		}

		return ul;


	}
	
	function api_renderModel(index) {
		
		console.log("Index: %d", index);
		const MAX_TILE = 0x2730;
		//const MAX_TILE = 0x2f0;
		//const MAX_TILE = StgLoader.MEM.MAX_TILE;
		const UNIQUE = [];
		const FLOOR = [];
		const ROOM = this.MEM.rooms[index];
		
		console.log(ROOM);
		let ofs = ROOM.ofs;

		for(let z = 0; z < ROOM.z_len; z++) {
			for(let x = 0; x < ROOM.x_len; x++) {
				let type = this.MEM.view.getUint16(ofs, true);
				ofs += 2;
				
				if(type === 0) {
					continue;
				}

				if(type & 0x8000) {
					FLOOR.push({
						x_pos : x - 3.5,
						z_pos : ROOM.z_len - z - 2.5,
						type : type
					});
					console.log("Floor tile?");
					continue;
				}

				if(type > MAX_TILE) {
					console.log("Everything I know is a lie");
					continue;
				}

				switch(type) {
				case 0x2c0:
				case 0x2c1:
				case 0x2d3:
					continue;
					break;
				}

				if(UNIQUE.indexOf(type) !== -1) {
					continue;
				}

				UNIQUE.push(type);

			}
		}
		
		this.MEM.geometry = new THREE.Geometry();
		this.MEM.materials = [];
		this.MEM.mats = [];
		this.MEM.images = [];
		this.MEM.lookup = {};

		this.API.renderTerrain(UNIQUE);
		this.API.renderFloor(FLOOR);
		
		let debugMat = new THREE.MeshNormalMaterial();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(this.MEM.geometry);
		buffer.computeVertexNormals();
		let mesh = new THREE.Mesh(buffer, this.MEM.mats);
		//let mesh = new THREE.Mesh(buffer, debugMat);
		mesh.name = "name_this_sheeeit";
		return mesh;
	}

	function api_renderFloor(indices) {
			
		console.log(indices[0]);
		
		let matIndex = this.MEM.mats.length;
		this.MEM.mats.push(new THREE.MeshBasicMaterial({
			color: 0xff0000
		}));
		
		/*
		indices = [
			indices[0]
		];
		*/

		this.MEM.geometry.computeBoundingBox();
		let box = this.MEM.geometry.boundingBox;
		console.log(box);

		indices.forEach(index => {

			let v_list = [
				new THREE.Vector3(-1.6, 0, -1.6),
				new THREE.Vector3(-1.6, 0, 1.6),
				new THREE.Vector3(1.6, 0, 1.6),
				new THREE.Vector3(1.6, 0, -1.6),
			];

			let length = this.MEM.geometry.vertices.length;
			let p = new Array(4);

			for(let i = 0; i < v_list.length; i++) {

				let vertex = v_list[i];
				vertex.x += index.x_pos * 3.2;
				//vertex.y = point.y * poly.scale;
				vertex.z += index.z_pos * 3.2;

				vertex.x += box.min.x;
				vertex.z += box.min.z;

				let key = [
					vertex.x.toFixed(2),
					vertex.y.toFixed(2),
					vertex.z.toFixed(2)
				].join(",");

				if(!this.MEM.lookup.hasOwnProperty(key)) {
					this.MEM.lookup[key] = this.MEM.geometry.vertices.length;
					this.MEM.geometry.vertices.push(vertex);
				}

				p[i] = this.MEM.lookup[key];

			}

			let a = p[0];
			let b = p[1];
			let c = p[2];
			let d = p[3];

			let face_a = new THREE.Face3(a, b, c);
			let face_b = new THREE.Face3(d, a, c);

			face_a.materialIndex = matIndex;
			face_b.materialIndex = matIndex;

			this.MEM.geometry.faces.push(face_a);
			this.MEM.geometry.faces.push(face_b);

			a = new THREE.Vector2(0,0);
			b = new THREE.Vector2(0,1);
			c = new THREE.Vector2(1,0);
			d = new THREE.Vector2(1,1);

			this.MEM.geometry.faceVertexUvs[0].push([b, a, c]);
			this.MEM.geometry.faceVertexUvs[0].push([d, b, c]);

		});

	}

	function api_renderTerrain(indices) {
		
		console.log("Rendering terrain!!");
		const IDX_OFFSET = 0x0015c000;

		/*
		indices = [ 
			indices[0],
			indices[1],
			indices[2],
			indices[3],
			indices[4] 
		];
		*/

		indices.forEach(index => {
		
			let ofs = IDX_OFFSET + (index * 12);
			
			/*
			let short_a = this.MEM.view.getInt16(ofs + 0, true);
			let short_b = this.MEM.view.getInt16(ofs + 2, true);
			let x_pos = this.MEM.view.getUint16(ofs + 4, true);
			let z_pos = this.MEM.view.getUint16(ofs + 6, true);
			*/

			const pos_x = this.MEM.view.getInt8(ofs + 1) * 1.6;
			const pos_z = this.MEM.view.getInt8(ofs + 5) * 1.6;
			const STG_OFS = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;

			if(STG_OFS > StgLoader.MEM.LAST_TILE) {
				console.log("TOOO BIGG!!?");
				return;
			}

			const TILE_OFS = this.MEM.view.getUint32(STG_OFS + 0x18, true) & 0xffffff;

			ofs = TILE_OFS;
			let tex_count = this.MEM.view.getUint8(ofs + 0);
			let poly_count = this.MEM.view.getUint8(ofs + 1) & 0x0f;
			ofs += 4;
			
			let tex_map = [];
			for(let i = 0; i < tex_count; i++) {
				
				let tex_no = this.MEM.view.getUint32(ofs, true);
				if(this.MEM.materials.indexOf(tex_no) === -1) {
					this.MEM.materials.push(tex_no);

					let canvas = Framebuffer.API.renderTexture(tex_no);
					let ctx = canvas.getContext("2d");
					this.MEM.images.push(ctx);

					let texture = new THREE.Texture(canvas);
					texture.flipY = false;
					texture.needsUpdate = true;

					this.MEM.mats.push( new THREE.MeshBasicMaterial({
						map : texture,
						alphaTest : 0.05,
						transparent : true,
						side : THREE.DoubleSide
					}));

				}
				tex_map[i] = this.MEM.materials.indexOf(tex_no);
				ofs += 4;

			}
			
			let poly_map = [];
			for(let i = 0; i < poly_count; i++) {

				let poly = {
					uv_ofs : this.MEM.view.getUint32(ofs + 0, true) & 0xffffff,
					scale : this.MEM.view.getUint8(ofs + 3) & 0xf0,
					tex_no : this.MEM.view.getUint8(ofs + 3) & 0x0f,
					quad_ofs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
					quad_count : this.MEM.view.getUint8(ofs + 7)
				}

				switch(poly.scale >> 3) {
				case 4:
					poly.scale = 0.025;
					break;
				case 6:
					poly.sclae = 0.05;
					break;
				case 8:
					poly.scale = 0.1;
					break;
				case 10:
					poly.scale = 0.2;
					break;
				default:
					console.log("what is thissss!!!?");
					break;
				}

				poly.tex_no = tex_map[ poly.tex_no ];
				poly_map.push(poly);

			}

			poly_map.forEach(poly => {
				
				let flags = [];

				ofs = poly.quad_ofs;
				for(let i = 0; i < poly.quad_count; i++) {
					flags[i] = this.MEM.view.getUint8(ofs + 3);
					ofs += 4;

					let points = [
						{
							i : 0,
							x : this.MEM.view.getInt8(ofs + 0),
							y : this.MEM.view.getUint8(ofs + 1),
							z : this.MEM.view.getInt8(ofs + 2)
						},
						{
							i : 1,
							x : this.MEM.view.getInt8(ofs + 3),
							y : this.MEM.view.getUint8(ofs + 4),
							z : this.MEM.view.getInt8(ofs + 5)
						},
						{
							i : 2,
							x : this.MEM.view.getInt8(ofs + 6),
							y : this.MEM.view.getUint8(ofs + 7),
							z : this.MEM.view.getInt8(ofs + 8)
						},
						{
							i : 3,
							x : this.MEM.view.getInt8(ofs + 9),
							y : this.MEM.view.getUint8(ofs + 10),
							z : this.MEM.view.getInt8(ofs + 11)
						}
					];
					ofs += 12;

					let length = this.MEM.geometry.vertices.length;
					let p = new Array(4);

					points.forEach(point => {
						
						let vertex = new THREE.Vector3();
						vertex.x = point.x * poly.scale + pos_x;
						vertex.y = point.y * poly.scale;
						vertex.z = -point.z * poly.scale - pos_z;

						let key = [
							vertex.x.toFixed(2),
							vertex.y.toFixed(2),
							vertex.z.toFixed(2)
						].join(",");

						if(!this.MEM.lookup.hasOwnProperty(key)) {
							this.MEM.lookup[key] = this.MEM.geometry.vertices.length;
							this.MEM.geometry.vertices.push(vertex);
						}

						p[point.i] = this.MEM.lookup[key];
					});

					let a = p[0];
					let b = p[1];
					let c = p[2];
					let d = p[3];

					let face_a = new THREE.Face3(b, a, c);
					let face_b = new THREE.Face3(d, b, c);

					face_a.materialIndex = poly.tex_no;
					face_b.materialIndex = poly.tex_no;

					this.MEM.geometry.faces.push(face_a);
					this.MEM.geometry.faces.push(face_b);
				}

				ofs = poly.uv_ofs;
				for(let i = 0; i < poly.quad_count; i++) {

					let coords = [
						{
							u : this.MEM.view.getUint8(ofs + 0),
							v : this.MEM.view.getUint8(ofs + 1)
						},
						{
							u : this.MEM.view.getUint8(ofs + 2),
							v : this.MEM.view.getUint8(ofs + 3)
						},
						{
							u : this.MEM.view.getUint8(ofs + 4),
							v : this.MEM.view.getUint8(ofs + 5)
						},
						{
							u : this.MEM.view.getUint8(ofs + 6),
							v : this.MEM.view.getUint8(ofs + 7)
						}
					];
					ofs += 8;

					let a = new THREE.Vector2(
						coords[0].u * 0.00390625 + 0.001953125, 
						coords[0].v * 0.00390625 + 0.001953125
					);
					let b = new THREE.Vector2(
						coords[1].u * 0.00390625 + 0.001953125, 
						coords[1].v * 0.00390625 + 0.001953125
					);
					let c = new THREE.Vector2(
						coords[2].u * 0.00390625 + 0.001953125, 
						coords[2].v * 0.00390625 + 0.001953125
					);
					let d = new THREE.Vector2(
						coords[3].u * 0.00390625 + 0.001953125, 
						coords[3].v * 0.00390625 + 0.001953125
					);

					this.MEM.geometry.faceVertexUvs[0].push([b, a, c]);
					this.MEM.geometry.faceVertexUvs[0].push([d, b, c]);

				}

			});


		});

	}

}).apply({});

console.log(IdxLoader);

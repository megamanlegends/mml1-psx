"use strict";

const PbdLoader = (function() {
	
	const FPS = 30;
	const SCALE = 0.00125;
	const ROT = new THREE.Matrix4().makeRotationX(Math.PI);

	this.MEM = {
		matList : [],
		models : [
			/*
			"Special Weapon",
			"Head",
			"Left Foot",
			"Right Foot",
			"Buster",
			"Megaman Common"
			*/
			"Megaman Equiped",
			"Megaman Standard"
		]
	}

	this.DOM = {}

	this.EVT = {
		handleMenuClick : evt_handleMenuClick.bind(this)
	}

	this.API = {
		prepare : api_prepare.bind(this),
		renderMenu : api_renderMenu.bind(this),

		// Read Body Parts

		meshStandard : api_meshStandard.bind(this),
		meshEquiped : api_meshEquiped.bind(this),
		meshCommon : api_meshCommon.bind(this),

		// Selective Parts

		readBody : api_readBody.bind(this),
		readHead : api_readHead.bind(this),
		readLeftArm : api_readLeftArm.bind(this),
		readBusterArm : api_readBusterArm.bind(this),
		readRightArm : api_readRightArm.bind(this),
		readSpecialArm : api_readSpecialArm.bind(this),
		readFeet : api_readFeet.bind(this),

		readTriList : api_readTriList.bind(this),
		readQuadList : api_readQuadList.bind(this),
		readVertices : api_readVertices.bind(this),
		loadTexture : api_loadTexture.bind(this),

		readAnimationTimings : api_readAnimationTimings.bind(this),
		readBones : api_readBones.bind(this),
		readAnim : api_readAnim.bind(this),
		readAnimations : api_readAnimations.bind(this),
		debugAnimations : api_debugAnimations.bind(this)
	}

	init.apply(this);
	return this;

	function init() {
		
	}

	function evt_handleMenuClick(evt) {


		for(let i = 0; i < this.MEM.list.length; i++) {
			this.MEM.list[i].classList.remove("active");
		}

		evt.target.classList.add("active");
		let name = evt.target.getAttribute("data-name");
		let index = this.MEM.list.indexOf(evt.target);
		
		let mesh;

		switch(name) {
		case "special-weapon":

			break;
		case "head":

			break;
		case "left foot":

			break;
		case "right foot":

			break;
		case "buster":

			break;
		case "megaman-equiped":

			mesh = this.API.meshEquiped();

			break;
		case "megaman-standard":
			
			mesh = this.API.meshStandard();

			break;
		case "megaman-common":
			
			mesh = this.API.meshCommon();

			break;
		}

	  	StateViewer.API.setModel(mesh);

	}


	function api_prepare(mem) {

		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);

		this.MEM.animations = [];
		this.API.readBones();;
		
		let model;

		try {
			model = { poseOfs : 0xDD600, animOfs : 0xD9D00 };
			this.API.readAnimations(model, this.MEM.bones, this.MEM.animations);
		} catch(err) {

		}
		
		try {
			model = { poseOfs : 0xE9600, animOfs : 0xDBA00 };
			this.API.readAnim(model);
		} catch(err) {

		}
		
		try {
			model = { poseOfs : 0xED600, animOfs : 0xDC400 };
			this.API.readAnim(model);
		} catch(err) {

		}
		
		try {
			model = { poseOfs : 0xDC880, animOfs : 0xEF600 };
			this.API.readAnim(model);
		} catch(err) {

		}

		try {
			model = { poseOfs : 0xEF600, animOfs : 0xDC880 };
			this.API.readAnim(model);
		} catch(err) {

		}
		
		try {
			model = { poseOfs : 0xF1600, animOfs : 0xDCD00 };
			this.API.readAnim(model);
		} catch(err) {

		}

		try {
			model = { poseOfs : 0xF3600, animOfs : 0xDD180 };
			this.API.readAnim(model);
		} catch(err) {

		}

		return this.MEM.models.length;

	}

	function api_renderMenu() {

		let li;
		const ul = document.createElement("ul");

		this.MEM.list = [];

		this.MEM.models.forEach(part => {

			li = document.createElement("li");
			li.textContent = part;
			let name = part.replace(/\s+/g, '-').toLowerCase();
			li.setAttribute("data-name", name);
			li.addEventListener('click', this.EVT.handleMenuClick);
			ul.appendChild(li);
			this.MEM.list.push(li);

		});

		return ul;

	}

	/**
	 * Render Entire Megaman Model
	 **/

	function api_meshStandard() {

		// Prepare Polygons

		const materials = [ 1006632997, 1044906031 ];
        for(let i = 0; i < materials.length; i++) {
            let canvas = Framebuffer.API.renderTexture(materials[i]);
            let texture = new THREE.Texture(canvas);
            texture.flipY = false;
            texture.needsUpdate = true;
            materials[i] = new THREE.MeshBasicMaterial({
                map : texture,
                skinning : true
            });
        }


		let polygons = [];

		this.API.readBody(polygons);
		this.API.readHead(polygons);
		this.API.readLeftArm(polygons);
		this.API.readRightArm(polygons);
		this.API.readFeet(polygons);

		// Read Geometry

		let geometry = new THREE.Geometry();
		this.MEM.geometry = geometry;

		polygons.forEach(poly => {

			let matIndex = poly.matId || 0;
			this.MEM.vertexOfs = this.MEM.geometry.vertices.length;	
			this.API.readTriList(poly.tri_count, poly.triOfs, matIndex);
			this.API.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
			this.API.readVertices(poly.vert_count, poly.vertOfs, poly.boneId);

		});

		geometry.computeFaceNormals();
   		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		// Create mesh
		
		let mesh = new THREE.SkinnedMesh(buffer, materials);
		mesh.geometry.animations = this.MEM.animations;

		let armSkeleton = new THREE.Skeleton(this.MEM.bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);

		return mesh;

	}

	function api_meshEquiped() {

		// Prepare Polygons

		const materials = [ 1006632997, 1044906031 ];
        for(let i = 0; i < materials.length; i++) {
            let canvas = Framebuffer.API.renderTexture(materials[i]);
            let texture = new THREE.Texture(canvas);
            texture.flipY = false;
            texture.needsUpdate = true;
            materials[i] = new THREE.MeshBasicMaterial({
                map : texture,
                skinning : true
            });
        }


		let polygons = [];

		this.API.readBody(polygons);
		this.API.readHead(polygons);
		this.API.readBusterArm(polygons);
		this.API.readSpecialArm(polygons);
		this.API.readFeet(polygons);

		// Read Geometry

		let geometry = new THREE.Geometry();
		this.MEM.geometry = geometry;

		polygons.forEach(poly => {

			let matIndex = poly.matId || 0;
			this.MEM.vertexOfs = this.MEM.geometry.vertices.length;	
			this.API.readTriList(poly.tri_count, poly.triOfs, matIndex);
			this.API.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
			this.API.readVertices(poly.vert_count, poly.vertOfs, poly.boneId);

		});

		geometry.computeFaceNormals();
   		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		// Create mesh
		
		let mesh = new THREE.SkinnedMesh(buffer, materials);
		mesh.geometry.animations = this.MEM.animations;

		let armSkeleton = new THREE.Skeleton(this.MEM.bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);

		return mesh;

	}

	function api_meshCommon() {

		// Prepare Polygons

		const materials = [ 1006632997, 1044906031 ];
        for(let i = 0; i < materials.length; i++) {
            let canvas = Framebuffer.API.renderTexture(materials[i]);
            let texture = new THREE.Texture(canvas);
            texture.flipY = false;
            texture.needsUpdate = true;
            materials[i] = new THREE.MeshBasicMaterial({
                map : texture,
                skinning : true
            });
        }


		let polygons = [];

		this.API.readBody(polygons);

		// Read Geometry

		let geometry = new THREE.Geometry();
		this.MEM.geometry = geometry;

		polygons.forEach(poly => {

			let matIndex = poly.matId || 0;
			this.MEM.vertexOfs = this.MEM.geometry.vertices.length;	
			this.API.readTriList(poly.tri_count, poly.triOfs, matIndex);
			this.API.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
			this.API.readVertices(poly.vert_count, poly.vertOfs, poly.boneId);

		});

		geometry.computeFaceNormals();
   		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		// Create mesh
		
		let mesh = new THREE.SkinnedMesh(buffer, materials);
		mesh.geometry.animations = this.MEM.animations;

		let armSkeleton = new THREE.Skeleton(this.MEM.bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);

		return mesh;

	}

	function api_readBody(polygons) {

		const BODY_OFS = 0x00f5600;

		let ofs = BODY_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let count = (first - ofs) / 0x10;
		let weights = [ 0, 8, 9, 10, 12, 13 ];

		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "body",
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

	}

	function api_readHead(polygons) {

		const HEAD_OFS = 0x00f7800;

		let ofs = HEAD_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}
		let count = (first - ofs) / 0x10;
		let matId = [ 0, 1, 1, 1 ];
	
		let face = [];
		for(let i = 0; i < count; i++) {
			face.push({
				name : "head" + i,
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : 1,
				matId : matId[i]
			});
			ofs += 16;
		}
		
		if(face[0].tri_count === 68) {
			face[0].matId = 1;
		}

		face.forEach( p => {
			polygons.push(p); 
		});

	}

	function api_readLeftArm(polygons) {

		const LEFTARM_OFS = 0x00fa000;

		let ofs = LEFTARM_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let weights = [ 2, 3, 4 ];
		let count = (first - ofs) / 0x10;

		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "left arm" + i,
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

	}

	function api_readBusterArm(polygons) {

		const BUSTER_OFS = 0x00fdf00;

		let ofs = BUSTER_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let weights = [ 5, 6, 7 ];
		let count = (first - ofs) / 0x10;

		// let left = [];
		// this.API.readLeftArm(left);

		let buster = [];
		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "buster" + i,
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

	}

	function api_readRightArm(polygons) {

		const SPECIALWPN_OFS = 0x00fb500;
		const RIGHTARM_OFS = 0x00fca00;

		let ofs = RIGHTARM_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;
		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}
		let count = (first - ofs) / 0x10;
		let weights = [ 5, 6, 7];

		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "body",
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

	}

	function api_readSpecialArm(polygons) {

		const SPECIALWPN_OFS = 0x00fb500;

		let ofs = SPECIALWPN_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}
		let count = (first - ofs) / 0x10;
		let weights = [ 2, 3, 4];

		if(count <= 0) {
			return this.API.readLeftArm(polygons);
		}

		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "special_arm" + i,
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

	}

	function api_readFeet(polygons) {

		const FEET_OFS = 0x00ff400;

		let ofs = FEET_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let count = (first - ofs) / 0x10;
		let weights = [ 11, 14 ];

		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "body",
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

	}

	function api_readTriList(nbFace, ofs, matId) {

		if(ofs < 0) {
			return;
		}

		let materialIndex = matId || 0;
		let uv = new Array(3);

		for(let i = 0; i < nbFace; i++) {
			
			ofs += 40;
			
			for(let k = 0; k < uv.length; k++) {
				ofs += 4;
				uv[k] = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0) * 0.00390625 + 0.001953125,
					this.MEM.view.getUint8(ofs + 1) * 0.00390625 + 0.001953125
				);
				ofs += 4;
			}
			
			let a = this.MEM.view.getUint8(ofs + 0) + this.MEM.vertexOfs;
			let b = this.MEM.view.getUint8(ofs + 1) + this.MEM.vertexOfs;
			let c = this.MEM.view.getUint8(ofs + 2) + this.MEM.vertexOfs;
			ofs += 4;
			
			let face = new THREE.Face3(b, a, c);
			face.materialIndex = matId;
			this.MEM.geometry.faces.push(face);

			this.MEM.geometry.faceVertexUvs[0].push([uv[1], uv[0], uv[2]]);

		}
	}

	function api_readQuadList(nbFace, ofs, matId) {

		if(ofs < 0) {
			return;
		}

		let materialIndex = matId || 0;
		let uv = new Array(4);

		for(let i = 0; i < nbFace; i++) {
			
			ofs += 48;
			
			for(let k = 0; k < uv.length; k++) {
				ofs += 4;
				uv[k] = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0) * 0.00390625 + 0.001953125,
					this.MEM.view.getUint8(ofs + 1) * 0.00390625 + 0.001953125
				);
				ofs += 4;
			}
			
			let a = this.MEM.view.getUint8(ofs + 0) + this.MEM.vertexOfs;
			let b = this.MEM.view.getUint8(ofs + 1) + this.MEM.vertexOfs;
			let c = this.MEM.view.getUint8(ofs + 2) + this.MEM.vertexOfs;
			let d = this.MEM.view.getUint8(ofs + 3) + this.MEM.vertexOfs;
			ofs += 4;
			
			let face = new THREE.Face3(b, a, c);
			face.materialIndex = matId;
			this.MEM.geometry.faces.push(face);
			
			face = new THREE.Face3(d, b, c);
			face.materialIndex = matId;
			this.MEM.geometry.faces.push(face);

			this.MEM.geometry.faceVertexUvs[0].push([uv[1], uv[0], uv[2]]);
			this.MEM.geometry.faceVertexUvs[0].push([uv[3], uv[1], uv[2]]);

		}

	}

	function api_readVertices(count, ofs, boneId) {
		
		/*
		const SCALE = 0.00125;
		const ROT = new THREE.Matrix4().makeRotationX(Math.PI);
		*/

		for(let i = 0; i < count; i++) {

			let x = this.MEM.view.getInt16(ofs + 0, true) * SCALE;
			let y = this.MEM.view.getInt16(ofs + 2, true) * SCALE;
			let z = this.MEM.view.getInt16(ofs + 4, true) * SCALE;

			let vertex = new THREE.Vector3(x, y, z);
			vertex.applyMatrix4(ROT);

			if(boneId !== -1) {
				vertex.applyMatrix4(this.MEM.bones[boneId].matrixWorld);
			}

			this.MEM.geometry.vertices.push(vertex);
			this.MEM.geometry.skinIndices.push(new THREE.Vector4(boneId, 0, 0, 0));
			this.MEM.geometry.skinWeights.push(new THREE.Vector4(1, 0, 0, 0));

			ofs += 8;

		}

	}

	function api_loadTexture(url) {

		return new Promise( (resolve, reject) => {
			
			let loader = new THREE.TextureLoader();

			loader.load(url, texture => {
				resolve(texture);
			}, null, err => {
				reject(err);
			});

		});

	}

	function api_readAnimationTimings(ofs) {

		let first = this.MEM.view.getUint32(ofs, true) & 0xffffff;

		let timings = [];

		while(ofs < first) {
			
			let ptr = this.MEM.view.getUint32(ofs, true) & 0xffffff;
			ofs += 4;

			if(ptr === 0) {
				continue;
			}
			
			timings.push(ptr);

		}
		
		timings.forEach(timeOfs => {
			
			let last = -1;
			let index, frame, end;
			while(end !== 0xff) {

				index = this.MEM.view.getUint8(timeOfs + 0);
				frame = this.MEM.view.getUint8(timeOfs + 1);
				end = this.MEM.view.getUint8(timeOfs + 3);

				if(index !== last + 1) {
				}

				last = index;

				if(end & 0x80) {
					break;
				}
				
				timeOfs += 8;
			}

		});
	
	}

    function api_readBones(model) {

		const boneSrc = [
		  {
			"pos": {
			  "x": 0,
			  "y": 0.90625,
			  "z": -1.1098361617272889e-16
			},
			"name" : "bone_000",
			"parent": -1
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": 0.2575,
			  "z": -3.153465507804434e-17
			},
			"name" : "bone_001",
			"parent": 0
		  },
		  {
			"pos": {
			  "x": -0.17500000000000002,
			  "y": 0.1625,
			  "z": -0.01250000000000002
			},
			"name" : "bone_002",
			"parent": 0
		  },
		  {
			"pos": {
			  "x": -0.025,
			  "y": -0.2,
			  "z": 2.4492935982947065e-17
			},
			"name" : "bone_003",
			"parent": 2
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": -0.1525,
			  "z": 1.8675863686997135e-17
			},
			"name" : "bone_004",
			"parent": 3
		  },
		  {
			"pos": {
			  "x": 0.17500000000000002,
			  "y": 0.1625,
			  "z": -0.01250000000000002
			},
			"name" : "bone_005",
			"parent": 0
		  },
		  {
			"pos": {
			  "x": 0.025,
			  "y": -0.2,
			  "z": 2.4492935982947065e-17
			},
			"name" : "bone_006",
			"parent": 5
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": -0.1525,
			  "z": 1.8675863686997135e-17
			},
			"name" : "bone_007",
			"parent": 6
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": 0,
			  "z": 0
			},
			"name" : "bone_008",
			"parent": 0
		  },
		  {
			"pos": {
			  "x": -0.08125,
			  "y": -0.09125,
			  "z": 1.1174902042219598e-17
			},
			"name" : "bone_009",
			"parent": 8
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": -0.28125,
			  "z": 3.444319122601931e-17
			},
			"name" : "bone_010",
			"parent": 9
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": -0.34875,
			  "z": 4.2709557120263944e-17
			},
			"name" : "bone_011",
			"parent": 10
		  },
		  {
			"pos": {
			  "x": 0.08125,
			  "y": -0.09125,
			  "z": 1.1174902042219598e-17
			},
			"name" : "bone_012",
			"parent": 8
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": -0.28125,
			  "z": 3.444319122601931e-17
			},
			"name" : "bone_013",
			"parent": 12
		  },
		  {
			"pos": {
			  "x": 0,
			  "y": -0.34875,
			  "z": 4.2709557120263944e-17
			},
			"name" : "bone_014",
			"parent": 13
		  }
		];
		
		let bones = [];
		boneSrc.forEach(src => {
			
			let bone = new THREE.Bone();

			bone.position.x = src.pos.x;
			bone.position.y = src.pos.y;
			bone.position.z = src.pos.z;
			bone.name = src.name;

			if(bones[src.parent]) {
				bones[src.parent].add(bone);
			}
		
			bones.push(bone);

		});
		
		bones.forEach(bone=> {
			bone.updateMatrix();
			bone.updateMatrixWorld();
		});

		this.MEM.bones = bones;

    }

	function api_readAnim(model) {

		// First we want to get the timings
		let first = this.MEM.view.getUint32(model.animOfs, true) & 0xffffff;
		if(first === 0) {
			return;
		}

		let count = (first - model.animOfs) / 4;
		const possible = [];

		let animOfs = model.animOfs;
		let poseOfs = model.poseOfs;
		for(let i = 0; i < count; i++) {
			
			let animRef = {
				animOfs : this.MEM.view.getUint32(animOfs, true) & 0xffffff,
				poseOfs : this.MEM.view.getUint32(poseOfs, true) & 0xffffff
			}

			animOfs += 4;
			poseOfs += 4;

			if(animRef.animOfs !== 0 && animRef.poseOfs === 0) {
				continue;
			}

			if(animRef.animOfs === 0 && animRef.poseOfs !== 0) {
				continue;
			}

			if(animRef.animOfs === 0 && animRef.poseOfs === 0) {
				continue;
			}

			possible.push(animRef);
		}		
		
		// Now for each possible animation we get all of the poses
		
		possible.forEach(animRef => {
			
			first = this.MEM.view.getUint32(animRef.poseOfs, true) & 0xffffff;
			count = (first - animRef.poseOfs) / 4;
			animRef.poses = new Array(count);
			for(let i = 0; i < count; i++) {
				animRef.poses[i] = this.MEM.view.getUint32(animRef.poseOfs + i*4, true) & 0xffffff;
			}
			
			let stride = animRef.poses[1] - animRef.poses[0];
			animRef.timings = [];
			
			let pose, frame, frames, animOfs, end;
			animOfs = animRef.animOfs;
			
			frames = 0;
			frame = 0;

			while(end !== -1 && end !== -128) {
				pose = this.MEM.view.getUint8(animOfs + 0);
				frame = this.MEM.view.getUint8(animOfs + 1);
				end = this.MEM.view.getInt8(animOfs + 3);
				animOfs += 8;
				
				animRef.timings.push({
					ofs : animRef.poses[ pose ],
					frame : frame,
					time : frames / 30
				});

				if(end & 0x80) {
					break;
				}
				
				frames++;
			}

			let bones = this.MEM.bones;

            let str = this.MEM.animations.length.toString();
            while(str.length < 3) {
                str = "0" + str;
            }

            const ANIM = {
                name : "anim_" + str,
                fps : FPS,
                length : frames / FPS,
                hierarchy : []
            }

            for(let k = 0; k < bones.length; k++) {
                ANIM.hierarchy.push({
                    parent : k - 1,
                    keys : []
                });
            }
		
			animRef.timings.forEach(nop => {
				
				let ofs = nop.ofs;
				let view = this.MEM.view;
			
				for(let k = 0; k < this.MEM.bones.length; k++) {

					let key = {
						time : nop.time,
						rot : {
							x : 0,
							y : 0,
							z : 0,
						},
						scl : [1, 1, 1],
						pos : [
							bones[k].position.x,
							bones[k].position.y,
							bones[k].position.z
						]
					};

					switch(k) {
					case 0:

						let pos = {
                    		x : view.getInt16(ofs + 0, true) & 0xFFF,
                    		y : view.getInt16(ofs + 1, true) >> 4,
                    		z : view.getInt16(ofs + 3, true) & 0xFFF,
                		};
						ofs += 4;
						
						pos.x *= SCALE;
						pos.y *= SCALE;
						pos.z *= SCALE;
						
						let vec3 = new THREE.Vector3(pos.x, pos.y, pos.z);
						vec3.applyMatrix4(ROT);

						//key.pos[0] = vec3.x;
						key.pos[1] = vec3.y;
						//key.pos[2] = vec3.z;

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 1:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						
						ofs += 4;
						break;
					case 2:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 3:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;

						break;
					case 4:

						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						ofs += 4;

						break;
					case 5:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 6:
						
						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;

						break;
					case 7:

						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						ofs += 4;

						break;
					case 8:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 9:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						
						ofs += 4;
						break;
					case 10:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						ofs += 2;
						break;
					case 11:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						
						ofs += 4;

						break;
					case 12:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 13:
						
						if(nop.ofs === 0xdd7f8) {
							console.log("Offset: 0x%s", ofs.toString(16));
						}
						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						ofs += 1;

						break;
					case 14:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;

						break;
					}

					let e = new THREE.Euler(
						key.rot.x * Math.PI / 180,
						-key.rot.y * Math.PI / 180,
						-key.rot.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);
					key.rot = q;

					ANIM.hierarchy[k].keys.push(key);
				}
				
			});

            let clip = THREE.AnimationClip.parseAnimation(ANIM, bones);
            if(!clip) {
                return;
            }

            clip.optimize();
            this.MEM.animations.push(clip);
		});

	}

	function api_readAnimations(model, bones, animations) {


		if(model.animOfs === 0) {
			console.log("Something is not right here");
			return;
		}

		// First we read the controller

		let animOfs = model.animOfs;

		let first = this.MEM.view.getUint32(animOfs, true) & 0xffffff;


		let count = (first - animOfs) / 4;
		let poseOfs = model.poseOfs + 4;
		let anims = [];
		let animCount = 0;
		
		for(let i = 0; i < count; i++) {

			let anim = {
				time : this.MEM.view.getUint32(animOfs, true) & 0xffffff,
				pose : this.MEM.view.getUint32(poseOfs, true) & 0xffffff,
				timing : [],
				frames : 0,
				max : 0
			};

			animOfs += 4;
			poseOfs += 4;

			if(anim.time === 0) {
				continue;
			}
			
			animCount++;

			// Get timings
			
			const FPS = 30;

			let ptr = anim.time;
			let num, pose, frames, frame, end, max;
			num = 0;
			end = 0;
			max = 0;
			frames = 0;

			let firstPose = this.MEM.view.getUint32(anim.pose, true) & 0xffffff;
			max = (firstPose - anim.pose) / 4;

			while(end !== -1 && end !== -128) {
				pose = this.MEM.view.getUint8(ptr);
				frame = this.MEM.view.getUint8(ptr + 1);
				end = this.MEM.view.getInt8(ptr + 3);
				ptr += 8;
				
				if(pose >= max) {
					continue;
				}

				anim.timing.push({
					pose : pose,
					frame : frame,
					time : frames / 30
				});
				frames += frame;

				if(end & 0x80) {
					break;
				}

			}
			max++;

			// Read all of the pose tracks

			let ofs = anim.pose;
			let ptrList = [];

			for(let k = 0; k < max; k++) {
				ptrList[k] = this.MEM.view.getUint32(ofs, true) & 0xffffff;
				//ptrList[k] = ptrList[k].toString(16);
				ofs += 4;
			}

			let ptrs = [];
			for(let k = 0; k < anim.timing.length; k++) {
				anim.timing[k].ofs = ptrList[anim.timing[k].pose];
			}

			// Start creating animation

			let str = animations.length.toString();
			while(str.length < 3) {
				str = "0" + str;
			}
			
			const ANIM = {
				name : "anim_" + str,
				fps : FPS,
				length : frames / FPS,
				hierarchy : []
			}

			for(let k = 0; k < this.MEM.bones.length; k++) {
				ANIM.hierarchy.push({
					parent : k - 1,
					keys : []
				});
			}
		
			anim.timing.forEach(nop => {
				
				ofs = nop.ofs;
				let view = this.MEM.view;
			
				for(let k = 0; k < this.MEM.bones.length; k++) {

					let key = {
						time : nop.time,
						rot : {
							x : 0,
							y : 0,
							z : 0,
						},
						scl : [1, 1, 1],
						pos : [
							bones[k].position.x,
							bones[k].position.y,
							bones[k].position.z
						]
					};

					switch(k) {
					case 0:

						let pos = {
                    		x : view.getInt16(ofs + 0, true) & 0xFFF,
                    		y : view.getInt16(ofs + 1, true) >> 4,
                    		z : view.getInt16(ofs + 3, true) & 0xFFF,
                		};
						ofs += 4;
						
						pos.x *= SCALE;
						pos.y *= SCALE;
						pos.z *= SCALE;
						
						let vec3 = new THREE.Vector3(pos.x, pos.y, pos.z);
						vec3.applyMatrix4(ROT);

						//key.pos[0] = vec3.x;
						key.pos[1] = vec3.y;
						//key.pos[2] = vec3.z;

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 1:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						
						ofs += 4;
						break;
					case 2:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 3:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;

						break;
					case 4:

						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						ofs += 4;

						break;
					case 5:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 6:
						
						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;

						break;
					case 7:

						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						ofs += 4;

						break;
					case 8:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 9:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						
						ofs += 4;
						break;
					case 10:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						ofs += 2;
						break;
					case 11:

						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
						
						ofs += 4;

						break;
					case 12:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
						ofs += 5;

						break;
					case 13:
						
						if(nop.ofs === 0xdd7f8) {
							console.log("Offset: 0x%s", ofs.toString(16));
						}
						key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
						ofs += 1;

						break;
					case 14:

						key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
						key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
						key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;

						break;
					}

					let e = new THREE.Euler(
						key.rot.x * Math.PI / 180,
						-key.rot.y * Math.PI / 180,
						-key.rot.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);
					key.rot = q;

					ANIM.hierarchy[k].keys.push(key);
				}
				
			});
			
			let clip = THREE.AnimationClip.parseAnimation(ANIM, this.MEM.bones);

			if(!clip) {
				continue;
			}

			clip.optimize();
			animations.push(clip);
			
		}
		
		console.log(animations);
	}

	function api_debugAnimations() {

		console.log("debugging animations!!!");

		let animOfs = this.MEM.view.getUint32(0xdd608, true) & 0xffffff;
		console.log("Reading animation from: 0x%s", animOfs.toString(16));

		let firstOfs = this.MEM.view.getUint32(animOfs, true) & 0xffffff;
		let secondOfs = this.MEM.view.getUint32(animOfs + 4, true) & 0xffffff;

		let count = (firstOfs - animOfs) / 4;
		console.log(count);

		let len = secondOfs - firstOfs;
		let boneCount = Math.floor(len / 4);
		console.log("Animation length: 0x%s", len.toString(16));
		console.log("Bone Count: %d", boneCount);

	}


}).apply({});

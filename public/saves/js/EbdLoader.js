/*
 * @author - Kion
 */

"use strict";

const EbdLoader = (function() {

	const SCALE = 0.00125;
	const ROT = new THREE.Matrix4().makeRotationX(Math.PI);

	this.MEM = {};

	this.DOM = {};

	this.EVT = {
		handleMenuClick : evt_handleMenuClick.bind(this)
	};

	this.API = {
		prepare : api_prepare.bind(this),
		renderMenu : api_renderMenu.bind(this),
		renderModel : api_renderModel.bind(this),
		readBones : api_readBones.bind(this),
		readAnimations : api_readAnimations.bind(this),
		readPolygons : api_readPolygons.bind(this),
		readGeometry : api_readGeometry.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

	}

	function evt_handleMenuClick(evt) {

		for(let i = 0; i < this.MEM.list.length; i++) {
			this.MEM.list[i].classList.remove("active");
		}
	
		evt.target.classList.add("active");
		let index = this.MEM.list.indexOf(evt.target);
		let mesh = this.API.renderModel(index);
		
		//mesh.rotation.z = Math.PI;
		//mesh.scale.x = -1;
		StateViewer.API.setModel(mesh);

	}

	function api_prepare(mem) {

		const MAX = 100;
		const OFFSET = [ 0x16c000, 0x190000, 0x1f2000 ];
		
		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);
		this.MEM.models = [];

		let ofs = OFFSET[0];
		let count = this.MEM.view.getUint32(ofs, true);
		ofs += 4;
		
		let st = this.MEM.view.getUint8(0xC356E).toString(16);
		if(st.length < 2) {
			st = "0" + st;
		}
		st = "st" + st + "_em";

		for(let i = 0; i < count; i++) {
			let m = {
				id : this.MEM.view.getUint32(ofs + 0, true),
				meshOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				poseOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				animOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff
			};

			m.name = st + m.id.toString(16);
			this.MEM.models.push(m);
			ofs += 0x10;
		}

		return count;

	}

	function api_renderMenu() {
		
		const ul = document.createElement("ul");
		
		this.MEM.list = [];
		for(let i = 0; i < this.MEM.models.length; i++) {
			
			let li = document.createElement("li");
			li.textContent = this.MEM.models[i].name;
			li.addEventListener("click", this.EVT.handleMenuClick);
			ul.appendChild(li);
			this.MEM.list.push(li);

		}

		return ul;

	}

	function api_renderModel(index) {
		
		const model = this.MEM.models[index];
		
		// Read Mesh
		
		let ofs = model.meshOfs + 0x10;
		const polygon_count = this.MEM.view.getUint8(ofs + 1);
		const hierarchy = new Array(polygon_count);
		let bone_count = 0;

		for(let i = 0; i < polygon_count; i++) {

			let poly = {
				id : this.MEM.view.getUint8(ofs + 0),
				parent : this.MEM.view.getUint8(ofs + 1),
				child : this.MEM.view.getUint8(ofs + 2),
				flags : this.MEM.view.getUint8(ofs + 3)
			};

			if(poly.child > bone_count) {
				bone_count = poly.child;
			}

			hierarchy[i] = poly;
			ofs += 4;
		}

		const bones = new Array(bone_count + 1);
		const animations = new Array();

		if(model.poseOfs) {
			this.API.readBones(model, bones, hierarchy);
		}

		if(model.animOfs) {
			this.API.readAnimations(model, bones, animations);
		}

		// Read level of detail
		
		const highOfs = this.MEM.view.getUint32(model.meshOfs + 0x70, true) & 0xffffff;
		const medOfs = this.MEM.view.getUint32(model.meshOfs + 0x74, true) & 0xffffff;
		const lowOfs = this.MEM.view.getUint32(model.meshOfs + 0x78, true) & 0xffffff;

		// First Read high level of detail model
		
		let polygons = this.API.readPolygons(highOfs, polygon_count, hierarchy);
		
		/*
		// Patch mesh polygons
		switch(model.id) {
		case 0x540:
		case 0x640:
		case 0xb40:

			polygons[19].skip = false;
			polygons[19].bone_id = 1;
			polygons[17].skip = true;

			break;
		case 0x940:

			console.log(polygons);
			polygons[17].skip = true;
			polygons[16].skip = true;
			// polygons[15].skip = true; // eyes

			polygons[18].skip = false;
			polygons[18].bone_id = 1;

			break;
		}
		*/

		if(polygons.length >= 19 && model.id & 0x40) {

			polygons[18].skip = false;
			polygons[18].bone_id = 1;
			polygons[16].skip = true;

		}

		let mesh = this.API.readGeometry(polygons, model.poseOfs ? bones : null);

		mesh.geometry.animations = animations;
		mesh.name = model.name;
		return mesh;

	}

	function api_readBones(model, bones, hierarchy) {
		
		let poseOfs = model.poseOfs;
		let boneOfs = this.MEM.view.getUint32(poseOfs, true) & 0xffffff;

		for(let i = 0; i < bones.length; i++) {

			let x = this.MEM.view.getInt16(boneOfs + 0, true) * SCALE;
			let y = this.MEM.view.getInt16(boneOfs + 2, true) * SCALE;
			let z = this.MEM.view.getInt16(boneOfs + 4, true) * SCALE;

			let vec3 = new THREE.Vector3(x, y, z);
			vec3.applyMatrix4(ROT);

			boneOfs += 8;

			let bone = new THREE.Bone();
			bone.position.x = vec3.x;
			bone.position.y = vec3.y;
			bone.position.z = vec3.z;

			let str = i.toString();
			while(str.length < 3) {
				str = "0" + str;
			}
			bone.name = "bone_" + str;
			bones[i] = bone;

			if(i === 0) {
				continue;
			}

			let parent = hierarchy[i].parent;
			bones[parent].add(bone);

		}
	
		bones.forEach(bone=> {
			bone.updateMatrix();
			bone.updateMatrixWorld();
		});

	}

	function api_readAnimations(model, bones, animations) {

		if(model.animOfs === 0) {
			return;
		}

		console.log("Reading animation at: 0x%s", model.animOfs.toString(16));

		// First we read the controller

		let animOfs = model.animOfs;
		let first = this.MEM.view.getUint32(animOfs, true) & 0xffffff;
		let count = (first - animOfs) / 4;
		// count--;
		// animOfs += 4;
		let poseOfs = model.poseOfs + 4;
		let anims = [];
		let animCount = 0;
	
		for(let i = 0; i < count; i++) {

			let anim = {
				time : this.MEM.view.getUint32(animOfs, true) & 0xffffff,
				pose : this.MEM.view.getUint32(poseOfs, true) & 0xffffff,
				timing : [],
				frames : 0,
				max : 0
			};

			animOfs += 4;
			poseOfs += 4;

			if(anim.time === 0) {
				continue;
			}
	
			animCount++;

			// Get timings
			
			const FPS = 30;

			let ptr = anim.time;
			let num, pose, frames, frame, end;
			num = 0;
			end = 0;
			frames = 0;

			let firstPose = this.MEM.view.getUint32(anim.pose, true) & 0xffffff;

			while(end !== -1 && end !== -128) {
				pose = this.MEM.view.getUint8(ptr);
				frame = this.MEM.view.getUint8(ptr + 1);
				end = this.MEM.view.getInt8(ptr + 3);
				let term = this.MEM.view.getUint8(ptr + 3);

				ptr += 8;

				anim.timing.push({
					pose : pose,
					frame : frame,
					time : frames / FPS
				});

				if(term & 0x80) {
					break;
				}

				frames += 1;

			}

			console.log(frames.toString(16));

			// Read all of the pose tracks

			let ofs = anim.pose;
			let ptrList = [];

			const max = (firstPose - anim.pose) / 4;
			for(let k = 0; k < max; k++) {
				ptrList[k] = this.MEM.view.getUint32(ofs, true) & 0xffffff;
				ofs += 4;
			}

			let ptrs = [];
			for(let k = 0; k < anim.timing.length; k++) {
				anim.timing[k].ofs = ptrList[anim.timing[k].pose];
			}

			// Start creating animation

			let str = animations.length.toString();
			while(str.length < 3) {
				str = "0" + str;
			}
			
			const ANIM = {
				name : "anim_" + str,
				fps : FPS,
				length : frames / FPS,
				hierarchy : []
			}

			for(let k = 0; k < bones.length; k++) {
				ANIM.hierarchy.push({
					parent : k - 1,
					keys : []
				});
			}
			
			anim.timing.forEach(nop => {
				
				ofs = nop.ofs;
				let view = this.MEM.view;

				let pos = {
					x : view.getUint16(ofs + 0, true) & 0xFFF,
					y : view.getUint16(ofs + 1, true) >> 4,
					z : view.getUint16(ofs + 3, true) & 0xFFF,
				};
				
				if (pos.x & 0x800) {
					pos.x = (0x800 - (pos.x & 0x7ff));
				}
				
				if (pos.y & 0x800) {
					pos.y = (0x800 - (pos.y & 0x7ff));
				}
				
				if (pos.z & 0x800) {
					pos.z = (0x800 - (pos.z & 0x7ff));
				}
				
				pos.x *= SCALE;
				pos.y *= SCALE;
				pos.z *= SCALE;
				
				let vec3 = new THREE.Vector3(pos.x, pos.y, pos.z);
				vec3.applyMatrix4(ROT);

				ofs += 4;

				for(let k = 0; k < bones.length; k++) {
					
					let r;
					if ((k % 2) === 0) {
						r = {
							x : (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360,
							y : (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360,
							z : (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360,
						};
						ofs += 5;
					} else {
						r = {
							x : (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360,
							y : (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360,
							z : (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360,
						};
						ofs += 4;
					}

					let e = new THREE.Euler(
						r.x * Math.PI / 180,
						-r.y * Math.PI / 180,
						-r.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);

					let key = {
						time : nop.time,
						rot : q.toArray(),
						scl : [1, 1, 1],
						pos : [
							bones[k].position.x,
							bones[k].position.y,
							bones[k].position.z
						]
					};

					ANIM.hierarchy[k].keys.push(key);
				}

			});
			
			if(ANIM.hierarchy[0].keys.length === 1) {
				continue;
			}

			let clip = THREE.AnimationClip.parseAnimation(ANIM, bones);
			if(!clip) {
				continue;
			}
			clip.optimize();
			animations.push(clip);
			
		}
		
	}

	function api_readPolygons(ofs, count, hierarchy) {

		// First we calculate the actual number of polygons
		
		ofs += 0x14;
		let triOfs = this.MEM.view.getUint32(ofs + 0x04, true) & 0xffffff;
		let quadOfs = this.MEM.view.getUint32(ofs + 0x08, true) & 0xffffff;

		triOfs = triOfs || 0xffffff;
		quadOfs = quadOfs || 0xffffff;

		const end = triOfs < quadOfs ? triOfs : quadOfs;
		let polyCount = (end - ofs) / 0x14;
		
		if(polyCount < 0) {
			polyCount = count;
		}
		console.log(polyCount);

		let polygons = new Array(polyCount);
		
		for(let i = 0; i < polyCount; i++) {

			polygons[i] = {
				tri_count : this.MEM.view.getUint8(ofs + 0x00),
				quad_count : this.MEM.view.getUint8(ofs + 0x01),
				vert_count : this.MEM.view.getUint8(ofs + 0x02),
				id : this.MEM.view.getUint8(ofs + 0x03),
				triOfs : this.MEM.view.getUint32(ofs + 0x04, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 0x08, true) & 0xffffff,
				texture : this.MEM.view.getUint32(ofs + 0x0c, true),
				vertOfs : this.MEM.view.getUint32(ofs + 0x10, true) & 0xffffff,
				skip : i >= count,
				bone_id : -1
			};

			if(polygons[i].triOfs === 0) {
				polygons[i].tri_count = 0;
			}

			if(polygons[i].quadOfs === 0) {
				polygons[i].quad_count = 0;
			}

			if(polygons[i].vertOfs === 0) {
				polygons[i].vert_count = 0;
			}
			
			let id = polygons[i].id;
			if(id < hierarchy.length) {
				polygons[i].bone_id = hierarchy[id].child;
			}
		
			ofs += 0x14;
			
		}
		
		return polygons;

	}

	function api_readGeometry(polygons, bones) {

		let materials = new Array();
		let geometry = new THREE.Geometry();
		
		for(let i = 0; i < polygons.length; i++) {
			
			let poly = polygons[i];
			if(poly.skip) {
				continue;
			}

			if(materials.indexOf(poly.texture) === -1) {
				materials.push(poly.texture);
			}

		}
		
		let images = [];
		let mats = [];

		for(let i = 0; i < materials.length; i++) {
			let canvas = Framebuffer.API.renderTexture(materials[i]);
			let texture = new THREE.Texture(canvas);
			texture.flipY = false;
			texture.needsUpdate = true;
			mats[i] = new THREE.MeshBasicMaterial({
				map : texture,
				skinning : true
			});
			let ctx = canvas.getContext("2d");
			images.push(ctx);
		}
		
		for(let i = 0; i < polygons.length; i++) {
			
			let poly = polygons[i];
			if(poly.skip) {
				continue;
			}

			let ofs;
			let boneId = poly.bone_id;
			let matIndex = materials.indexOf(poly.texture);
			
			ofs = poly.triOfs;
			for(let k = 0; k < poly.tri_count; k++) {
				
				let auv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x00) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x01) * 0.00390625 + 0.001953125)
				);
				
				let buv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x02) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x03) * 0.00390625 + 0.001953125)
				);
				
				let cuv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x04) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x05) * 0.00390625 + 0.001953125)
				);

				let a = this.MEM.view.getUint8(ofs + 0x08) + geometry.vertices.length;
				let b = this.MEM.view.getUint8(ofs + 0x09) + geometry.vertices.length;
				let c = this.MEM.view.getUint8(ofs + 0x0a) + geometry.vertices.length;
				
				let face = new THREE.Face3(b, a, c);
				face.materialIndex = matIndex;
				geometry.faces.push(face);
				
				geometry.faceVertexUvs[0].push([buv, auv, cuv]);
				ofs += 0x0c;
				
				// Check for transparent

                let xmin = 255;
                let ymin = 255;
                let xmax = 0;
                let ymax = 0;

                let coords = [{
                    u : this.MEM.view.getUint8(ofs + 0),
                    v : this.MEM.view.getUint8(ofs + 1)
                },{
                    u : this.MEM.view.getUint8(ofs + 2),
                    v : this.MEM.view.getUint8(ofs + 3)
                },{
                    u : this.MEM.view.getUint8(ofs + 4),
                    v : this.MEM.view.getUint8(ofs + 5)
                }];

                coords.forEach(coord => {

                    if(coord.u < xmin) {
                        xmin = coord.u;
                    }

                    if(coord.v < ymin) {
                        ymin = coord.v;
                    }

                    if(coord.u > xmax) {
                        xmax = coord.u;
                    }

                    if(coord.v > ymax) {
                        ymax = coord.v;
                    }

                });

                let width = xmax - xmin;
                let height = ymax - ymin;
                let ctx = images[matIndex];

                width = width || 1;
                height = height || 1;

                let data = ctx.getImageData(xmin, ymin, width, height);

                for(let i = 0; i < data.data.length; i+= 4) {
                    let r = data.data[i + 0];
                    let g = data.data[i + 1];
                    let b = data.data[i + 2];
                    let a = data.data[i + 3];

                    if(a !== 0) {
                        continue;
                    }

                    mats[matIndex].transparent = true;
                    mats[matIndex].alphaTest = 0.05;
                    break;
                }

			}
			
			ofs = poly.quadOfs;
			for(let k = 0; k < poly.quad_count; k++) {
				
				let auv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x00) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x01) * 0.00390625 + 0.001953125)
				);
				
				let buv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x02) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x03) * 0.00390625 + 0.001953125)
				);
				
				let cuv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x04) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x05) * 0.00390625 + 0.001953125)
				);
				
				let duv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x06) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x07) * 0.00390625 + 0.001953125)
				);

				let a = this.MEM.view.getUint8(ofs + 0x08) + geometry.vertices.length;
				let b = this.MEM.view.getUint8(ofs + 0x09) + geometry.vertices.length;
				let c = this.MEM.view.getUint8(ofs + 0x0a) + geometry.vertices.length;
				let d = this.MEM.view.getUint8(ofs + 0x0b) + geometry.vertices.length;
				
				let face = new THREE.Face3(b, a, c);
				face.materialIndex = matIndex;
				geometry.faces.push(face);
				
				face = new THREE.Face3(d, b, c);
				face.materialIndex = matIndex;
				geometry.faces.push(face);
				
				geometry.faceVertexUvs[0].push([buv, auv, cuv]);
				geometry.faceVertexUvs[0].push([duv, buv, cuv]);

				ofs += 0x0c;
				
				// Check for transparent

                let xmin = 255;
                let ymin = 255;
                let xmax = 0;
                let ymax = 0;

                let coords = [{
                    u : this.MEM.view.getUint8(ofs + 0),
                    v : this.MEM.view.getUint8(ofs + 1)
                },{
                    u : this.MEM.view.getUint8(ofs + 2),
                    v : this.MEM.view.getUint8(ofs + 3)
                },{
                    u : this.MEM.view.getUint8(ofs + 4),
                    v : this.MEM.view.getUint8(ofs + 5)
                },{
                    u : this.MEM.view.getUint8(ofs + 6),
                    v : this.MEM.view.getUint8(ofs + 7)
                }];

                coords.forEach(coord => {

                    if(coord.u < xmin) {
                        xmin = coord.u;
                    }

                    if(coord.v < ymin) {
                        ymin = coord.v;
                    }

                    if(coord.u > xmax) {
                        xmax = coord.u;
                    }

                    if(coord.v > ymax) {
                        ymax = coord.v;
                    }

                });

                let width = xmax - xmin;
                let height = ymax - ymin;
                let ctx = images[matIndex];

                width = width || 1;
                height = height || 1;

                let data = ctx.getImageData(xmin, ymin, width, height);

                for(let i = 0; i < data.data.length; i+= 4) {
                    let r = data.data[i + 0];
                    let g = data.data[i + 1];
                    let b = data.data[i + 2];
                    let a = data.data[i + 3];

                    if(a !== 0) {
                        continue;
                    }

                    mats[matIndex].transparent = true;
                    mats[matIndex].alphaTest = 0.05;
                    break;
                }


			}
			
			ofs = poly.vertOfs;
			for(let k = 0; k < poly.vert_count; k++) {

				let x = this.MEM.view.getInt16(ofs + 0, true) * SCALE;
				let y = this.MEM.view.getInt16(ofs + 2, true) * SCALE;
				let z = this.MEM.view.getInt16(ofs + 4, true) * SCALE;
				let vertex = new THREE.Vector3(x, y, z);
				vertex.applyMatrix4(ROT);
				geometry.vertices.push(vertex);

				ofs += 8;

				if(!bones) {
					continue;
				}

				geometry.skinIndices.push(new THREE.Vector4(boneId, 0, 0, 0));
				geometry.skinWeights.push(new THREE.Vector4(1, 0, 0, 0));
				if(boneId !== -1) {
					vertex.applyMatrix4(bones[boneId].matrixWorld);
				}
			}

		}

		geometry.computeFaceNormals();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		let material = new THREE.MeshNormalMaterial();
		let mesh; 
		
		if(!bones) {
			mesh = new THREE.Mesh(buffer, mats);
		} else {
			mesh = new THREE.SkinnedMesh(buffer, mats);
			let armSkeleton = new THREE.Skeleton(bones);
			let rootBone = armSkeleton.bones[0];
			mesh.add(rootBone);
			mesh.bind(armSkeleton);
		}

		return mesh;

	}

}).apply({});

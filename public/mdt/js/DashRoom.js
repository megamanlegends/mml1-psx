const DashViewer = (function() {

	this.MEM = {
		db : new Dexie('mml1-psx'),
		scene : new THREE.Scene(),
		camera : new THREE.PerspectiveCamera(50,1,0.1,20000),
		renderer : new THREE.WebGLRenderer(),
		opts : {
			LOD : "high",
			FORMAT : "gltf"
		}
	};

	this.DOM = {
		menu : {
			open : document.getElementById('DashViewer.menu.open'),
			download : document.getElementById('DashViewer.menu.save'),
			settings : document.getElementById('DashViewer.menu.settings'),
			source : document.getElementById('DashViewer.menu.source'),
			files : document.getElementById('DashViewer.menu.source.files'),
			options : document.getElementById('DashViewer.menu.options')
		},
		area : {
			stageSelect : document.getElementById('DashViewer.area.stageSelect'),
			roomSelect : document.getElementById('DashViewer.area.roomSelect'),
			viewport: document.getElementById('DashViewer.area.viewport'),
			pallet : document.getElementById('DashViewer.area.pallet')
		},
		format : {
			gltf : document.getElementById('DashViewer.format.gltf'),
			glb : document.getElementById('DashViewer.format.glb')
		},
		lod : {
			high : document.getElementById('DashViewer.lod.high'),
			medium : document.getElementById('DashViewer.lod.medium'),
			low : document.getElementById('DashViewer.lod.low')
		}
	}

	this.EVT = {
		handleOpenClick : evt_handleOpenClick.bind(this),
		handleFileChange : evt_handleFileChange.bind(this),
		handleWindowResize : evt_handleWindowResize.bind(this),
		handleStageSelect : evt_handleStageSelect.bind(this),
		handleRoomSelect : evt_handleRoomSelect.bind(this),
		handleSettingsClick : evt_handleSettingsClick.bind(this),
		handleSourceClick : evt_handleSourceClick.bind(this),
		handleDownloadClick : evt_handleDownloadClick.bind(this),
		handleOptionsClick : evt_handleOptionsClick.bind(this)
	};

	this.API = {
		readFile : api_readFile.bind(this),
		animate : api_animation.bind(this),
		setViewport : api_setViewport.bind(this),
		loadStages : api_loadStages.bind(this),
		loadRooms : api_loadRooms.bind(this),
		renderTexture : api_renderTexture.bind(this),
		loadTile : api_loadTile.bind(this),
		formatDec : api_formatDec.bind(this),
		formatHex : api_formatHex.bind(this),
		formatBin : api_formatBin.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		let localOpts = localStorage.getItem('mml-psx');
		if(localOpts) {
			localOpts = JSON.parse(localOpts);
			for(let key in localOpts) {
				this.MEM.opts[key] = localOpts[key];
			}
		}

		this.DOM.format[this.MEM.opts.FORMAT].classList.add('active');
		this.DOM.lod[this.MEM.opts.LOD].classList.add('active');

		this.MEM.db.version(1).stores({files : '&name'});
		this.MEM.db.open();

		this.MEM.camera.position.z = -55;
		this.MEM.camera.position.y = 25;
		this.MEM.camera.position.x = 0;
		this.MEM.camera.lookAt(new THREE.Vector3(0, 25, 0));

		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0xeeeeee, 1);

		this.MEM.scene.add(new THREE.GridHelper(100, 10));
		this.MEM.scene.add(new THREE.AmbientLight(0xffffff));
		this.DOM.area.viewport.appendChild(this.MEM.renderer.domElement);
		THREE.OrbitControls(this.MEM.camera, this.MEM.renderer.domElement);
		
		this.DOM.area.stageSelect.addEventListener("click", this.EVT.handleStageSelect);
		this.DOM.area.roomSelect.addEventListener("click", this.EVT.handleRoomSelect);
		window.addEventListener("resize", this.EVT.handleWindowResize);
		this.API.setViewport();
		this.API.animate();
		this.API.loadStages();

		this.DOM.menu.open.addEventListener("click", this.EVT.handleOpenClick);
		this.DOM.menu.files.addEventListener("change", this.EVT.handleFileChange);
		
		// Settings
		
		this.DOM.menu.settings.addEventListener("click", this.EVT.handleSettingsClick);
		this.DOM.menu.source.addEventListener("click", this.EVT.handleSourceClick);
		this.DOM.menu.download.addEventListener("click", this.EVT.handleDownloadClick);
		this.DOM.menu.options.addEventListener("click", this.EVT.handleOptionsClick);

	}

	function evt_handleOpenClick() {
		
		this.DOM.menu.files.click();

	}

	function evt_handleWindowResize() {
		
		this.API.setViewport();
		
	}

	async function evt_handleFileChange(evt) {
		
		for(let i = 0; i < evt.target.files.length; i++) {
			
			let file = evt.target.files[i];
			let query = {};
			query.name = file.name.toUpperCase();
			
			try {
				query.data = await this.API.readFile(file);
			} catch(err) {
				continue;
			}
			
			this.MEM.db.files.put(query);

		}

		this.API.loadStages();
	
	}

	function evt_handleStageSelect(evt) {

		let elem = evt.target;
		if(elem.tagName !== "LI") {
			return;
		}

		let filename = elem.textContent;
		this.API.loadRooms(filename);

		for(let i = 0; i < this.DOM.area.stageSelect.children.length; i++) {
			this.DOM.area.stageSelect.children[i].classList.remove("active");
		}

		elem.classList.add("active");

	}

	function evt_handleRoomSelect(evt) {

		let elem = evt.target;

		if(this.MEM.mesh) {
			this.MEM.scene.remove(this.MEM.mesh);
		}

		let num = elem.textContent.split(" ").pop();
		let stg = this.MEM.file.name.split(".").shift();

		let lookup = {
			high : "data-high",
			med : "data-med",
			low : "data-low"
		}

		let attr = lookup[this.MEM.opts.LOD];

		switch(elem.getAttribute("data-type")) {
		case "stg":

			let ofs = elem.getAttribute(attr);
			ofs = parseInt(ofs);
			this.API.loadTile(ofs, stg + "_" + num);

			// Load single tile type

			break;
		default:
			return;
			break;
		}

		for(let i = 0; i < this.DOM.area.roomSelect.children.length; i++) {
			this.DOM.area.roomSelect.children[i].classList.remove("active");
		}

		elem.classList.add("active");

	}

	function evt_handleSelectChange(ptr) {
		
		let palIndex = ptr.pal.selectedIndex;
		let imgIndex = ptr.img.selectedIndex;

		let pallet = ptr.pallets[palIndex];
		let image = ptr.images[imgIndex];
		
		ptr.material.transparent = ptr.check.checked;
		ptr.material.map.name = ptr.pal.options[palIndex].textContent;
		
		// Update debug table

		this.API.renderTexture(ptr, pallet, image);

	}

	function evt_handleSettingsClick(evt) {

		this.DOM.menu.options.classList.toggle("hide");
		evt.stopPropagation();

	}

	function evt_handleSourceClick() {

		window.location.href = "https://gitlab.com/megamanlegends/mml1-psx";

	}

    function evt_handleDownloadClick() {

        if(!this.MEM.mesh) {
            return;
        }

        let expt, opts;

        switch(this.MEM.opts.FORMAT) {
        case "gltf":

            expt = new THREE.GLTFExporter();
            opts = {
                binary : false
            }

            expt.parse(this.MEM.mesh, result => {

                let mime = {type:"model/gltf+json"};
                let blob = new Blob([JSON.stringify(result)], mime);
                saveAs(blob, this.MEM.mesh.name + ".gltf");

            }, opts);

            break;
        case "glb":

            expt = new THREE.GLTFExporter();
            opts = {
                binary : true
            }

            expt.parse(this.MEM.mesh, result => {

                let mime = {type:"application/octet-stream"};
                let blob = new Blob([result], mime);
                saveAs(blob, this.MEM.mesh.name + ".glb");

            }, opts);

            break;
        }

    }

    function evt_handleOptionsClick(evt) {

        evt.stopPropagation();

        if(evt.target.tagName === "LABEL") {
            return;
        }

        let id = evt.target.getAttribute("id").split(".").pop();

        switch(evt.target) {
        case this.DOM.format.gltf:
        case this.DOM.format.glb:
            for(let key in this.DOM.format) {
                this.DOM.format[key].classList.remove('active');
            }
            this.MEM.opts.FORMAT = id;
            break;
        case this.DOM.lod.high:
        case this.DOM.lod.medium:
        case this.DOM.lod.low:
            for(let key in this.DOM.lod) {
                this.DOM.lod[key].classList.remove('active');
            }
            this.MEM.opts.LOD = id;
            break;
        }

        evt.target.classList.add('active');
        localStorage.setItem('mml-psx', JSON.stringify(this.MEM.opts));

    }



	function api_readFile(file) {

		return new Promise( (resolve, reject) => {

			let reader = new FileReader();

			reader.onload = function(e) {
				resolve(e.target.result);
			}

			reader.onerror = function(err) {
				reject(err);
			}

			reader.readAsArrayBuffer(file);

		});

	}

	function api_animation() {

		requestAnimationFrame(this.API.animate);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

	}

	function api_setViewport() {

		let width = this.DOM.area.viewport.offsetWidth;
		let height = this.DOM.area.viewport.offsetHeight;

		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();
		this.MEM.renderer.setSize( width, height);

	}

	function api_loadStages() {
		
		this.MEM.TIM = [];
		this.DOM.area.stageSelect.innerHTML = "";

		this.MEM.db.files.each(file => {

			let view = new DataView(file.data);
			let ofs = 0;
			let len = file.data.byteLength;
			let found = false;

			do {

				let dots = view.getUint16(ofs + 0x40, true);
				if(dots !== 0x2e2e) {
					continue;
				}
				let length = view.getUint32(ofs + 0x04, true);

				let str = "";
				for(let i = 0x40; i < 0x60; i++) {
					let c = view.getUint8(ofs + i);
					if(c === 0) {
						break;
					}
					str += String.fromCharCode(c);
				}

				let ext = str.split(".").pop();

				switch(ext) {
				case "STG":
					found = true;
					break;
				case "TIM":

					this.MEM.TIM.push({
						offset : ofs,
						pallet_x : view.getUint32(ofs + 0x0c, true),
						pallet_y : view.getUint32(ofs + 0x10, true),
						nb_colors : view.getUint32(ofs + 0x14, true),
						nb_pallet : view.getUint32(ofs + 0x18, true),
						image_x : view.getUint32(ofs + 0x1c, true),
						image_y : view.getUint32(ofs + 0x20, true),
						width : view.getUint32(ofs + 0x24, true),
						height : view.getUint32(ofs + 0x28, true),
						view : view,
						name : str.toUpperCase()
					});

					break;
				}

			} while( (ofs += 0x400) < len);
			
			if(!found) {
				return;
			}

			let li = document.createElement("li");
			li.textContent = file.name;
			this.DOM.area.stageSelect.appendChild(li);

			if(!this.MEM.psxM) {
				this.API.loadRooms(file.name);
				li.classList.add("active");
			}

		});

	}

	async function api_loadRooms(name) {
		
		this.MEM.psxM = new ArrayBuffer(0x200000);
		this.MEM.view = new DataView(this.MEM.psxM);

		let file;

		try {
			file = await this.MEM.db.files.get({name:name});	
		} catch(err) {
			throw err;
		}
		
		let ofs = 0;
		let len = file.data.byteLength;
		let view = new DataView(file.data);
		this.MEM.file = file;
		let mdtLen = 0;

		do {
			
			let dots = view.getUint16(ofs + 0x40, true);
			if(dots !== 0x2e2e) {
				continue;
			}

			let length = view.getUint32(ofs + 0x04, true);
			let memory = view.getUint32(ofs + 0x0c, true);
			memory = memory & 0xFFFFFF;

			let str = "";
			for(let i = 0x40; i < 0x60; i++) {
				let c = view.getUint8(ofs + i);
				if(c === 0) {
					break;
				}
				str += String.fromCharCode(c);
			}

			let ext = str.split(".").pop();
			ofs += 0x800;
			let found = false;

			switch(ext) {
			case "MDT":
			case "IDX":
			case "STG":
			
				this.MEM[ext] = memory;
				for(let i = 0; i < length; i+=4) {
					this.MEM.psxM[memory + i] = file.data[ofs + i];
					let d = view.getUint32(ofs + i, true);
					this.MEM.view.setUint32(memory + i, d, true);
				}

				break;
			}
			
		} while( (ofs += 0x400) < len);
		
		this.DOM.area.roomSelect.innerHTML = "";

		// Load tile types
	
		ofs = this.MEM.STG;
		let first = this.MEM.view.getUint32(ofs + 0x18, true);
		first &= 0xffffff;
		let count = (first - ofs) / 0x30;

		for(let i = 0; i < count; i++) {

			let highOfs = this.MEM.view.getUint32(ofs + 0x18, true);
			highOfs &= 0xffffff;

			let medOfs = this.MEM.view.getUint32(ofs + 0x1c, true);
			medOfs &= 0xffffff;

			let lowOfs = this.MEM.view.getUint32(ofs + 0x20, true);
			lowOfs &= 0xffffff;

			let li = document.createElement("li");
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}
			
			li.textContent = "Tile Type " + num;
			li.setAttribute("data-type", "stg");
			li.setAttribute("data-high", highOfs);
			li.setAttribute("data-med", medOfs);
			li.setAttribute("data-low", lowOfs);
			this.DOM.area.roomSelect.appendChild(li);

			ofs += 0x30;

		}

	}

	function api_renderTexture(ptr, pallet, tim) {
		
		let ctx = ptr.ctx;
		let image_x = ptr.image_x;
		let image_y = ptr.image_y;
		let pallet_x = ptr.pallet_x;
		let pallet_y = ptr.pallet_y;

		let px = this.API.formatDec(pallet.pallet_x) + " " + this.API.formatBin(pallet.pallet_x);
		let py = this.API.formatDec(pallet.pallet_y) + " " + this.API.formatBin(pallet.pallet_y);

		let ix = this.API.formatDec(tim.image_x) + " " + this.API.formatBin(tim.image_x);
		let iy = this.API.formatDec(tim.image_y) + " " + this.API.formatBin(tim.image_y);


		ptr.debug.pallet_x.textContent = px;
		ptr.debug.pallet_y.textContent = py;

		ptr.debug.image_x.textContent = ix;
		ptr.debug.image_y.textContent = iy;

		ctx.clearRect(0, 0, 256, 256);

		let view = pallet.view;
		let diff = pallet_x - pallet.pallet_x;
		let colors = new Array(pallet.nb_colors);
		let ofs = pallet.offset + 0x100 + diff;
		
		for(let i = 0; i < colors.length; i++) {
			let color = view.getUint16(ofs, true);
			let r = ((color >> 0x00) & 0x1f) << 3;
			let g = ((color >> 0x05) & 0x1f) << 3;
			let b = ((color >> 0x0a) & 0x1f) << 3;
			let a = color > 0 ? 1 : 0;
			colors[i] = "rgba(" + r + "," + g + "," + b + "," + a + ")";
			ofs += 2;
		}

		ofs = tim.offset + 0x800;
		view = tim.view;

		let image = {};
		image.inc = 1;
		image.block_width = 64;
		image.block_height = 32;
		image.colors = tim.nb_colors;
		image.x = tim.image_x;
		image.y = tim.image_y;
		image.width = tim.width * 2;
		image.height = tim.height;

		if(tim.nb_colors === 16) {
			image.inc *= 2;
			image.block_width *= 2;
			image.width *= 2;
		}

		image.body = new Array(image.width * image.height);

		for (let y = 0; y < image.height; y += image.block_height) {
			for (let x = 0; x < image.width; x += image.block_width) {
				for (let by = 0; by < image.block_height; by++) {
					for (let bx = 0; bx < image.block_width; bx += image.inc) {

						let byte = view.getUint8(ofs++);
						let pos;

						switch (image.colors) {
						case 16:
							pos = ((y + by) * image.width) + (x + bx);
							image.body[pos] = colors[byte & 0xf];
							pos = ((y + by) * image.width) + (x + bx + 1);
							image.body[pos] = colors[byte >> 4];
							break;
						case 256:
							pos = ((y + by) * image.width) + (x + bx);
							image.body[pos] = colors[byte];
							break;
						}

					}
				}
			}
		}

		let y_ofs = image.y % 256;
		let x_ofs = (tim.image_x - image_x) * 4;

		for(let y = 0; y < image.height; y++) {
			for(let x = 0; x < image.width; x++) {

				ctx.fillStyle = image.body[y * image.width + x];
				ctx.fillRect(x + x_ofs, y + y_ofs, 1, 1);

			}
		}
		
		ptr.texture.needsUpdate = true;

	}

	function api_loadTile(ofs, name) {

		let textures = {
			dword : []
		};

		let polyList = [];
		let nbTex = this.MEM.view.getUint8(ofs + 0);
		let nbPoly = this.MEM.view.getUint8(ofs+ 1) & 0x0f;
		ofs += 4;
			
		for(let i = 0; i < nbTex; i++) {

			let tex = this.MEM.view.getUint32(ofs, true);

			if(textures.dword.indexOf(tex) === -1) {
				textures.dword.push(tex);
			}

			ofs += 4;
		}
		
		this.MEM.pallet = [];
		this.DOM.area.pallet.innerHTML = "";
		let materials = [];

		for(let i = 0; i < textures.dword.length; i++) {
			
			let div = document.createElement("div");
			div.classList.add("group");
			
			let h3 = document.createElement("h3");
			h3.textContent = "Pallet " + i;
			div.appendChild(h3);

			let table = document.createElement("table");
			table.setAttribute("border", "1");
			table.classList.add("debug");

			let row = table.insertRow();

			row.insertCell().textContent = "Label";
			row.insertCell().textContent = "Decimal";
			row.insertCell().textContent = "Hex";
			row.insertCell().textContent = "Binary";
			
			let dword = textures.dword[i];
			let palletPage = (dword & 0xFFFF0000) >> 16;
			let imagePage = dword & 0xFFFF;

			let image_x = (imagePage & 0x0f) << 6;
			let image_y = imagePage & 0x10 ? 256 : 0;

			let pallet_x = (palletPage & 0x3f) << 4;
			let pallet_y = palletPage >> 6;

			row = table.insertRow();
			row.insertCell().textContent = "Pallet Page";
			row.insertCell().textContent = this.API.formatDec(palletPage);
			row.insertCell().textContent = this.API.formatHex(palletPage);
			row.insertCell().textContent = this.API.formatBin(palletPage);
			
			row = table.insertRow();
			row.insertCell().textContent = "Pallet X";
			row.insertCell().textContent = this.API.formatDec(pallet_x);
			row.insertCell().textContent = this.API.formatHex(pallet_x);
			row.insertCell().textContent = this.API.formatBin(pallet_x);
			
			row = table.insertRow();
			row.insertCell().textContent = "Pallet Y";
			row.insertCell().textContent = this.API.formatDec(pallet_y);
			row.insertCell().textContent = this.API.formatHex(pallet_y);
			row.insertCell().textContent = this.API.formatBin(pallet_y);

			row = table.insertRow();
			row.insertCell().textContent = "Image Page";
			row.insertCell().textContent = this.API.formatDec(imagePage);
			row.insertCell().textContent = this.API.formatHex(imagePage);
			row.insertCell().textContent = this.API.formatBin(imagePage);
			
			row = table.insertRow();
			row.insertCell().textContent = "Image X";
			row.insertCell().textContent = this.API.formatDec(image_x);
			row.insertCell().textContent = this.API.formatHex(image_x);
			row.insertCell().textContent = this.API.formatBin(image_x);
			
			row = table.insertRow();
			row.insertCell().textContent = "Image Y";
			row.insertCell().textContent = this.API.formatDec(image_y);
			row.insertCell().textContent = this.API.formatHex(image_y);
			row.insertCell().textContent = this.API.formatBin(image_y);
			
			div.appendChild(table);
			
			let canvas = document.createElement("canvas");
			canvas.width = 256;
			canvas.height = 256;
			let context = canvas.getContext("2d");
			div.appendChild(canvas);

			context.fillStyle = "#f00";
			context.fillRect(0, 0, 256, 256);

			context.fillStyle = "#fff";
			context.fillRect(16, 16, 224, 224);

			context.fillStyle = "#000";
			context.font = '30px serif';
			context.fillText('Hello world', 50, 90, 140);

			// Draw debug image?
			
			let debug = {};

			table = document.createElement('table');
			table.setAttribute("border", "1");
			table.classList.add("pallet");

			row = table.insertRow();
			row.insertCell().textContent = "Pallet: ";

			let pal = document.createElement("select");
			let cell = row.insertCell();
			cell.appendChild(pal);
			
			row = table.insertRow();
			row.insertCell().textContent = "Pallet x:";
			debug.pallet_x = row.insertCell();
			
			row = table.insertRow();
			row.insertCell().textContent = "Pallet y:";
			debug.pallet_y = row.insertCell();
			div.appendChild(table);

			table = document.createElement('table');
			table.setAttribute("border", "1");
			table.classList.add("pallet");

			row = table.insertRow();
			row.insertCell().textContent = "Image: ";
			cell = row.insertCell();
			let img = document.createElement("select");
			cell.appendChild(img);
			
			row = table.insertRow();
			row.insertCell().textContent = "Image x:";
			debug.image_x = row.insertCell();
			
			row = table.insertRow();
			row.insertCell().textContent = "Image y:";
			debug.image_y = row.insertCell();
			div.appendChild(table);

			row = table.insertRow();
			row.insertCell().textContent = "Material: ";
			let label = document.createElement("label");
			let node = document.createTextNode(" Transparent");
			let check = document.createElement("input");
			check.setAttribute("type", "checkbox");
			cell = row.insertCell();
			label.appendChild(check);
			label.appendChild(node);
			cell.appendChild(label);
	
			this.DOM.area.pallet.appendChild(div);
			
			let ptr = {};
			ptr.canvas = canvas;
			ptr.ctx = context;
			ptr.pallets = [];
			ptr.images = [];
			ptr.pallet_x = pallet_x;
			ptr.pallet_y = pallet_y;
			ptr.image_x = image_x;
			ptr.image_y = image_y;
			ptr.pal = pal;
			ptr.img = img;
			ptr.debug = debug;
			ptr.check = check;

			this.MEM.pallet.push(ptr);

			// Add Event Listeners

			let callback = evt_handleSelectChange.bind(this, ptr);
			pal.addEventListener("input", callback);
			img.addEventListener("input", callback);
			check.addEventListener("input", callback);
			
			// Find matching pallets

			let pallet;

			for(let i = 0; i < this.MEM.TIM.length; i++) {
				
				// Check for Duplicates
				let found = false;
				for (let k = 0; k < ptr.pallets.length; k++) {
					if(this.MEM.TIM[i].name !== ptr.pallets[k].name) {
						continue;
					}
					found = true;
					break;
				}
				
				if(this.MEM.TIM[i].nb_colors === 256) {
					continue;
				}

				if(this.MEM.TIM[i].pallet_y !== pallet_y) {
					continue;
				}
				
				let nbColor = this.MEM.TIM[i].nb_colors;
				let nbPallet = this.MEM.TIM[i].nb_pallet;

				if(pallet_x < this.MEM.TIM[i].pallet_x) {
					continue;
				}

				if(pallet_x > this.MEM.TIM[i].pallet_x + nbColor*nbPallet*2) {
					continue;
				}
				
				if(!pallet) {
					pallet = this.MEM.TIM[i];
				}
				
				let option = document.createElement("option");
				option.textContent = this.MEM.TIM[i].name;
				option.setAttribute("value", ptr.pallets.length);
				pal.appendChild(option);
				ptr.pallets.push(this.MEM.TIM[i]);

			}

			// Find mathing images

			let image;

			for(let i = 0; i < this.MEM.TIM.length; i++) {
				
				// Check for Duplicates
				let found = false;
				for (let k = 0; k < ptr.images.length; k++) {
					if(this.MEM.TIM[i].name !== ptr.images[k].name) {
						continue;
					}
					found = true;
					break;
				}

				if(found) {
					continue;
				}

				// First Check Y Index
				
				if(this.MEM.TIM[i].nb_colors === 256) {
					continue;
				}

				let yIndex = parseInt(this.MEM.TIM[i].image_y / 256) * 256;
				
				if(yIndex !== image_y) {
					continue;
				}
				
				// Then Check X Index

				let xIndex =  this.MEM.TIM[i].image_x - image_x;
				
				if(i === 0 && this.MEM.TIM[i].name.indexOf("SC1800.TIM") !== -1) {
					console.log("Found!!!");
					console.log(xIndex);
				}

				if(xIndex < 0 || xIndex >= 64) {
					continue;
				}
				
				if(!image) {
					image = this.MEM.TIM[i];
				}
				
				if(this.MEM.TIM[i].name.indexOf("SC1800.TIM") !== -1) {
					console.log("index: %d", ptr.images.length);
					console.log(this.MEM.TIM[i].name);
				}

				let option = document.createElement("option");
				option.textContent = this.MEM.TIM[i].name;
				option.setAttribute("value", ptr.images.length);
				img.appendChild(option);
				ptr.images.push(this.MEM.TIM[i]);

			}

			let texture = new THREE.Texture(canvas);
			texture.flipY = false;

			ptr.material = new THREE.MeshBasicMaterial({
				map : texture
			});

			materials[i] = ptr.material;

			ptr.texture = texture;

			if(!image || !pallet) {
				// Draw debug texture?
				texture.needsUpdate = true;
				continue;
			}

			// Render texture

			this.API.renderTexture(ptr, pallet, image);
			texture.needsUpdate = true;

		}


		for(let i = 0; i < nbPoly; i++) {
				
			let uvOfs = this.MEM.view.getUint32(ofs, true);
			let scale = this.MEM.view.getUint8(ofs + 3) & 0xf0;
			let texId = this.MEM.view.getUint8(ofs + 3) & 0x0f;
			scale = scale >> 3;

			let quadOfs = this.MEM.view.getUint32(ofs + 4, true);
			let nbQuad = this.MEM.view.getUint8(ofs + 7);
				
			polyList.push({
				scale : scale,
				quadOfs : quadOfs & 0xffffff,
				uvOfs : uvOfs & 0xffffff,
				nbQuad : nbQuad,
				texId : texId
			});
			
			ofs += 8;

		}

		let geometry = new THREE.Geometry();
		
	 	// polyList = [ polyList[0] ];
		// console.log(polyList);
		
		polyList.forEach(poly => {
			ofs = poly.quadOfs;
			
			let flags = [];

			for(let k = 0; k < poly.nbQuad; k++) {
				
				flags[k] = this.MEM.view.getUint8(ofs + 3);
				ofs += 4;

				let points = [{
						x : this.MEM.view.getInt8(ofs + 0),
						y : this.MEM.view.getUint8(ofs + 1),
						z : this.MEM.view.getInt8(ofs + 2)
					}, {
						x : this.MEM.view.getInt8(ofs + 3),
						y : this.MEM.view.getUint8(ofs + 4),
						z : this.MEM.view.getInt8(ofs + 5)
					}, {
						x : this.MEM.view.getInt8(ofs + 6),
						y : this.MEM.view.getUint8(ofs + 7),
						z : this.MEM.view.getInt8(ofs + 8)
					}, {
						x : this.MEM.view.getInt8(ofs + 9),
						y : this.MEM.view.getUint8(ofs + 10),
						z : this.MEM.view.getInt8(ofs + 11)
					}];

					let length = geometry.vertices.length;
	
					for(let i = 0; i < points.length; i++) {
						let vertex = new THREE.Vector3();
						vertex.x = points[i].x * 4;
						vertex.y = points[i].y * 4;
						vertex.z = points[i].z * 4;
						geometry.vertices.push(vertex);
					}

					let a = length + 0;
					let b = length + 1;
					let c = length + 2;
					let d = length + 3;

					let face_a = new THREE.Face3(a, b, c);
					let face_b = new THREE.Face3(b, d, c);

					face_a.materialIndex = poly.texId;
					face_b.materialIndex = poly.texId;

					geometry.faces.push(face_a);
					geometry.faces.push(face_b);
				
					ofs += 0x0c;

					if(flags[k] & 0x04) {
						
						console.log("draw back side");
						face_a = new THREE.Face3(c, b, a);
						face_b = new THREE.Face3(c, d, b);

						face_a.materialIndex = poly.texId;
						face_b.materialIndex = poly.texId;

						geometry.faces.push(face_a);
						geometry.faces.push(face_b);

					}

			}

			ofs = poly.uvOfs;
			for(let k = 0; k < poly.nbQuad; k++) {
				
				let coords = [{
					u : this.MEM.view.getUint8(ofs + 0) * 0.00390625 + 0.001953125,
					v : this.MEM.view.getUint8(ofs + 1) * 0.00390625 + 0.001953125
				},{
					u : this.MEM.view.getUint8(ofs + 2) * 0.00390625 + 0.001953125,
					v : this.MEM.view.getUint8(ofs + 3) * 0.00390625 + 0.001953125
				},{
					u : this.MEM.view.getUint8(ofs + 4) * 0.00390625 + 0.001953125,
					v : this.MEM.view.getUint8(ofs + 5) * 0.00390625 + 0.001953125
				},{
					u : this.MEM.view.getUint8(ofs + 6) * 0.00390625 + 0.001953125,
					v : this.MEM.view.getUint8(ofs + 7) * 0.00390625 + 0.001953125
				}];
				
				let a = new THREE.Vector2(coords[0].u, coords[0].v);
				let b = new THREE.Vector2(coords[1].u, coords[1].v);
				let c = new THREE.Vector2(coords[2].u, coords[2].v);
				let d = new THREE.Vector2(coords[3].u, coords[3].v);
				
				geometry.faceVertexUvs[0].push([a, b, c]);
				geometry.faceVertexUvs[0].push([b, d, c]);

				ofs += 8;
				
				if(flags[k] & 0x04) {
				
					geometry.faceVertexUvs[0].push([c, b, a]);
					geometry.faceVertexUvs[0].push([c, d, b]);

				}

			}

		});

		geometry.computeFaceNormals();
		geometry.computeBoundingBox();

		let box = geometry.boundingBox;
		let xDiff = (box.max.x + box.min.x) / 2;
		let zDiff = (box.max.z + box.min.z) / 2;
		//geometry.translate(-xDiff, 0, -zDiff);

		let mat = new THREE.MeshNormalMaterial();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);
		let mesh = new THREE.Mesh(buffer, materials);
		mesh.name = name;
		this.MEM.scene.add(mesh);
		this.MEM.mesh = mesh;
	
	}

	function api_formatDec(num) {

		let str = num.toString();

		while(str.length < 6) {
			str = "0" + str;
		}
		
		return str;

	}


	function api_formatHex(num) {

		let str = num.toString(16);

		while(str.length < 4) {
			str = "0" + str;
		}

		return "0x" + str;

	}

	function api_formatBin(num) {

		let str = num.toString(2);

		while(str.length < 16) {
			str = "0" + str;
		}

		return str;

	}


}).apply({});

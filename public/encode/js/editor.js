const Editor = (function() {

	"use strict";

	this.MEM = {
	
	};

	this.DOM = {
		img : [
			document.getElementById("img[0]"),
			document.getElementById("img[1]")
		],
		canvas : document.getElementById("canvas"),
		btn : {
			palette : document.getElementById("btn.palette"),
			image : document.getElementById("btn.image")
		}
	};

	this.EVT = {
		handleEncode : evt_handleEncode.bind(this),
		handlePaletteClick : evt_handlePaletteClick.bind(this),
		handleImageClick : evt_handleImageClick.bind(this)
	};

	this.API = {
		encode16 : api_encode16.bind(this),
		encode256 : api_encode256.bind(this),
		getPixelIndex : api_getPixelIndex.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

		this.DOM.btn.palette.addEventListener('click', this.EVT.handlePaletteClick);
		this.DOM.btn.image.addEventListener('click', this.EVT.handleImageClick);
		
		this.DOM.img[0].addEventListener("click", this.EVT.handleEncode);
		this.DOM.img[1].addEventListener("click", this.EVT.handleEncode);

		this.MEM.ctx = this.DOM.canvas.getContext("2d");

	}

	function evt_handleEncode(evt) {
		
		let img = evt.target;

		this.MEM.width = img.width;
		this.MEM.height = img.height;
		this.DOM.canvas.width = img.width;
		this.DOM.canvas.height = img.height;

		console.log(this.MEM);

		switch(img.getAttribute("data-code")) {
		case "16":
			this.API.encode16(img);
			break;
		case "256":
			this.API.encode256(img);
			break;
		}


	}

	function evt_handlePaletteClick() {

		var blob = new Blob([this.MEM.pBuff], {type: "octet/stream"});
		saveAs(blob, "palette.bin");

	}

	function evt_handleImageClick() {

		var blob = new Blob([this.MEM.iBuff], {type: "octet/stream"});
		saveAs(blob, "image.bin");

	}

	function api_encode16(img) {
		
		console.log(img);
		this.MEM.ctx.drawImage(img, 0, 0, this.MEM.width, this.MEM.height);

		this.MEM.paletteKeys = {};
		this.MEM.palette = new Array(16);
		this.MEM.image = new Array(this.MEM.width * this.MEM.height / 2);

		let inc = 2;
		let block_width = 128;
		let block_height = 32;

		let y, x, by, bx, pos, byte, low, high, index;
		pos = 0;

		for(y = 0; y < this.MEM.height; y += block_height) {
			for(x = 0; x < this.MEM.width; x += block_width) {
				for(by = 0; by < block_height; by++) {
					for(bx = 0; bx < block_width; bx += inc) {

						index = this.API.getPixelIndex(y + by, x + bx);
						low = index;
						index = this.API.getPixelIndex(y + by, x + bx + 1);
						high = index;
						byte = (high << 4) | low;
						this.MEM.image[pos++] = byte;

					}
				}
			}
		}

		console.log(this.MEM.palette);

		this.MEM.pBuff = new Uint16Array(this.MEM.palette);
		this.MEM.iBuff = new Uint8Array(this.MEM.image);

	}

	function api_encode256(img) {

		this.MEM.ctx.drawImage(img, 0, 0, this.MEM.width, this.MEM.height);

		this.MEM.paletteKeys = {};
		this.MEM.palette = new Array(256);
		this.MEM.image = new Array(this.MEM.width * this.MEM.height);

		let inc = 1;
		let block_width = 64;
		let block_height = 32;

		let y, x, by, bx, pos, byte, low, high, index;
		pos = 0;

		for(y = 0; y < this.MEM.height; y += block_height) {
			for(x = 0; x < this.MEM.width; x += block_width) {
				for(by = 0; by < block_height; by++) {
					for(bx = 0; bx < block_width; bx += inc) {

						index = this.API.getPixelIndex(y + by, x + bx);
						this.MEM.image[pos++] = index;

					}
				}
			}
		}

		this.MEM.pBuff = new Uint16Array(this.MEM.palette);
		this.MEM.iBuff = new Uint8Array(this.MEM.image);

		console.log(this.MEM.iBuff);

	}

	function api_getPixelIndex(y, x) {
		
		let imgData = this.MEM.ctx.getImageData(x, y, 1, 1);

		let r = parseInt((imgData.data[0] / 255) * 31);
		let g = parseInt((imgData.data[1] / 255) * 31);
		let b = parseInt((imgData.data[2] / 255) * 31);
		let a = imgData.data[3];

		if(a < 255) {
			a = 0;
		}

		r = r << 0x00;
		g = g << 0x05;
		b = b << 0x0a;
		a = a << 0x0f;

		let color = r | g | b | a;
		let hex = "0x" + color.toString(16);

		if(hex in this.MEM.paletteKeys) {
			return this.MEM.paletteKeys[hex];
		}

		let index = Object.keys(this.MEM.paletteKeys).length;
		this.MEM.paletteKeys[hex] = index;
		this.MEM.palette[index] = color;

		if(y === 0 && (x === 1 || x === 0)) {
			console.log("--------------");
			console.log(this.MEM.paletteKeys);
			console.log(this.MEM.palette);
			console.log(imgData);
			console.log(index);
			console.log(hex);
		}

		return index;

	}

}).apply({});

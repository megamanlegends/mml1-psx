"use strict";

const fs = require('fs');
const models = require('./models.js');

const path = '../dat/';
let files = fs.readdirSync(path);

const mdt_list = {};

files.forEach(function(file) {

	let buffer = fs.readFileSync(path + file);
	let name = null;
	let ofs, len, mem

	for(let i = 0; i < buffer.length; i+= 0x400) {
		
		let str = buffer.toString('ascii', i + 0x40, i + 0x60);
		if(str.indexOf('MDT') === -1) {
			continue;
		}
		
		ofs = i + 0x800;
		len = buffer.readUInt32LE(i + 4);
		mem = buffer.readUInt32LE(i + 0x0c)
		name = str;
		break;
	}
	
	if(!name) {
		return;
	}
	
	console.log(file);
	let pos = buffer.indexOf("STG");
	let stgOfs = pos % 0x100;
	let stgName = buffer.toString('ascii', stgOfs + 0x40, stgOfs + 0x60);
	let stgLen = buffer.readUInt32LE(stgOfs + 4);
	let stgMem = buffer.readUInt32LE(stgOfs + 12);
	stgOfs += 0x800;

	mdt_list[file] = {
		mdt : {
			name : name.replace(/\0/g, ''),
			offset : ofs,
			length : len,
			memory : mem
		},
		stg : {
			name : stgName.replace(/\0/g, ''),
			offset : stgOfs,
			length : stgLen,
			memory : stgMem
		}
	};

});

fs.writeFileSync("MDTTable.json", JSON.stringify(mdt_list, null, 4));

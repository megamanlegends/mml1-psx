"use strict";

const fs = require('fs');
const models = require('./models.js');

const path = '../dat/';
let files = fs.readdirSync(path);

const EbdTable = {};

files.forEach(function(file) {

	let buffer = fs.readFileSync(path + file);
	let name = null;
	let ofs, len, mem

	for(let i = 0; i < buffer.length; i+= 0x400) {
		
		let str = buffer.toString('ascii', i + 0x40, i + 0x60);
		if(str.indexOf('EBD') === -1) {
			continue;
		}
		
		ofs = i + 0x800;
		len = buffer.readUInt32LE(i + 4);
		mem = buffer.readUInt32LE(i + 0x0c)
		name = str;
		break;
	}

	if(!name) {
		return;
	}

	console.log(file);
	console.log(name);

	let stage = "";

	if(name.indexOf("DEMO") !== -1) {
		stage = "DEMO";
	} else if(name.indexOf("SUPPORT") !== -1) {
		stage = "SUPPORT";
	} else {
		stage = name.split('\\').pop().split("_").shift();
	}

	EbdTable[stage] = EbdTable[stage] || {};

	let entry = {};
	entry.ebd_file = file;
	entry.ebd_prefix = name.split('\\').pop().split(".").shift();
	entry.ebd_offset = ofs;
	entry.ebd_length = len;
	entry.ebd_memory = mem;

	let numModels = buffer.readUInt32LE(ofs);
	ofs += 4;

	for(let i = 0; i < numModels; i++) {
		
		let id = buffer.readUInt32LE(ofs + 0).toString(16);

		let mdl = {
			"mesh_ofs" : buffer.readUInt32LE(ofs + 0x04), 
			"bone_ofs" : buffer.readUInt32LE(ofs + 0x08), 
			"anim_ofs" : buffer.readUInt32LE(ofs + 0x0c)
		};
		mdl.id = id;
		ofs += 0x10;
		
		if(models[id]) {
			mdl.name = models[id].name;
			mdl.found = 1;
		} else {
			mdl.name = id + " Object";
			mdl.found = 0;
		}

		for(let k in entry) {
			mdl[k] = entry[k];
		}

		if(!EbdTable[stage][id]) {
			EbdTable[stage][id] = mdl;
		}

	}


});

fs.writeFileSync("EbdTable.json", JSON.stringify(EbdTable, null, 4));
